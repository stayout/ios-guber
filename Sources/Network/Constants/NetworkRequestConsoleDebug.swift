//
//  NetworkRequestConsoleDebug.swift
//  Networking
//
//  Created by Stayout on 12.10.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation
import Common

struct NetworkRequestConsoleDebug {

    fileprivate static let baseInformation: Bool = false
    fileprivate static let headers: Bool = false
    fileprivate static let response: Bool = false
    fileprivate static let HTTPCode: Bool = false
    fileprivate static let responsedJSON: Bool = false
    fileprivate static let payloadJSON: Bool = false

}

extension NetworkRequestConsoleDebug {
    
    static func URL(_ info: Any) {
        if NetworkRequestConsoleDebug.baseInformation {
            print("================================================================================")
            print("started request with url: \(info)")
        }
    }
    
    static func response(_ info: Any) {
        if NetworkRequestConsoleDebug.response {
            print("response: \(info)")
        }
    }
    
    static func headers(_ info: Any?) {
        if NetworkRequestConsoleDebug.headers {
            print("headers: \(String(describing: info))")
        }
    }
    
    static func HTTPCode(_ info: HTTPURLResponse) {
        if NetworkRequestConsoleDebug.HTTPCode {
            print("response with code: \(info.statusCode)")
        }
    }
    
    static func responsedJSON(_ data: Data?) {
        if let value = data?.asJSONAny, NetworkRequestConsoleDebug.responsedJSON {
            print("responsed json: \(value)")
        }
    }
    
    static func payloadJSON(_ data: Data?) {
        guard let data = data, NetworkRequestConsoleDebug.payloadJSON else { return }
        if let json: Any = data.asJSONDictionary ?? data.asJSONArray() ?? String(data: data, encoding: .utf8) {
            print("payload json: \(json)")
        }
    }

}
