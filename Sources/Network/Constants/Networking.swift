//
//  Networking.swift
//  Networking
//
//  Created by Aleksandr Konakov on 26.11.2020.
//  Copyright © 2020 WS. All rights reserved.
//

public enum Networking {
    
//    public static let environment: Environment = .development
    public static let environment: Environment = .production
    
}
