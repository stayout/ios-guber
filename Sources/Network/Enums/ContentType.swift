//
//  ContentType.swift
//  Networking
//
//  Created by Stayout on 09/03/2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation

public enum ContentType {
    case json
    case form
    case multipart
    
    public var description: String {
        switch self {
        case .json: return "application/json"
        case .form: return "application/x-www-form-urlencoded"
        case .multipart: return "multipart/form-data"
        }
    }
}
