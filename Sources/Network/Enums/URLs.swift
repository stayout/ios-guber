//
//  URLs.swift
//  Networking
//
//  Created by Stayout on 22/01/2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation

public enum URLs {
    case api
    
    case current
    case create
    case details
    case list
    case phases
    case update
    
}

extension URLs {
    
    public var description: String {
        switch self {
        case .api: return Networking.environment.api
            
        case .current: return "/current"
        case .create: return "/create"
        case .details: return "/details"
        case .list: return "/list"
        case .phases: return "/phases"
        case .update: return "/update"

        }
    }
    
    public static func +(left: URL, right: URLs) -> URL {
        return left.appendingPathComponent(right.description)
    }
    
    public static func +(left: URLs, right: String) -> URL {
        return try! left.description.appending(right.description).asURL()
    }
    
    public static func +(left: URLs, right: URLs) -> URL {
        return try! left.description.appending(right.description).asURL()
    }
    
}
