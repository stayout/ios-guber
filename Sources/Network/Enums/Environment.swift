//
//  Environment.swift
//  Networking
//
//  Created by Aleksandr Konakov on 26.11.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation
import Common

extension Environment {

    enum Keys: String {
        case api = "Api"
    }

    func key(_ value: Keys) -> String {
        return "\(value.rawValue).\(rawValue)"
    }
    
}

public enum Environment: String {
    
    case development = "development"
    case production = "production"
    
    public var api: String {
        return Bundle.environment.string(for: key(.api))!
    }
    
}
