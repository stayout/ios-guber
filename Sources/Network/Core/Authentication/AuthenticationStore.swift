//
//  AuthenticationStore.swift
//  Networking
//
//  Created by Stayout on 15.02.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation
import Model
import Common
import RxSwift

public protocol AuthenticationStoreProtocol {
    
    static var isAuthenticated: Bool { get }
    
    static func save(_ value: Authentication)
    static func load() -> Authentication?
    static func clear()
    
}

enum AuthenticationStoreError: Error {
    case notSaved
}

public final class AuthenticationStore: AuthenticationStoreProtocol {
    
    public static var isAuthenticated: Bool {
        return load() != nil
    }
    
    public static func save(_ value: Authentication) {
        KeychainAuthenticationService.save(value: value.login, as: KeychainAuthenticationService.type.login.rawValue)
        KeychainAuthenticationService.save(value: value.password, as: KeychainAuthenticationService.type.password.rawValue)
        KeychainAuthenticationService.save(value: value.useBiometrics, as: KeychainAuthenticationService.type.useBiometrics.rawValue)
        
        if let keycode = value.pinCode {
            KeychainAuthenticationService.save(value: keycode, as: KeychainAuthenticationService.type.pinCode.rawValue)
        }
        else {
            KeychainTokenService.clear(by: KeychainAuthenticationService.type.pinCode.rawValue)
        }
    }
    
    public static func load() -> Authentication? {
        guard
            let login: String = KeychainAuthenticationService.load(by: KeychainAuthenticationService.type.login.rawValue),
            let password: String = KeychainAuthenticationService.load(by: KeychainAuthenticationService.type.password.rawValue),
            let useBiometrics: Bool = KeychainAuthenticationService.load(by: KeychainAuthenticationService.type.useBiometrics.rawValue)
            else { return nil }
        let pinCode: String? = KeychainAuthenticationService.load(by: KeychainAuthenticationService.type.pinCode.rawValue)
        
        return Authentication(login: login, password: password, pinCode: pinCode, useBiometrics: useBiometrics)
    }
    
    public static func clear() {
        KeychainAuthenticationService.clear()
        NotificationCenter.post(name: .userSignedOut)
    }
    
}
