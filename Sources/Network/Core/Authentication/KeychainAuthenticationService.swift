//
//  KeychainAuthenticationService.swift
//  Networking
//
//  Created by Stayout on 15.02.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

public struct KeychainAuthenticationService: KeychainServiceProtocol {
    
    public enum type: String, CaseIterable {
        case login = "login"
        case password = "password"
        case pinCode = "pinCode"
        case useBiometrics = "useBiometrics"
    }
    
    public static func clear() {
        type.allCases.forEach { clear(by: $0.rawValue) }
    }
    
}
