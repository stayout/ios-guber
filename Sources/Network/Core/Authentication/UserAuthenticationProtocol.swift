//
//  UserAuthenticationProtocol.swift
//  Networking
//
//  Created by Stayout on 17.11.2020.
//  Copyright © 2020 WS. All rights reserved.
//

public protocol UserAuthenticationProtocol {
    var isAuthenticated: Bool { get }
}

extension UserAuthenticationProtocol {
    
    public var isAuthenticated: Bool {
        return AuthenticationStore.isAuthenticated
    }
    
}
