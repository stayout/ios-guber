//
//  NetworkRequest.swift
//  Networking
//
//  Created by Stayout on 11.10.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import Common
import Model

public final class NetworkRequest {

    let sessionManager = SessionManager()
    let tokenStore = TokenStore()
    
    func makeRequest(url: URL,
                     data: Data? = nil,
                     method: HTTPMethod,
                     contentType: ContentType = .json,
                     cachePolicy: NSURLRequest.CachePolicy? = nil) -> Observable<Data> {
        var request = try! URLRequest(url: url, method: method)
        request.httpBody = data
        if let policy = cachePolicy {
            request.cachePolicy = policy
        }
        
        return Observable.create({ [weak self] observer -> Disposable in
            self?.sessionManager.request(request).responseData(completionHandler: { result in
                NetworkRequestConsoleDebug.URL(url)
                NetworkRequestConsoleDebug.headers(request.allHTTPHeaderFields)
                NetworkRequestConsoleDebug.payloadJSON(data)

                if let response = result.response {
                    NetworkRequestConsoleDebug.HTTPCode(response)
                    NetworkRequestConsoleDebug.response(response)

                    guard 200..<300 ~= response.statusCode else {
                        NetworkRequestConsoleDebug.responsedJSON(result.data)
                        observer.onError( HTTPError(code: response.statusCode, data: result.data) )
                        return
                    }
                }
                
                if let responsedData = result.data {
                    NetworkRequestConsoleDebug.responsedJSON(responsedData)
                    observer.onNext(responsedData)
                }
                
                observer.onCompleted()
            })
            return Disposables.create()
        })
    }
    
}
