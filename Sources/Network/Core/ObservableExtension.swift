//
//  ObservableExtension.swift
//  Networking
//
//  Created by Stayout on 30.04.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation
import RxSwift
import Model
import Common

public extension Observable {
   
    func json() -> Observable<JSONDictonary> {
        if let json = self as? Observable<JSONDictonary> {
            return json
        }
        guard let data = self as? Observable<Data> else {
            return Observable<JSONDictonary>.of( JSONDictonary() )
        }
        return data.flatMap({ data in
            Observable<JSONDictonary>.create ({ observer in
                do {
                    let jsonAny = try JSONSerialization.jsonObject(with: data, options: [])
                    if let json = jsonAny as? JSONDictonary {
                        observer.onNext(json)
                    }
                    else if let jsonArray = jsonAny as? JSONArray {
                        let dict: JSONDictonary = ["items": jsonArray]
                        observer.onNext(dict)
                    }
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
                return Disposables.create()
            })
        })
    }
    
    func void() -> Observable<Void> {
        return Observable<Void>.create ({ observer in
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        })
    }
    
    func value<A>() -> Observable<A> {
        guard let value = self as? Observable<A> else {
            return Observable<A>.of()
        }
        
        return value.flatMap({ result in
            Observable<A>.create ({ observer in
                observer.onNext(result)
                observer.onCompleted()
                return Disposables.create()
            })
        })
    }
    
    func count() -> Observable<Int> {
        return json().flatMap({ json -> Observable<Int> in
            let count = json.int(for: "totalCount") ?? 0
            return Observable<Int>.of(count)
        })
    }
    
    func item<T: JSONDecodable>() -> Observable<T> {
        return json().flatMap({ json in
            Observable<T>.create ({ observer in
                do {
                    let item = try T(json: json)
                    observer.onNext(item)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
                return Disposables.create()
            })
        })
    }
    
    func item<T: JSONDecodable>(forKey key: String) -> Observable<T> {
        return json().flatMap({ json -> Observable<T> in
            if let json = json.dict(for: key) {
                return Observable<JSONDictonary>.of(json).item()
            }
            throw JSONSerializationError(missing: key, classType: T.self)
        })
    }
    
    func items<T: JSONDecodable>(forKey key: String = "items") -> Observable<[T]> {
        return json().flatMap({ json in
            Observable<[T]>.create ({ observer in
                
                let items: [T] = {
                    guard let array = json.array(for: key) else { return [] }
                    return array.compactMap({ try? T(json: $0) })
                }()
                
                observer.onNext(items)
                observer.onCompleted()
                
                return Disposables.create()
            })
        })
    }
    
}
