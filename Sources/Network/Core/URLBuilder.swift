//
//  URLBuilder.swift
//  Networking
//
//  Created by Stayout on 22/01/2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation

public struct URLBuilder {
    
    public let url: URL
    
    public init(url: URLs) {
        self.url = try! url.description.asURL()
    }
    
    public init(url: URL) {
        self.url = url
    }
    
    public func with(queries: Dictionary<String, Any?>) -> URLBuilder {
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        var queryItems = [URLQueryItem]()
        
        queries.forEach({ query in
            if let value = query.value as? String {
                let item = URLQueryItem(name: query.key, value: value)
                queryItems.append(item)
            }
            else if let value = query.value as? Int {
                let item = URLQueryItem(name: query.key, value: value.description)
                queryItems.append(item)
            }
            else if let array = query.value as? [String] {
                let items = array.map({ value in
                    return URLQueryItem(name: query.key, value: value)
                })
                queryItems.append(contentsOf: items)
            }
        })
        
        if components?.queryItems == nil {
            components?.queryItems = queryItems
        } else {
            components?.queryItems?.append(contentsOf: queryItems)
        }
        
        guard let urlComponents = components, let constructedUrl = urlComponents.url else {
            fatalError("Unconstructable componets of \(url)")
        }
        
        return URLBuilder(url: constructedUrl)
    }
    
    public func with(paths: [String]) -> URLBuilder {
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        
        paths.forEach { pathComponent in
            components?.path += "/\(pathComponent)"
        }
        
        guard let constructedUrl = components?.url else {
            fatalError("Unconstructable componets of \(url)")
        }
        
        return URLBuilder(url: constructedUrl)
    }
    
    public func with(path: String) -> URLBuilder {
        let pathUrl = URLBuilder(url: url)
            .with(paths: [path])
            .url
        return URLBuilder(url: pathUrl)
    }
    
    public func with(pageSize: Int) -> URLBuilder {
        return with(queries: ["pageSize": pageSize.description])
    }
    
    public func with(pageNo: Int, pageSize: Int) -> URLBuilder {
        return with(queries: ["pageNo": pageNo.description, "pageSize": pageSize.description])
    }

    public func with(accessToken: String) -> URLBuilder {
        return with(queries: ["access_token": accessToken])
    }
    
}
