//
//  KeychainTokenService.swift
//  RxAuth
//
//  Created by Stayout on 26/12/2016.
//  Copyright © 2016 WhiteSoft. All rights reserved.
//

import Foundation
import Model

public struct KeychainTokenService: KeychainServiceProtocol {
    
    public enum type: String, CaseIterable {
        case accessToken = "access_token"
    }
    
    public static func clear() {
        type.allCases.forEach { clear(by: $0.rawValue) }
    }
    
}
