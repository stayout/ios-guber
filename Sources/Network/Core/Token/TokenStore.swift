//
//  TokenStore.swift
//  RxAuth
//
//  Created by Stayout on 23/12/2016.
//  Copyright © 2016 WhiteSoft. All rights reserved.
//

import Foundation
import RxSwift
import Model

protocol TokenStoreProtocol {
    func save(_ token: Token) -> Observable<Token>
    func load() -> Token?
    func clear()
}

enum TokenStoreError: Error {
    case noTokenSaved
}

class TokenStore: TokenStoreProtocol {
    
    private var token: Token?
    
    func save(_ token: Token) -> Observable<Token> {
        KeychainTokenService.save(value: token.accessToken, as: KeychainTokenService.type.accessToken.rawValue)
        return Observable.just(token)
    }
    
    func load() -> Token? {
        guard let accessToken: String = KeychainTokenService.load(by: KeychainTokenService.type.accessToken.rawValue) else { return nil
        }
        return Token(accessToken: accessToken)
    }
    
    func clear() {
        KeychainTokenService.clear()
    }
    
}
