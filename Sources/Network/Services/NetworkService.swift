//
//  NetworkService.swift
//  Networking
//
//  Created by Stayout on 20/02/2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import RxSwift
import Alamofire
import Common
import Model

public struct NetworkService {
    
    fileprivate let authManager = AuthService()
    fileprivate let networkRequest = NetworkRequest()
    
    public func makeRequest(url: URL,
                            data: Data? = nil,
                            method: HTTPMethod? = nil,
                            contentType: ContentType = .json) -> Observable<Data> {
        let method: HTTPMethod = data != nil && method == nil ? .post : (method ?? .get)
        
        return self.networkRequest.makeRequest(url: url,
                                               data: data,
                                               method: method,
                                               contentType: contentType)
    }
    
}
