//
//  AuthService.swift
//  Voice
//
//  Created by Aleksandr Konakov on 28.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation
import Model
import Common
import RxSwift

public final class AuthService {
    
    fileprivate let tokenStore = TokenStore()
    fileprivate let networkService = NetworkRequest()
    
    public init() { }
    
}

public extension AuthService {
    
}
