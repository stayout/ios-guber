//
//  KeychainServiceProtocol.swift
//  Networking
//
//  Created by Stayout on 15.02.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation
import Security

fileprivate let account: String = "Voice"

public protocol KeychainServiceProtocol {
    
    static func query(by value: String, type: String) -> CFDictionary?
    static func load(by type: String) -> String?
    static func load(by type: String) -> Int?
    static func save(value: String, as type: String)
    static func save(value: Int, as type: String)

    static func clear(by type: String)
    
}

public extension KeychainServiceProtocol {
    
    static func query(by value: String, type: String) -> CFDictionary? {
        guard let data = value.data(using: String.Encoding.utf8) else { return nil }
        return [kSecClass as String: kSecClassGenericPassword,
                kSecAttrService as String: type,
                kSecAttrAccount as String: account,
                kSecValueData as String: data] as CFDictionary
    }
    
    private static func query(by type: String) -> CFDictionary? {
        guard let value: String = load(by: type) else { return nil }
        return query(by: value, type: type)
    }
    
    static func clear(by type: String) {
        guard let query = query(by: type) else { return }
        SecItemDelete(query)
    }
    
    static func load(by type: String) -> String? {
        let query: [String: Any]  = [kSecClass as String: kSecClassGenericPassword,
                                     kSecAttrService as String: type,
                                     kSecAttrAccount as String: account,
                                     kSecReturnData as String: kCFBooleanTrue,
                                     kSecMatchLimit as String: kSecMatchLimitOne]
        
        var result: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &result)
        guard status == errSecSuccess, let resultData = result as? Data else { return nil }
        
        return String(data: resultData, encoding: String.Encoding.utf8)
    }
    
    static func load(by type: String) -> Int? {
        guard let value: String = load(by: type) else { return nil }
        return Int(value)
    }
    
    static func load(by type: String) -> Bool? {
        guard let value: String = load(by: type) else { return nil }
        return Bool(value)
    }
    
    static func save(value: String, as type: String) {
        guard let query = query(by: value, type: type) else { return }
        SecItemDelete(query)
        SecItemAdd(query, nil)
    }
    
    static func save(value: Int, as type: String) {
        save(value: String(value), as: type)
    }
    
    static func save(value: Bool, as type: String) {
        save(value: String(value), as: type)
    }

}
