//
//  WhoAmI.swift
//  Model
//
//  Created by Stayout on 01.09.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common

private struct Keys {
    static let id = "UserID"
}

public final class WhoAmI: JSONCodable {
    
    public let id: Int

    public init(json: JSONDictonary) throws {
        id = try json.value(for: Keys.id)
    }
    
    public func encode() -> JSONDictonary {
        return .json([
            Keys.id: id
        ])
    }
    
}
