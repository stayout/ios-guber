//
//  Token.swift
//  Model
//
//  Created by Stayout on 28/12/2016.
//  Copyright © 2016 WhiteSoft. All rights reserved.
//

import Common

private struct Keys {
    static let accessToken = "Token"
}

public final class Token: JSONDecodable {
    
    public let accessToken: String

    public init(json: JSONDictonary) throws {
        accessToken = try json.value(for: Keys.accessToken)
    }
    
    public init(accessToken: String) {
        self.accessToken = accessToken
    }
    
}
