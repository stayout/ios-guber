//
//  Route.swift
//  Model
//
//  Created by Stayout on 19.03.2021.
//

import YandexMapsMobile

public struct Route {
    
    let start: YMKPoint
    let end: YMKPoint
    
    public init(start: YMKPoint,
                end: YMKPoint) {
        self.start = start
        self.end = end
    }
    
}
