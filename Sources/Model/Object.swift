//
//  Object.swift
//  Model
//
//  Created by Stayout on 15.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common

private struct Keys {
    static let id = "id"
    static let name = "name"
}

public final class Object: RootItem, JSONDecodable {
    
    public let id: Int
    public let name: String
    
    public init(json: JSONDictonary) throws {
        id = try json.value(for: Keys.id)
        name = try json.value(for: Keys.name)
    }
    
}
