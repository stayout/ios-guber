//
//  Phone.swift
//  Model
//
//  Created by Aleksandr Konakov on 17.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common

private struct Keys {
    static let country = "country"
    static let number = "number"
    static let formattedNumber = "formattedNumber"
}

public final class Phone: JSONCodable {

    public let country: Country
    public let number: String
    public let formattedNumber: String
    
    public init?(country: Country?, number: String?, formattedNumber: String?) {
        if let country = country, let number = number, let formattedNumber = formattedNumber {
            self.country = country
            self.number = "\(country.code)\(number)"
            self.formattedNumber = "+\(country.code) \(formattedNumber)"
        }
        else {
            return nil
        }
    }
    
    public init(json: JSONDictonary) throws {
        country = try json.value(for: Keys.country)
        number = try json.value(for: Keys.number)
        formattedNumber = try json.value(for: Keys.formattedNumber)
    }
    
    public func encode() -> JSONDictonary {
        return .json([
            Keys.country: country,
            Keys.number: number,
            Keys.formattedNumber: formattedNumber
        ])
    }
    
}
