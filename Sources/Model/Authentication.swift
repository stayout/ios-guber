//
//  Authentication.swift
//  Model
//
//  Created by Stayout on 15.02.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

public final class Authentication {
    
    public let login: String
    public let password: String
    public var pinCode: String?
    public var useBiometrics: Bool
    
    public init(login: String,
                password: String,
                pinCode: String? = nil,
                useBiometrics: Bool = false) {
        self.login = login
        self.password = password
        self.pinCode = pinCode
        self.useBiometrics = useBiometrics
    }
    
}
