//
//  Country.swift
//  Model
//
//  Created by Aleksandr Konakov on 27.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common

private struct Keys {
    static let id = "id"
    static let code = "code"
    static let name = "name"
    static let mask = "mask"
    static let language = "language"
}

public final class Country: JSONCodable {
    
    public let id: Int
    public let code: Int
    public let name: String
    public let mask: String
    public let language: String
    
    public init(json: JSONDictonary) throws {
        id = try json.value(for: Keys.id)
        code = try json.value(for: Keys.code)
        name = try json.value(for: Keys.name)
        mask = try json.value(for: Keys.mask)
        language = try json.value(for: Keys.language)
    }
    
    public func encode() -> JSONDictonary {
        return .json([
            Keys.id: id,
            Keys.code: code,
            Keys.name: name,
            Keys.mask: mask,
            Keys.language: language
        ])
    }
    
}
