//
//  AppDelegate.swift
//  SocialTaxi
//
//  Created by Stayout on 03.03.2021.
//

import UIKit
import Common
import YandexMapsMobile

@main class AppDelegate: RootAppDelegate {

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        didFinishLaunch(application, options: launchOptions)
        YMKMapKit.setApiKey("485c4b63-0a24-4771-8b61-e606f42b20b6")
        YMKMapKit.setLocale("ru_RU")
        window?.rootViewController = RootAppBuilder().build()
        return true
    }

}

