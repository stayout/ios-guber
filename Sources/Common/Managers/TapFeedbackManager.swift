//
//  TapFeedbackManager.swift
//  Common
//
//  Created by Stayout on 29.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public class TapFeedbackManager {
    
    class func generate(type: UINotificationFeedbackGenerator.FeedbackType) {
        let generator = UINotificationFeedbackGenerator()
        generator.prepare()
        generator.notificationOccurred(type)
    }
    
}
