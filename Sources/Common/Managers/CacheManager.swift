//
//  CacheService.swift
//  Voice
//
//  Created by Stayout on 20.07.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

public class CacheManager {
    
    public typealias Source = JSONCodable
    private static let userDefaults = UserDefaults.standard
    
    public class func save<C: Source>(_ object: C) {
        let json = object.encode()
        userDefaults.set(json, forKey: C.typeName)
        userDefaults.synchronize()
    }
    
    public class func save<C: Source>(_ objects: [C]) {
        let json = objects.map({  $0.encode() })
        userDefaults.set(json, forKey: C.typeName)
        userDefaults.synchronize()
    }
    
    public class func append<C: Source>(_ object: C) {
        let items: [C] = load() ?? [C]()
        let json = ([object] + items).encode()
        userDefaults.set(json, forKey: C.typeName)
        userDefaults.synchronize()
    }
    
    public class func append<C: Source>(_ objects: [C]) {
        let items: [C] = load() ?? [C]()
        let json = (objects + items).encode()
        userDefaults.set(json, forKey: C.typeName)
        userDefaults.synchronize()
    }
    
    public class func load<C: Source>() -> C? {
        guard let json = userDefaults.dictionary(forKey: C.typeName) else { return nil }
        return try? C(json: json)
    }
    
    public class func load<C: Source>() -> [C]? {
        guard let array = userDefaults.array(forKey: C.typeName) as? JSONArray else { return nil }
        return array.map({ try? C(json: $0) }).compactMap({ return $0 })
    }
    
    public class func clear(type: Source.Type) {
        userDefaults.removeObject(forKey: type.typeName)
        userDefaults.synchronize()
    }
    
}

extension JSONCodable {
    static var typeName: String {
        return String(describing: self)
    }
}
