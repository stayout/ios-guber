//
//  ImageManager.swift
//  Common
//
//  Created by Stayout on 19.09.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import Photos

public enum ImageManagerOptionTypes {
    case synchronous
    case highQualityOptions
    case isNetworkAccessAllowed
}

public class ImageManagerOptions: PHImageRequestOptions {
    
    convenience init(options: ImageManagerOptionTypes...) {
        self.init()
        
        for option in options {
            if option == .synchronous {
                isSynchronous = true
            }
            else if option == .highQualityOptions {
                deliveryMode = .highQualityFormat
            }
            else if option == .isNetworkAccessAllowed {
                isNetworkAccessAllowed = true
            }
        }
    }
    
}

public class ImageManager: PHCachingImageManager {
    
    public static let instance = PHCachingImageManager()
    
    public static func data(for asset: PHAsset?,
                            options: ImageManagerOptions? = nil,
                            completionHander: @escaping (Data?) -> Void ) {
        guard let asset = asset else { return }
        instance.requestImageData(for: asset, options: options, resultHandler: { (data, _, _, info) in
            completionHander(data)
        })
    }
    
    public static func image(for asset: PHAsset?,
                             size: CGSize = PHImageManagerMaximumSize,
                             contentMode: PHImageContentMode = .aspectFill,
                             options: ImageManagerOptions? = nil,
                             progressHandler: ((Double) -> Void)? = nil,
                             completionHander: @escaping (UIImage?) -> Void) {
        guard let asset = asset else { return }
        options?.progressHandler = { (progress, _, _, _) in
            progressHandler?(progress)
        }
        
        instance.requestImage(for: asset, targetSize: size, contentMode: contentMode, options: options, resultHandler: { image, info in
            completionHander(image)
        })
    }
    
    public static func compressImage(_ image: UIImage?, limit: Double) -> Data? {
        guard let image = image, var fileData = image.jpegData(compressionQuality: 1) else { return nil }
        var quality: CGFloat = 1
        
        while Double(fileData.count) > limit {
            quality -= 0.1
            fileData = image.jpegData(compressionQuality: quality)!
        }
        
        return fileData
    }
    
    public static func image(path: String?,
                             size: CGSize? = nil,
                             completionHandler: @escaping (UIImage?) -> Void ) {
        guard let path = path, let size = size else { return }
        DispatchQueue.global().async {
            let image = UIImage(contentsOfFile: path)?.resize(size: size, mode: .clip)
            DispatchQueue.main.async { completionHandler( image ) }
        }
    }
    
}
