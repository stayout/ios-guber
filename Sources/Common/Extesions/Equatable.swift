//
//  Equatable.swift
//  Common
//
//  Created by Stayout on 11.02.2021.
//  Copyright © 2021 WS. All rights reserved.
//

public extension Equatable {
    
    func contains(_ values: Self...) -> Bool {
        return values.contains(self)
    }
    
}

public extension Array where Element: Equatable {
    
    func contains(_ values: Element...) -> Bool {
        for value in values {
            if contains(where: { $0 == value }) {
                return true
            }
        }
        return false
    }
    
}

public extension StringProtocol {
    
    func contains(_ values: Self...) -> Bool {
        for element in values.compactMap({ $0 }) {
            if (self as? String)?.contains(element) == true {
                return true
            }
        }
        return false
    }
    
}
