//
//  NSCoder.swift
//  Common
//
//  Created by Stayout on 14/12/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

extension NSCoder {
    
    public func decode<T>(forKey: String) -> T? {
        return decodeObject(forKey: forKey) as? T
    }
    
    public func decodeString(forKey: String) -> String? {
        return decode(forKey: forKey)
    }
    
    public func decodeInt(forKey: String) -> Int? {
        return decode(forKey: forKey)
    }
    
    public func decodeDate(forKey: String) -> Date? {
        return decode(forKey: forKey)
    }

    
}
