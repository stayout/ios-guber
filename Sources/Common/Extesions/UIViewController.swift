//
//  ViewControllerExtension.swift
//  Common
//
//  Created by Stayout on 20/03/2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    class var visibleViewController: UIViewController? {
        return UIApplication.presentedViewController()
    }
    
    class var visibleNavigationController: UINavigationController? {
        return UIApplication.presentedNavigationController()
    }
    
    var isCurrentController: Bool {
        return self == UIApplication.presentedViewController()
    }
    
}

public extension UIViewController {

    convenience init(title: String?) {
        self.init()
        self.title = title
    }
    
    static func instantiate<T: UIViewController>(storyboard: String = String(describing: T.self),
                                                 identifier: String = String(describing: T.self),
                                                 bundle: Bundle? = nil) -> T {
        let bundle = bundle ?? Bundle(for: self.classForCoder())
        let storyboard = UIStoryboard(name: storyboard, bundle: bundle)
        if let controller = storyboard.instantiateInitialViewController() as? T {
            return controller
        }
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
    
    static func fromNib<T: UIViewController>(resourseName: String = String(describing: T.self)) -> T {
        let bundle = Bundle(for: self.classForCoder())
        return bundle.loadNibNamed(resourseName, owner: self, options: nil)![0] as! T
    }
    
    func findRootViewController<C>(_ base: UIViewController?) -> C? {
        if let controller = base as? C {
            return controller
        }
        else if let controller = base {
            return findRootViewController(controller.parent)
        }
        return nil
    }
    
    func addDismissButton(handler: (() -> Void)?) {
        if navigationController?.viewControllers.first == self, tabBarController == nil {
            navigationItem.leftBarButtonItem = WSBarButtonItem(image: Images.navigationDismiss(), handler: { _ in handler?() })
        }
    }
    
    func configureAsModal(transitionDelegate: UIViewControllerTransitioningDelegate) {
        if UIDevice.isPad {
            transitioningDelegate = transitionDelegate
            modalPresentationStyle = .custom
            view.layer.cornerRadius = 10
            view.layer.masksToBounds = true
        }
    }
    
    func dismiss() {
        DispatchQueue.main.async { [weak self] in
            self?.dismiss(animated: true)
        }
    }
    
    func present(controller: UIViewController?,
                 animated: Bool = true,
                 completion: (() -> Void)? = nil,
                 isModal: Bool = false,
                 presentationStyle: UIModalPresentationStyle = .fullScreen) {
        guard let controller = controller else { return }
        
        UIApplication.hideKeyboard()
        if isModal {
            let navigationController = UIModalNavigationController(rootViewController: controller).configureAsModal()
            navigationController.modalPresentationStyle = presentationStyle
            present(navigationController, animated: animated, completion: completion)
        }
        else {
            controller.modalPresentationStyle = presentationStyle
            present(controller, animated: animated, completion: completion)
        }
    }
    
    func presentError(title: String?, message: String?, delay: Double = 0, acceptHandler: (()->Void)? = nil) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: { [weak self] in
            let controller = UIAlertController(title: title, message: message, acceptButtonAction: acceptHandler)
            self?.present(controller, animated: true)
        })
    }
    
}
