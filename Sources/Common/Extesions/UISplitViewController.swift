//
//  UISplitViewController.swift
//  Voice
//
//  Created by Stayout on 01.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public extension UISplitViewController {
    
    convenience init(controllers: [UIViewController?]) {
        self.init()
        self.viewControllers = controllers.compactMap({ return $0 })
    }
    
    func configure(minimumPrimaryColumnWidth: CGFloat = 320,
                   maximumPrimaryColumnWidth: CGFloat = 320,
                   displayMode: UISplitViewController.DisplayMode = .allVisible) -> Self {
        self.minimumPrimaryColumnWidth = minimumPrimaryColumnWidth
        self.maximumPrimaryColumnWidth = maximumPrimaryColumnWidth
        self.preferredDisplayMode = displayMode
        return self
    }
    
}
