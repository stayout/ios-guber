//
//  CaseIterable.swift
//  Common
//
//  Created by Aleksandr Konakov on 13.01.2021.
//  Copyright © 2021 WS. All rights reserved.
//

public extension CaseIterable where Self: Equatable {
    
    func next() -> Self {
        let all = Self.allCases
        let currentIndex = all.firstIndex(of: self)!
        let nextIndex = all.index(after: currentIndex)
        return all[nextIndex == all.endIndex ? all.startIndex : nextIndex]
    }
    
}
