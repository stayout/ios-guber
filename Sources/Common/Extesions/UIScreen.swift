//
//  UIScreen.swift
//  Voice
//
//  Created by Stayout on 01.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public extension UIScreen {
    
    static var size: CGSize {
        return UIScreen.main.bounds.size
    }
    
    static var width: CGFloat {
        return UIScreen.size.width
    }
    
    static var height: CGFloat {
        return UIScreen.size.height
    }
    
    static var isTightScreen: Bool {
        return UIScreen.width <= 320.0
    }
}
