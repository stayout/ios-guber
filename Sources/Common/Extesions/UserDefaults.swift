//
//  UserDefaults.swift
//  Common
//
//  Created by Stayout on 02.11.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public extension UserDefaults {
    
    static func set(value: Any, key: UserDefaultsKeys) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func removeValue(forKey key: UserDefaultsKeys) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func value<C>(key: UserDefaultsKeys) -> C? {
        if let value = UserDefaults.standard.value(forKey: key.rawValue) as? C {
            return value
        }
        return nil
    }
    
}
