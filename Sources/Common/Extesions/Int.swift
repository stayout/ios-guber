//
//  IntExtension.swift
//  Common
//
//  Created by Stayout on 13.04.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation

public extension Int {
    
    /// Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
    ///
    /// - Parameter array: массив слов для чисел (1, 2, 5)
    /// - Returns: строковое представление числа
    func ending(by array: [String]) -> String {
        if self % 100 >= 11 && self % 100 <= 19 { return "\(array[2])" }
        else {
            switch self % 10 {
            case 1: return "\(array[0])"
            case 2, 3, 4: return "\(array[1])"
            default: return "\(array[2])"
            }
        }
    }
    
    var factorial: Double {
        guard self != 0 else { return 1 }
        return (1...self).map(Double.init).reduce(1.0, *)
    }
    
    var double: Double {
        return Double(self)
    }
    
}
