//
//  StringImage.swift
//  Common
//
//  Created by Aleksandr Konakov on 01.12.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public extension String {
    
    func image(withAttributes attributes: [NSAttributedString.Key: Any]? = nil, size: CGSize? = nil) -> UIImage? {
        let size = size ?? (self as NSString).size(withAttributes: attributes)
        return UIGraphicsImageRenderer(size: size).image { _ in
            (self as NSString).draw(in: CGRect(origin: .zero, size: size),
                                    withAttributes: attributes)
        }
    }
    
}


