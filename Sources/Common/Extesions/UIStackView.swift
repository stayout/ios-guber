//
//  UIStackView.swift
//  Common
//
//  Created by Stayout on 13.01.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import UIKit

public extension UIStackView {
    
    convenience init(views: [UIView],
                     axis: NSLayoutConstraint.Axis,
                     alignment: UIStackView.Alignment = .fill,
                     distribution: UIStackView.Distribution = .fill,
                     spacing: CGFloat = 0) {
        self.init(arrangedSubviews: views)
        self.axis = axis
        self.alignment = alignment
        self.spacing = spacing
        self.distribution = distribution
    }
    
    func removeSubviews() {
        arrangedSubviews.forEach({ view in
            view.removeFromSuperview()
        })
    }
    
}
