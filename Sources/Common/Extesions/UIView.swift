//
//  UIViewExtension.swift
//  Common
//
//  Created by Stayout on 03.11.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import SnapKit

public extension UIView {
        
    convenience init(background: UIColor?) {
        self.init(frame: .zero)
        backgroundColor = background
    }
    
    func add(subview view: UIView?) {
        guard let view = view else { return }
        addSubview(view)
    }
    
    func add(subview view: UIView?, constraints: (_ maker: ConstraintMaker) -> Void ) {
        guard let view = view else { return }
        addSubview(view)
        view.snp.makeConstraints({ maker in
            constraints(maker)
        })
    }
    
    static func fromNib<T: UIView>(resourseName: String = String(describing: T.self)) -> T {
        let bundle = Bundle(for: self.classForCoder())
        return bundle.loadNibNamed(resourseName, owner: nil, options: nil)![0] as! T
    }
    
    func findView(by type: UIView.Type) -> UIView? {
        for subview in self.subviews {
            if subview.isKind(of: type) {
                return subview
            }
            else if let view = subview.findView(by: type) {
                return view
            }
        }
        return nil
    }
    
    func rounded(by corners: UIRectCorner, radius: CGFloat) {
        clipsToBounds = true
        layer.cornerRadius = radius
        layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
    }
    
    func applyDefaultShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 4)
        layer.shadowRadius = 10
        layer.shadowOpacity = 1
    }
    
    func removeShadow() {
        layer.shadowColor = nil
        layer.shadowOffset = .zero
        layer.shadowRadius = 0
        layer.shadowOpacity = 0
    }
    
    func addTopView(with color: UIColor?) {
        let view = UIView(background: color)
        add(subview: view, constraints: { maker in
            maker.bottom.equalTo(self.snp.top)
            maker.left.equalToSuperview()
            maker.height.equalToSuperview()
            maker.width.equalToSuperview()
        })
        sendSubviewToBack(view)
    }
    
    fileprivate var separatorHeight: CGFloat {
        return 0.25
    }
    fileprivate func layer(by rect: CGRect, color: UIColor?) -> CAShapeLayer {
        let layer = CAShapeLayer()
        layer.path = UIBezierPath(rect: rect).cgPath
        layer.strokeColor = color?.cgColor
        layer.fillColor = color?.cgColor
        return layer
    }
    
    func addTopSeparator(with color: UIColor?) {
        let rect = CGRect(origin: .zero, size: CGSize(width: bounds.width, height: separatorHeight))
        layer.addSublayer( layer(by: rect, color: color) )
    }
    
    func addBottomSeparator(with color: UIColor?) {
        let rect = CGRect(x: 0, y: bounds.height - separatorHeight, width: bounds.width, height: separatorHeight)
        layer.addSublayer( layer(by: rect, color: color) )
    }
    
    func set(isHidden: Bool, duration: Double) {
        self.set(alpha: isHidden ? 0 : 1, duration: duration)
    }
    
    func set(alpha: CGFloat, duration: Double) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = alpha
        })
    }
    
    func set(backgroundColor: UIColor?, duration: Double) {
        UIView.animate(withDuration: duration, animations: {
            self.backgroundColor = backgroundColor
        })
    }
    
    func layoutIfNeeded(duration: Double = 0,
                        animations: (() -> Void)? = nil,
                        completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration,
                       animations: {
                        animations?()
                        self.layoutIfNeeded()
        },
                       completion: completion)
    }

    func rotate(duration: Double = 0.5, angle: CGFloat, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration,
                       delay: 0,
                       options: .curveLinear,
                       animations: {
                       self.transform = self.transform.rotated(by: angle)
        }, completion: { _ in
            completion?()
        })
    }
    
}
