//
//  HTTPCookieStorage.swift
//  Common
//
//  Created by Stayout on 16.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public extension HTTPCookieStorage {
    
    static func cookie(url: URL, name: CookieName) -> HTTPCookie? {
        return HTTPCookieStorage.shared.cookies(for: url)?.first(where: { $0.name == name.description })
    }
    
    static func cookie(name: CookieName) -> HTTPCookie? {
        return HTTPCookieStorage.shared.cookies?.first(where: { $0.name == name.description })
    }
    
    static func clear() {
        HTTPCookieStorage.shared.cookies?.compactMap({ $0 }).forEach({
            HTTPCookieStorage.shared.deleteCookie($0)
        })
    }
    
}
