//
//  UIDeviceExtension.swift
//  Common
//
//  Created by Stayout on 17.04.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public extension UIDevice {
    
    static var isPad: Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    static var isPhone: Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    
}
