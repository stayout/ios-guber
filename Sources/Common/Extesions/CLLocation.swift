//
//  CLLocation.swift
//  Common
//
//  Created by Stayout on 11.02.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import MapKit

public extension CLLocation {
    
    func geocode(completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void) {
        CLGeocoder().reverseGeocodeLocation(self, completionHandler: completion)
    }
    
}
