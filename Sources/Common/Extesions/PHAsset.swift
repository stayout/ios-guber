//
//  PHAsset.swift
//  Voice
//
//  Created by Stayout on 20.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation
import Photos

public extension PHAsset {
    
    func url(completionHandler : @escaping ((_ responseURL : URL?) -> Void)) {
        if mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, _) -> Void in
                completionHandler(contentEditingInput?.fullSizeImageURL)
            })
        }
        else if mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: { (asset: AVAsset?, _, _) -> Void in
                guard let asset = asset as? AVURLAsset else {
                    completionHandler(nil)
                    return
                }
                completionHandler(asset.url)
            })
        }
    }
        
}
