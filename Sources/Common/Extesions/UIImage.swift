//
//  UIImageExtension.swift
//  Common
//
//  Created by Stayout on 28.11.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import Toucan

public extension UIImage {
    
    var resized: UIImage? {
        let size = CGSize(width: 1024, height: 1024)
        return resize(size: size, mode: .clip)
    }
    
    func resize(size: CGSize, mode: Toucan.Resize.FitMode) -> UIImage? {
        return Toucan(image: self).resize(size, fitMode: mode).image
    }
    
}
