//
//  UIViewProperties.swift
//  Common
//
//  Created by Stayout on 23.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public extension UIView {
    
    var height: CGFloat {
        return frame.size.height
    }
    
    var width: CGFloat {
        return frame.size.width
    }
    
    var bundle: Bundle {
        return Bundle(for: type(of: self))
    }
    
    var nibName: String {
        return type(of: self).description().split(by: ".").last ?? ""
    }
    
    func scrollView() -> UIScrollView? {
        return value(forKey: "scrollView") as? UIScrollView
    }
    
}

