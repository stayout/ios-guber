//
//  NotificationName.swift
//  Common
//
//  Created by Stayout on 25/10/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public extension Notification.Name {
    static let keyboardWillShow = UIResponder.keyboardWillShowNotification
    static let keyboardWillHide = UIResponder.keyboardWillHideNotification
    static let deviceOrientationDidChange = Notification.Name("deviceOrientationDidChange")
    static let selectedOutcomesDidChange = Notification.Name("selectedOutcomesDidChange")
    static let userBalanceDidChange = Notification.Name("userBalanceDidChange")
    static let setNeedsUpdateBalance = Notification.Name("setNeedsUpdateBalance")
    static let userSignedIn = Notification.Name("userSignedIn")
    static let userSignedOut = Notification.Name("userSignedOut")
    static let favoriteEventsDidChange = Notification.Name("favoriteEventsDidChange")
    static let quickBetSettingDidChange = Notification.Name("quickBetSettingDidChange")
    static let logAnalyticEvent = Notification.Name("logAnalyticEvent")
}
