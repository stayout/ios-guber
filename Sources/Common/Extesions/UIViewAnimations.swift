//
//  UIViewAnimations.swift
//  Voice
//
//  Created by Stayout on 18.07.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public extension UIView {
    
    func performScaleAnimation(duration: Double = 0.35,
                               hideHandler: (() -> Void)? = nil) {
        layer.removeAllAnimations()
        UIView.animate(withDuration: duration,
                       animations: {
                        self.transform = .hidden
                       },
                       completion: { _ in
                        if let hideHandler = hideHandler { hideHandler() }
                        UIView.animate(withDuration: duration, animations: {
                            self.transform = .normal
                        })
                       })
    }
    
    func performFlipAnimation(options: UIView.AnimationOptions = .transitionFlipFromLeft,
                              animationDuration: Double = 0.75,
                              hideHandler: (() -> Void)? = nil) {
        layer.removeAllAnimations()
        UIView.transition(with: self, duration: animationDuration, options: options, animations: {
            if let hideHandler = hideHandler { hideHandler() }
        }, completion: nil)
    }
    
    static func transition(from sourceViewController: UIViewController?,
                           to targetViewController: UIViewController?,
                           duration: Double = 0.5,
                           delay: Double = 0.0,
                           options: UIView.AnimationOptions = [.transitionCrossDissolve],
                           completion: ((Bool) -> Void)? = nil) {
        guard let sourceView = sourceViewController?.view, let targetView = targetViewController?.view else { return }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: {
            UIView.transition(from: sourceView, to: targetView, duration: duration, options: options, completion: completion)
        })
    }
    
    func shake(duration: CFTimeInterval,
               completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            CATransaction.begin()
            CATransaction.setCompletionBlock(completion)
            let translation = CAKeyframeAnimation(keyPath: "transform.translation.x");
            translation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            translation.values = [-10, 10, -10, 10, -5, 5, -3, 3, -2, 2, 0]
            
            let shakeGroup: CAAnimationGroup = CAAnimationGroup()
            shakeGroup.animations = [translation]
            shakeGroup.duration = duration
            
            self.layer.add(shakeGroup, forKey: "shakeIt")
            CATransaction.commit()
        }
    }
    
    func fadeTransition(_ duration: CFTimeInterval = 0.33) {
        DispatchQueue.main.async {
            let animation = CATransition()
            animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            animation.type = .fade
            animation.duration = duration
            self.layer.add(animation, forKey: CATransitionType.fade.rawValue)
        }
    }
    
    static func animateAsync(withDuration duration: Double,
                             animations: @escaping () -> Void,
                             delay: Double = 0.0,
                             completion: ((Bool) -> Void)? = nil) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: {
            UIView.animate(withDuration: duration, animations: animations, completion: completion)
        })
    }
    
}
