//
//  StringExtension.swift
//  Common
//
//  Created by Stayout on 02.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

public extension String {
    
    var asURL: URL? {
        return URL(string: self)
    }
    
    static var unique: String {
        return UUID().uuidString.lowercased
    }
    
    var extenstion: String {
        return (self as NSString).pathExtension.lowercased
    }
    
    var extensionReplaced: String {
        return replacing(extenstion)
    }
    
    var lowercased: String {
        return self.lowercased(with: nil)
    }
    
    var uppercased: String {
        return self.uppercased(with: nil)
    }
    
    var firstSymbolUppercased: String {
        return self[0..<1].uppercased + self[1..<count]
    }
    
    var clearedPhone: String {
        return replacing("+").replacing(" ").replacing("(").replacing(")").replacing("-")
    }
    
}

public extension String {
    
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }
    
    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
    
    static func + (left: String, right: String?) -> String {
        return left + (right ?? "")
    }
    
    static func + (left: String?, right: String) -> String {
        return (left ?? "") + right
    }
    
    func subString(to index: Int) -> String {
        if count > 0 {
            return String(prefix(index))
        }
        return self
    }
    
    func split(by separator: Character) -> [String] {
        return split(separator: separator).map({ $0.description })
    }
    
    mutating func replace(_ string: String, by newValue: String = "") {
        self = replacing(string, by: newValue)
    }
    
    func replacing(_ string: String, by newValue: String = "") -> String {
        return replacingOccurrences(of: string, with: newValue)
    }
    
}
