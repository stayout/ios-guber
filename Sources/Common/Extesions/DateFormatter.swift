//
//  DateFormatter.swift
//  Voice
//
//  Created by Stayout on 31.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    public convenience init(format: String, locale: Locale? = nil) {
        self.init()
        dateFormat = format
        self.locale = locale ?? Locale(identifier: "en")
    }
    
    public convenience init(format: DateFormats = .defaultDate, locale: Locale? = nil) {
        self.init()
        dateFormat = format.rawValue
        self.locale = locale ?? Locale(identifier: "en")
    }
    
    public static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    
}
