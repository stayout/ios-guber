//
//  UIAlertControllerExtension.swift
//  Common
//
//  Created by Stayout on 21.12.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

public enum WSAlertActionType {
    case done
    case normal
    case cancel
}

public class WSAlertAction: UIAlertAction {
    var type: WSAlertActionType?
}

public extension UIAlertController {
    
    func acceptActionButton() -> UIAlertAction? {
        guard let actions = self.actions as? [WSAlertAction] else { return nil }
        return actions.first(where: { $0.type == .done })
    }
    
    func add(action: WSAlertAction) -> UIAlertController {
        self.addAction(action)
        return self
    }
    
    convenience init(style: UIAlertController.Style = .alert,
                     title: String?,
                     message: String? = nil) {
        self.init(title: title, message: message, preferredStyle: style)
    }
    
    convenience init(style: UIAlertController.Style = .alert,
                     title: String?,
                     message: String? = nil,
                     acceptButtonTitle: String = "OK",
                     acceptButtonAction: (() -> Void)? = nil) {
        self.init(title: title, message: message, preferredStyle: style)
        let action = WSAlertAction(title: acceptButtonTitle, style: .default, handler: { _ in acceptButtonAction?() })
        action.type = .done
        self.addAction(action)
    }
    
    convenience init(style: UIAlertController.Style = .alert,
                     title: String?,
                     message: String? = nil,
                     acceptButtonTitle: String = "OK",
                     acceptButtonAction: (() -> Void)? = nil,
                     cancelButtonTitle: String = "Отмена",
                     cancelButtonAction: (() -> Void)? = nil) {
        self.init(style: style, title: title, message: message, acceptButtonTitle: acceptButtonTitle, acceptButtonAction: acceptButtonAction)
        let action = WSAlertAction(title: cancelButtonTitle, style: .cancel, handler: { _ in cancelButtonAction?() })
        action.type = .cancel
        self.addAction(action)
    }
    
}
