//
//  UITabBarController.swift
//  Common
//
//  Created by Stayout on 23.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public extension UITabBarController {
    
    convenience init?(controllers: [UIViewController?], animated: Bool = false) {
        let controllers = controllers.compactMap({ $0 })
        guard !controllers.isEmpty else { return nil }
        self.init()
        setViewControllers(controllers, animated: animated)
    }
    
}
