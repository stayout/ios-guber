//
//  UIColorExtension.swift
//  Common
//
//  Created by Stayout on 10.12.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

public extension UIColor {

    convenience init(_ desc: String, opacity: CGFloat = 1) {
        if let rgbValue = UInt(desc, radix: 16) {
            let red   =  CGFloat((rgbValue >> 16) & 0xff) / 255
            let green =  CGFloat((rgbValue >>  8) & 0xff) / 255
            let blue  =  CGFloat((rgbValue      ) & 0xff) / 255
            self.init(red: red, green: green, blue: blue, alpha: opacity)
        }
        else { self.init() }
    }
    
}
