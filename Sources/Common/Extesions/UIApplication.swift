//
//  UIApplicationExtension.swift
//  Common
//
//  Created by Stayout on 19.02.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public extension UIApplication {
    
    enum AppConfiguration {
        case debug
        case testFlight
        case appStore
    }
    
    static var configuration: AppConfiguration {
        if Bundle.main.appStoreReceiptURL?.lastPathComponent == "sandboxReceipt" {
            return .testFlight
        }
        #if DEBUG
        return .debug
        #else
        return .appStore
        #endif
    }
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
    class var window: UIWindow? {
        return UIApplication.shared.keyWindow
    }
    
    class var root: UIViewController? {
        return UIApplication.window?.rootViewController
    }
    
    class var safeArea: UIEdgeInsets {
        return UIApplication.window!.safeAreaInsets
    }
    
    class func open(url: URL?) {
        guard let url = url else { return }
        UIApplication.shared.open(url)
    }
    
    static func presentedViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return presentedViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return presentedViewController(selected)
        }
        if let presented = base?.presentedViewController {
            return presentedViewController(presented)
        }
        return base
    }
    
    static func presentedNavigationController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UINavigationController? {
        if let tab = base as? UITabBarController, let presented = tab.selectedViewController {
            return presentedNavigationController(presented)
        }
        if let presented = base?.presentedViewController {
            return presentedNavigationController(presented)
        }
        return base as? UINavigationController ?? base?.navigationController
    }
    
    static func hideKeyboard() {
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.endEditing(true)
        }
    }
    
    static func setRootViewController(_ controller: UIViewController?) {
        guard let window = UIApplication.window else { return }
        
        window.rootViewController = controller
        UIView.transition(with: window,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
    
}
