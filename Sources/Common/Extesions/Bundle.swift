//
//  Bundle.swift
//  Common
//
//  Created by Alexander Makarov on 18.02.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import Foundation

public extension Bundle {
    
    static var settings: JSONDictonary {
        return JSONDictonary(plist: "Settings")!
    }
    
    static var environment: JSONDictonary {
        return JSONDictonary(plist: "Environment")!
    }
}
