//
//  AttributedString.swift
//  Common
//
//  Created by Stayout on 24.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public extension String {
    
    func ranges(for string: String, options: NSRegularExpression.Options = .caseInsensitive) -> [NSRange]? {
        let regex = try? NSRegularExpression(pattern: string, options: options)
        return regex?.matches(in: self, range: NSRange(self.startIndex..., in: self)).compactMap({ $0.range })
    }
    
    func appending(attribute: NSAttributedString.Key,
                   value: Any?,
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        guard let value = value else { return attributedString }
        let regex = try? NSRegularExpression(pattern: self, options: options)
        guard let ranges = regex?.matches(in: self, range: NSRange(self.startIndex..., in: self)).compactMap({ $0.range }) else { return attributedString }
        ranges.forEach({ range in attributedString.addAttribute(attribute, value: value, range: range) })
        return attributedString
    }
    
    func appending(attributes: [NSAttributedString.Key: Any?]) -> NSMutableAttributedString {
        let attributes = attributes.filter({ $0.value != nil }) as [NSAttributedString.Key: Any]
        return NSMutableAttributedString(string: self, attributes: attributes)
    }
    
    func appending(attribute: NSAttributedString.Key,
                   value: Any?,
                   for string: String,
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        return attributedString.appending(attribute: attribute, value: value, for: string, options: options)
    }
    
    func appending(attribute: NSAttributedString.Key,
                   value: Any?,
                   for stringArray: [String],
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        return attributedString.appending(attribute: attribute, value: value, for: stringArray, options: options)
    }
    
    func appending(attributes: [NSAttributedString.Key: Any],
                   for string: String,
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        return attributedString.appending(attributes: attributes, for: string, options: options)
    }
    
    func appending(attributes: [NSAttributedString.Key: Any],
                   for stringArray: [String],
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        return attributedString.appending(attributes: attributes, for: stringArray, options: options)
    }
    
}

public extension NSMutableAttributedString {
    
    func append(attributes: [NSAttributedString.Key: Any?],
                for string: String,
                options: NSRegularExpression.Options = .caseInsensitive) {
        let attributes = attributes.filter({ $0.value != nil }) as [NSAttributedString.Key: Any]
        self.string.ranges(for: string, options: options)?.forEach({ range in
            addAttributes(attributes, range: range)
        })
    }
    
    func append(attributes: [NSAttributedString.Key: Any?],
                for stringArray: [String],
                options: NSRegularExpression.Options = .caseInsensitive) {
        let attributes = attributes.filter({ $0.value != nil }) as [NSAttributedString.Key: Any]
        for string in stringArray {
            append(attributes: attributes, for: string, options: options)
        }
    }
    
    func append(attribute: NSAttributedString.Key,
                value: Any?,
                for string: String,
                options: NSRegularExpression.Options = .caseInsensitive) {
        self.string.ranges(for: string, options: options)?.forEach({ range in
            addAttribute(attribute, value: value!, range: range)
        })
    }
    
    func append(attribute: NSAttributedString.Key,
                value: Any?,
                for stringArray: [String],
                options: NSRegularExpression.Options = .caseInsensitive) {
        for string in stringArray {
            append(attribute: attribute, value: value, for: string, options: options)
        }
    }
    
    func appending(attribute: NSAttributedString.Key,
                   value: Any?,
                   for string: String,
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        append(attribute: attribute, value: value, for: string, options: options)
        return self
    }
    
    func appending(attribute: NSAttributedString.Key,
                   value: Any?,
                   for stringArray: [String],
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        append(attribute: attribute, value: value, for: stringArray, options: options)
        return self
    }
    
    func appending(attributes: [NSAttributedString.Key: Any?],
                   for string: String,
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributes = attributes.filter({ $0.value != nil }) as [NSAttributedString.Key: Any]
        append(attributes: attributes, for: string, options: options)
        return self
    }
    
    func appending(attributes: [NSAttributedString.Key: Any?],
                   for stringArray: [String],
                   options: NSRegularExpression.Options = .caseInsensitive) -> NSMutableAttributedString {
        let attributes = attributes.filter({ $0.value != nil }) as [NSAttributedString.Key: Any]
        append(attributes: attributes, for: stringArray, options: options)
        return self
    }
    
}
