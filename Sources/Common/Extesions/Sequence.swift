//
//  Sequence.swift
//  Voice
//
//  Created by Stayout on 16.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

public extension Sequence where Iterator.Element: Hashable {
    
    var unique: [Iterator.Element] {
        var alreadyAdded = Set<Iterator.Element>()
        return self.filter({ alreadyAdded.insert($0).inserted })
    }
    
}
