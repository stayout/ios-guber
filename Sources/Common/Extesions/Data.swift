//
//  Data.swift
//  Voice
//
//  Created by Stayout on 22.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

extension Data {
    
    fileprivate mutating func append(_ value: String) {
        if let data = value.data(using: .utf8) {
            self.append(data)
        }
    }
    
    public func multipart(name: String, boundry: String) -> Data {
        var data = Data()
        
        data.append("------\(boundry)\r\n")
        data.append("Content-Disposition: form-data; name=\"signatureRequire\"")
        data.append("\r\n\r\n")
        data.append("false\r\n")
        
        data.append("------\(boundry)\r\n")
        data.append("Content-Disposition: form-data; name=\"type\"")
        data.append("\r\n\r\n")
        data.append("CONTENT\r\n")
        
        data.append("------\(boundry)\r\n")
        data.append("Content-Disposition: form-data; name=\"data\"; filename=\"\(name)\"\r\n")
        data.append("Content-Type: image/jpeg\r\n\r\n")
        
        data.append(self)
        data.append("\r\n")
        data.append("------\(boundry)--\r\n")
        
        return data
    }
    
}
