//
//  URLExtension.swift
//  Common
//
//  Created by Stayout on 24.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation

public extension URL {
    
    var attributes: [FileAttributeKey : Any]? {
        return try? FileManager.default.attributesOfItem(atPath: path)
    }
    
    var fileSize: UInt64 {
        return attributes?[.size] as? UInt64 ?? UInt64(0)
    }
    
    var creationDate: Date? {
        return attributes?[.creationDate] as? Date
    }
    
    var modificationDate: Date {
        return attributes?[.modificationDate] as? Date ?? Date()
    }
    
    var isPDF: Bool {
        return pathExtension.lowercased.contains("pdf")
    }
    
    var isDoc: Bool {
        return pathExtension.lowercased.contains("doc", "docx")
    }
    
    var isXls: Bool {
        return pathExtension.lowercased.contains("xls", "xlsx")
    }
    
    var isImage: Bool {
        return pathExtension.lowercased.contains("jpg", "jpeg", "gif", "png")
    }
    
}
