//
//  Progress.swift
//  Voice
//
//  Created by Stayout on 16.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

extension Progress {
    public func percentage(totalSize: Int64) -> Double {
        return Double(completedUnitCount) / Double(totalSize)
    }
}
