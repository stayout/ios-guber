//
//  StringRegExp.swift
//  Voice
//
//  Created by Stayout on 03/10/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

public enum RegularExpresion: String {
    case email = "^([a-z0-9_\\.-]+)@([a-z0-9_\\.-]+)\\.([a-z\\.]{2,6})$"
}

extension String {
    
    public func validate(expression: RegularExpresion = .email) -> Bool {
        guard let regexp = try? NSRegularExpression(pattern: expression.rawValue) else { return false }
        let results = regexp.matches(in: self, range: NSRange(self.startIndex..., in: self))
        return results.count != 0
    }
    
}
