//
//  UIButton.swift
//  Common
//
//  Created by Stayout on 18/12/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit
import SnapKit

public extension UIButton {
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()?.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
    
    ///Обновление высоты компонента с учетом отступа экрана.
    func updateHeight(_ value: CGFloat, bottomOffsetRequired: Bool = true) {
        let offset = bottomOffsetRequired == false ? 0 : UIApplication.window?.safeAreaInsets.bottom ?? 0
        self.snp.updateConstraints({ maker in
            maker.height.equalTo(value + offset / 1.5)
        })
        contentEdgeInsets = UIEdgeInsets(top: contentEdgeInsets.top,
                                         left: contentEdgeInsets.left,
                                         bottom: offset / 1.5,
                                         right: contentEdgeInsets.right)
    }
    
}
