//
//  UISearchBar.swift
//  Common
//
//  Created by Stayout on 17.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public extension UISearchBar {
    
    var textField: UITextField? {
        return subviews.first?.subviews.first(where: { $0.isKind(of: UITextField.self) }) as? UITextField
    }
    
    var backgroundImageView: UIImageView? {
        guard let searchBarBackgroundClass = NSClassFromString("UISearchBarBackground") else { return nil }
        return subviews.first?.subviews.first(where: { $0.isKind(of: searchBarBackgroundClass)} ) as? UIImageView
    }
    
}
