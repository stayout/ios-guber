//
//  DoubleExtension.swift
//  Common
//
//  Created by Stayout on 24.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public extension Double {
    
    func round(to: Int) -> Double {
        guard let rounded = Double(String(format: "%.\(to)f", self)) else { return self }
        return rounded
    }
    
    var floatValue: CGFloat {
        return CGFloat(self)
    }
    
}
