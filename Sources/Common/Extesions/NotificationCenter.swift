//
//  NotificationCenterExtension.swift
//  Common
//
//  Created by Stayout on 21.11.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

public extension NotificationCenter {
    
    static func addObserver(name: Notification.Name, queue: OperationQueue = .main, handler: @escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(forName: name, object: nil, queue: .main, using: handler)
    }
    
    static func addObserver(_ target: Any, selector: Selector, name: Notification.Name) {
        NotificationCenter.default.addObserver(target, selector: selector, name: name, object: nil)
    }

    static func removeObservers(_ target: Any) {
        NotificationCenter.default.removeObserver(target)
    }
    
    static func post(name: Notification.Name, object: Any? = nil) {
        NotificationCenter.default.post(name: name, object: object)
    }
    
}
