//
//  UINavigationControllerExtension.swift
//  Common
//
//  Created by Stayout on 26.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public extension UINavigationController {
    
    convenience init?(rootViewController: UIViewController?) {
        guard let controller = rootViewController else { return nil }
        self.init()
        configure()
        setViewControllers([controller], animated: true)
    }
    
    convenience init?(router: RootRouter) {
        self.init(rootViewController: router.viewController)
        configure()
    }
    
    @discardableResult func configure() -> Self {
        navigationBar.isTranslucent = false
        navigationBar.shadowImage = UIImage()
        navigationBar.barTintColor = .orange
        navigationBar.tintColor = .white
        navigationBar.titleTextAttributes = [.foregroundColor : UIColor.white]
        return self
    }
    
}
