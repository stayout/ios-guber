//
//  CGFloat.swift
//  Common
//
//  Created by Stayout on 26.03.2021.
//

import UIKit

public extension CGFloat {
    
    var toRadians: CGFloat {
        return self * .pi / 180
    }
    
    var toDegrees: CGFloat {
        return self * 180 / .pi
    }
    
}
