//
//  CGAffineTransform.swift
//  Voice
//
//  Created by Stayout on 30.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public extension CGAffineTransform {
    
    static var hidden: CGAffineTransform {
        return CGAffineTransform(scaleX: 0.0000000001, y: 0.0000000001)
    }
    
    static var normal: CGAffineTransform {
        return .identity
    }
    
}

public extension UIView {
    
    func apply(transform: CGAffineTransform, duration: TimeInterval? = 0.33) {
        if let duration = duration {
            UIView.animate(withDuration: duration, animations: {
                self.transform = transform
            })
        }
        else {
            self.transform = transform
        }
    }
    
}
