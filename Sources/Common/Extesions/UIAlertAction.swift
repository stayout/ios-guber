//
//  UIAlertAction.swift
//  Voice
//
//  Created by Stayout on 31.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public extension UIAlertAction {
    
    convenience init(title: String?,
                     titleTextColor: UIColor? = .black,
                     titleTextAlignment: NSTextAlignment? = nil,
                     icon: UIImage? = nil,
                     style: UIAlertAction.Style = .default,
                     handler: ((UIAlertAction) -> Void)? = nil) {
        self.init(title: title, style: style, handler: handler)
        
        if let color = titleTextColor {
            setValue(color, forKey: "titleTextColor")
        }
        if let alignment = titleTextAlignment {
            setValue(alignment, forKey: "titleTextAlignment")
        }
        if let icon = icon {
            setValue(icon, forKey: "image")
        }
    }
    
    convenience init(title: String? = nil,
                     option: RootAlertOption? = nil,
                     style: UIAlertAction.Style = .default,
                     handler: ((UIAlertAction) -> Void)? = nil) {
        self.init(title: title ?? option?.name,
                  titleTextColor: option?.titleTextColor,
                  titleTextAlignment: option?.titleTextAlignment,
                  icon: option?.icon,
                  style: style, handler: handler)
    }
    
}
