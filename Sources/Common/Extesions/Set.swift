//
//  Set.swift
//  Voice
//
//  Created by Stayout on 16.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

extension Set where Iterator.Element: Hashable {
    public mutating func insert(_ items: [Iterator.Element]) {
        for item in items { insert(item) }
    }
}
