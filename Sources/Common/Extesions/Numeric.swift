//
//  Numeric.swift
//  Voice
//
//  Created by Stayout on 25.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public extension Numeric {
    
    func formatted(separator: String? = nil,
                   style: NumberFormatter.Style = .decimal,
                   locale: Locale = .current) -> String {
        let formatter = NumberFormatter()
        formatter.locale = locale
        formatter.numberStyle = style
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        if let separator = separator {
            formatter.groupingSeparator = separator
        }
        return formatter.string(for: self) ?? ""
    }
    
}
