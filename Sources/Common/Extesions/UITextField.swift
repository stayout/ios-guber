//
//  UITextFieldExtension.swift
//  Common
//
//  Created by Stayout on 29.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public extension UITextField {
    
    @IBInspectable
    var placeholderColor: UIColor? {
        get {
            return self.placeholderColor
        }
        set {
            let text: String = {
                guard let text = self.placeholder else { return "" }
                return text
            }()
            attributedPlaceholder = NSAttributedString(string: text, attributes: [.foregroundColor: newValue!])
        }
    }
    
    func setPlaceholder(text: String, font: UIFont, color: UIColor) {
        attributedPlaceholder = NSAttributedString(string: text, attributes: [
            .font: font,
            .foregroundColor: color
        ])
    }
    
}
