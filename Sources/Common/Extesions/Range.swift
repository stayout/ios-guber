//
//  Range.swift
//  Common
//
//  Created by Stayout on 10.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public extension ClosedRange where Element == Int {
    
    func toIndexes(section: Int = 0) -> [IndexPath] {
        return self.map({ IndexPath(row: $0, section: section) })
    }
    
}

public extension Range where Element == Int {
    
    func toIndexes(section: Int = 0) -> [IndexPath] {
        return self.map({ IndexPath(row: $0, section: section) })
    }
    
}
