//
//  Hashable.swift
//  Voice
//
//  Created by Stayout on 31.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

extension Hashable {
    
    public static var values: [Self] {
        return [Self](AnySequence { () -> AnyIterator<Self> in
            var raw = 0
            var first: Self?
            return AnyIterator {
                let current = withUnsafeBytes(of: &raw) { $0.load(as: Self.self) }
                if raw == 0 {
                    first = current
                } else if current == first {
                    return nil
                }
                raw += 1
                return current
            }
        })
    }
    
}
