//
//  DateExtension.swift
//  Common
//
//  Created by Stayout on 07.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation

public extension Date {
    var minute: Int { return Calendar.current.dateComponents([.minute], from: self).minute! }
    var hour: Int { return Calendar.current.dateComponents([.hour], from: self).hour! }
    var day: Int { return Calendar.current.dateComponents([.day], from: self).day! }
    var month: Int { return Calendar.current.dateComponents([.month], from: self).month! }
    var year: Int { return Calendar.current.dateComponents([.year], from: self).year! }
}

public extension Date {
    
    init?(day: Int, month: Int, year: Int, format: DateFormats = .defaultDate) {
        guard let date = DateFormatter(format: format).date(from: "\(day)-\(month)-\(year)") else { return nil }
        self = date
    }
    
    init?(_ description: String?, formatter: DateFormatter) {
        guard let description = description, let date = formatter.date(from: description) else { return nil }
        self = date
    }
    
    init?(_ description: String?, format: String) {
        self.init(description, formatter: DateFormatter(format: format))
    }
    
    init?(_ description: String?, format: DateFormats = .defaultDate) {
        self.init(description, format: format.rawValue)
    }
    
    static var current: Date {
        return Date()
    }
    
    static var currentUnixTime: Double {
        return current.timeIntervalSince1970
    }
    
    var iso8601: String {
        return DateFormatter.iso8601.string(from: self)
    }
    
    func formatted(by format: String) -> String {
        return DateFormatter(format: format).string(from: self)
    }
    
    func formatted(by format: DateFormats = .defaultDate) -> String {
        return formatted(by: format.rawValue)
    }

    var detailDescription: String {
//            Если дата сегодня: 18:21, 12:06
//            Если в дата в этом году: 15 авг / 23 сен / 16 янв
//            Если дата не в этом году: 14.02.2016 / 01.06.2015
        
        let format: DateFormats = {
            let currentDate = Date()
            if day == currentDate.day { return .time }
            else if year == currentDate.year { return .shortDate }
            return .fullDate
        }()
        
        return formatted(by: format)
    }
    
    var detailWithTimeDescription: String {
//            Если в дата в этом году: 15 авг / 23 сен / 16 янв
//            Если дата не в этом году: 14.02.2016 / 01.06.2015
//            Формирование времени в формате: 18:21, 12:06
        let format: DateFormats = year == Date().year ? .dayAndMonth : .fullDate
        return "\(formatted(by: format)), \(formatted(by: .time))"
    }
    
    var simpleDescription: String {
//            Если в дата в этом году: 15 авг / 23 сен / 16 янв
//            Если дата не в этом году: 14.02.2016 / 01.06.2015
        let format: DateFormats = year == Date().year ? .shortDate : .fullDate
        return formatted(by: format)
    }
    
    func offsetForDate(_ offsetDate: Date = Date.current) -> (description: String, isPositive: Bool) {
        let fromDate = self > offsetDate ? offsetDate : self
        let toDate = self > offsetDate ? self : offsetDate
        let isPositive = offsetDate < self ? true : false
        
        let components: Set<Calendar.Component> = [.month, .day, .hour, .minute]
        let difference = NSCalendar.current.dateComponents(components, from: fromDate, to: toDate)
        
        if let month = difference.month, month > 0 {
            return ("\(month) мес \(difference.day!) д", isPositive)
        }
        
        if let day = difference.day, day > 0 {
            return ("\(day) д \(difference.hour!) ч", isPositive)
        }
        
        if let hour = difference.hour, hour > 0 {
            return ("\(hour) ч \(difference.minute!) мин", isPositive)
        }
        
        if let minute = difference.minute, minute > 0 {
            return ("\(minute) мин", isPositive)
        }
        
        return ("", isPositive)
    }
    
    static var nextHour: Date {
        let unixTime = Date.currentUnixTime - Double( Date().minute * 60 ) + 3600
        return Date(timeIntervalSince1970: unixTime)
    }
    
}
