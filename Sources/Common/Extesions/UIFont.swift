//
//  UIFont.swift
//  Voice
//
//  Created by Stayout on 12/11/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

extension UIFont {
    
    private static let fontScale: CGFloat = {
        if Int(UIScreen.main.bounds.width) == 320 || Int(UIScreen.main.scale) == 1 {
            return 1
        }
        
        return 1.17
    }()
    
    internal class func scaledFontSize(_ value: CGFloat) -> CGFloat {
        return round(value * fontScale)
    }
    
}

public protocol UIFontProtocol {
    static func font(weight: UIFont.Weight, size: CGFloat) -> UIFont?
}
