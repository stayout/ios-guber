//
//  UserDefaultKeys.swift
//  Common
//
//  Created by Stayout on 02.09.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public enum UserDefaultsKeys: String {
    case nextAllowedPasswordResetCodeRequestTime
    case nextAllowedVerificationCodeRequestTime
    case quickBetOnboardingRequired
    case quickBetAvailable
    case quickBetAmount
    case quickBetAcceptOddChanges
}
