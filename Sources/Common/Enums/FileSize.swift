//
//  FileSize.swift
//  Common
//
//  Created by Stayout on 04.09.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation

public enum FileSize: Double {
    case kb = 1024
    case mb = 1048576
    
    public static func description(with size: Double?) -> String? {
        guard let size = size else { return nil }
        
        if size > FileSize.mb.rawValue {
            return "\(String(format: "%.f", size / FileSize.mb.rawValue)) Мб"
        }
        else if size > FileSize.kb.rawValue {
            return "\(String(format: "%.f", size / FileSize.kb.rawValue)) Кб"
        }
        else {
            return "\(String(format: "%.f", size)) байт"
        }
    }
    
}
