//
//  CookieName.swift
//  Common
//
//  Created by Stayout on 16.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public enum CookieName {
    
    case session
    
    public var description: String {
        switch self {
        case .session: return "JSESSIONID"
        }
    }
    
}
