//
//  DateFormatters.swift
//  Common
//
//  Created by Stayout on 31.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

public enum DateFormats: String {
    case defaultDate = "dd.MM.yyyy"
    case shortDate = "d MMM"
    case dayAndMonth = "d MMMM"
    case monthAndDay = "MMM dd"
    case fullDate = "MM.dd.YY h:mm a"
    case time = "h:mm a"
    case certificate = "dd MMMM yyyy"
    case dueDate = "d MMM yyyy"
    case dateWithTime = "dd.MM (HH:mm)"
    case dateWithTimeWithSeconds = "dd.MM (HH:mm:ss)"
    case requestDate = "yyyy-MM-dd"
    case transactionList = "dd.MM.yyyy / h:mm:ss a"
}
