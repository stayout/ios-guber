//
//  MimeTypes.swift
//  Common
//
//  Created by Stayout on 30.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public enum MimeTypes: String, Convertable {
    
    case gif = "image/gif"
    case jpeg = "image/jpeg"
    case pdf = "application/pdf"
    case png = "image/png"
    case doc = "application/msword"
    case docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    case xls = "application/vnd.ms-excel"
    case xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    case zip = "application/zip"
    case unknown
    
    public init?(value: Any?) {
        if let value = value as? String, let enumValue = MimeTypes(rawValue: value) {
            self = enumValue
        }
        else {
            return nil
        }
    }
    
    public var icon: UIImage? {
        return nil
    }
    
    public var isImage: Bool {
        return contains(.gif, .jpeg, .png)
    }
    
}
