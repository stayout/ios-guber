//
//  AnalyticEvents.swift
//  Common
//
//  Created by Stayout on 02.03.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import UIKit

public enum AnalyticEvents {
    
    case loginSuccess(auto: Bool)
    case loginError(auto: Bool, code: Int?)
    case betSuccessful(amount: Double, isQuickBet: Bool)
    case betError(code: Int?, isQuickBet: Bool)
    case paymentMethodOpen(source: String)
    
    public var name: String {
        switch self {
        case .loginSuccess: return "login_success"
        case .loginError: return "login_error_errorCode"
        case .betSuccessful: return "bet_Successful"
        case .betError: return "bet_Error"
        case .paymentMethodOpen: return "paymentMethod_open"
        }
    }
    
    public var parameters: JSONDictonary? {
        switch self {
        case .loginSuccess(let auto):
            return [
                "auto": auto
            ]
        case .loginError(let auto, let code):
            return [
                "auto": auto,
                "errorCode": code ?? -1
            ]
        case .betSuccessful(let amount, let isQuickBet):
            return [
                "amount": amount,
                "is_quick_bet": isQuickBet
            ]
        case .betError(let code, let isQuickBet):
            return [
                "errorCode": code ?? -1,
                "is_quick_bet": isQuickBet
            ]
        case .paymentMethodOpen(let source):
            return [
                "source": source
            ]
        }
    }
    
}

public extension NotificationCenter {
    
    static func logEvent(_ event: AnalyticEvents) {
        NotificationCenter.default.post(name: .logAnalyticEvent, object: event)
    }
    
}
