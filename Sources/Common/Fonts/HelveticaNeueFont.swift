//
//  HelveticaNeueFont.swift
//  Voice
//
//  Created by Stayout on 12/11/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public class HelveticaNeueFont: UIFontProtocol {
    
    public static func font(weight: UIFont.Weight, size: CGFloat) -> UIFont? {
        let name: String = {
            switch weight {
            case .regular: return "HelveticaNeue"
            case .medium: return "HelveticaNeue-Medium"
            case .bold: return "HelveticaNeue-CondensedBold"
            case .light: return "HelveticaNeue-Light"
            default: return "HelveticaNeue"
            }
        }()
        
        return UIFont(name: name, size: UIFont.scaledFontSize(size))
    }
    
}
