//
//  SystemFont.swift
//  Common
//
//  Created by Stayout on 18/12/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public class SystemFont: UIFontProtocol {
    
    public static func font(weight: UIFont.Weight, size: CGFloat) -> UIFont? {
        return UIFont.systemFont(ofSize: UIFont.scaledFontSize(size), weight: weight)
    }
    
}
