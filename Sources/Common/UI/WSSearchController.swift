//
//  WSSearchController.swift
//  Common
//
//  Created by Stayout on 03.04.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public class WSSearchController: UISearchController, WSOverlayViewDelegate {
    
    public var sourceViewController: UIViewController?
    public var tableView: RootTableView?
    
    public func overlayTapped() {
        isActive = false
        hideOverlay()
    }
    
    public func beginEditing() {
        guard searchBar.text?.isEmpty == true else { return }
        showOverlay()
    }
    
    public func showOverlay() {
        tableView?.overlayView?.set(alpha: 0.4, duration: 0.25)
    }
    
    public func hideOverlay() {
        tableView?.overlayView?.set(alpha: 0, duration: 0.25)
    }
    
}

public final class WSPaddingSearchController: WSSearchController {
    
    fileprivate let paddingSearchBar = WSSearchBar()
    
    override public var searchBar: UISearchBar {
        return paddingSearchBar
    }
    
}
