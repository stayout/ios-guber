//
//  WSTextField.swift
//  Voice
//
//  Created by Stayout on 10.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit
import VMaskTextField

open class WSTextField: VMaskTextField {
    
    public var padding: UIEdgeInsets = .zero
    open var textDidChanged: ((String?) -> Void)?
    open var textBeginEditing: (() -> Void)?
    
    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    convenience init() {
        self.init(frame: .zero)
        configure()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    open func configure() {
        self.mask = ""
        delegate = self
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
    }
    
    @objc open func editingChanged(_ textField: UITextField) {
        textDidChanged?(textField.text)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
}

extension WSTextField: UITextFieldDelegate {
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        textBeginEditing?()
    }
    
    public func textField(_ textField: UITextField,
                          shouldChangeCharactersIn range: NSRange,
                          replacementString string: String) -> Bool {
        if self.mask == "" {
            return true
        }
        let shouldChange = super.shouldChangeCharacters(in: range, replacementString: string)
        textDidChanged?(textField.text)
        return shouldChange
    }
    
}
