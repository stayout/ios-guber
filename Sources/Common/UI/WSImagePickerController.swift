//
//  WSImagePickerController.swift
//  Common
//
//  Created by Stayout on 05.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public protocol WSImagePickerOutputProtocol: class {
    func imageDidPicked(_ image: UIImage)
}

public class WSImagePickerController: UIImagePickerController {
    
    private weak var output: WSImagePickerOutputProtocol?
    
    public convenience init(type: UIImagePickerController.SourceType,
                            output: WSImagePickerOutputProtocol?) {
        self.init()
        sourceType = type
        delegate = self
        self.output = output
    }
    
}

extension WSImagePickerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            output?.imageDidPicked(image)
        }
        dismiss(animated: true)
    }
    
}
