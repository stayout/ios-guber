//
//  WSSearchBar.swift
//  Common
//
//  Created by Stayout on 16.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

class WSSearchBar: UISearchBar {
    
    @IBInspectable var textFieldBackgroundColor: UIColor? {
        didSet {
            textField?.backgroundColor = textFieldBackgroundColor
        }
    }
    
}
