//
//  BadgeImageView.swift
//  Voice 3
//
//  Created by Вячеслав Толстобров on 16/04/2019.
//  Copyright © 2019 White Soft. All rights reserved.
//

import UIKit

public class BadgeView: UIView {
    
    private let imageView = UIImageView()
    private let badgeLabel = BadgeLabel()
    
    public var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    public var badge: String? {
        didSet {
            badgeLabel.text = badge
        }
    }
    
    public convenience init(image: UIImage?, imageSize: CGSize) {
        self.init(frame: .zero)
  
        imageView.image = image
        add(subview: imageView, constraints: { maker in
            maker.height.equalTo(imageSize.height)
            maker.width.equalTo(imageSize.width)
            maker.bottom.equalToSuperview()
            maker.centerX.equalToSuperview()
        })
        
        add(subview: badgeLabel, constraints: { maker in
            maker.top.equalToSuperview()
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
            maker.height.equalTo(20)
        })
    }
    
}
