//
//  WSOverlayView.swift
//  Common
//
//  Created by Stayout on 18.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import SnapKit

public protocol WSOverlayViewDelegate {
    func overlayTapped()
}

public class WSOverlayView: UIView {
    
    private let visibleAlpha: CGFloat = 0.4
    public var animationDuration = 0.5
    public var delegate: WSOverlayViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .black
        alpha = 0
        toggleVisibility()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(overlayTapped)))
    }
    
    public init() {
        super.init(frame: .zero)
        backgroundColor = .black
        alpha = 0
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(overlayTapped)))
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard superview != nil else { return }
        self.snp.makeConstraints({
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.width.equalToSuperview()
            $0.height.equalToSuperview()
        })
    }
    
    @objc public func overlayTapped() {
        delegate?.overlayTapped()
    }
    
    public func show() {
        set(alpha: visibleAlpha, duration: animationDuration)
    }
    
    public func hide() {
        set(alpha: 0, duration: animationDuration)
    }
    
    public func toggleVisibility() {
        set(alpha: (alpha == 0) ? visibleAlpha : 0, duration: animationDuration)
    }
    
}
