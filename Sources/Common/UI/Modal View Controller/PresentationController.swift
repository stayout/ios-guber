//
//  PresentationController.swift
//  Common
//
//  Created by Stayout on 31.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

class PresentationController: UIPresentationController {
    
    private let blurEffectView: UIVisualEffectView
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.isUserInteractionEnabled = true
        blurEffectView.addGestureRecognizer(tapGestureRecognizer)
        blurEffectView.alpha = 0
    }
    
    @objc fileprivate func dismiss() {
        presentedViewController.dismiss(animated: true)
    }
    
    override func presentationTransitionWillBegin() {
        containerView?.addSubview(blurEffectView)
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.blurEffectView.alpha = 1
        })
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.blurEffectView.alpha = 0
        })
    }
    
}
