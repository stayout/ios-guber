//
//  Animator.swift
//  Common
//
//  Created by Stayout on 31.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit
import Foundation

enum TransitionType {
    case pushFromBottom
    case pushFromTop
    case pushFromRight
    case pushFromLeft
    case zoom
}

final class Animator: NSObject {
    //MARK: Private Properties
    fileprivate var presenting: Bool = true
    
    let duration: Double = 0.4
    let transitionType: TransitionType = .zoom
    let size: CGSize = {
        let screenSize = UIScreen.main.bounds
        let vMargin: CGFloat = 40
        let hMargin = max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) / 10
        return CGSize(width: screenSize.width - hMargin * 2, height: screenSize.height - vMargin * 2)
    }()
    
    fileprivate func center(for containerView : UIView) -> CGPoint {
        switch transitionType {
        case .pushFromBottom: return CGPoint(x: containerView.center.x, y: 3 * containerView.center.y)
        case .pushFromTop: return CGPoint(x: containerView.center.x, y: -3 * containerView.center.y)
        case .pushFromRight: return CGPoint(x: 3 * containerView.center.x, y: containerView.center.y)
        case .pushFromLeft: return CGPoint(x: -3 * containerView.center.x, y: containerView.center.y)
        case .zoom: return containerView.center
        }
    }
    
}

extension Animator: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let fromViewController = transitionContext.viewController(forKey: .from)!
        let toViewController = transitionContext.viewController(forKey: .to)!
        
        let animatingView = presenting ? toViewController.view : fromViewController.view
        animatingView?.frame.size = size
        
        if presenting {
            animatingView?.center = center(for: containerView)
            containerView.addSubview(animatingView!)
        }
        
        if transitionType == .zoom {
            animatingView?.transform = self.presenting ? .hidden : .normal
        }
        
        UIView.animate(withDuration: duration, animations: {
            animatingView?.center = self.presenting ? containerView.center : self.center(for: containerView)
            if self.transitionType == .zoom {
                animatingView?.transform = self.presenting ? .normal : .hidden
            }
        }, completion: { (finished) in
            self.presenting = !self.presenting
            transitionContext.completeTransition(finished)
        })
    }
    
}
