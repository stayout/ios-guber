//
//  UITransitionDelegate.swift
//  Common
//
//  Created by Stayout on 31.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

class UITransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    fileprivate let animator = Animator()
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.animator
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = PresentationController(presentedViewController: presented, presenting: source)
        return presentationController
    }
    
}
