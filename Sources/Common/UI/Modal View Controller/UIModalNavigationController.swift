//
//  UIModalNavigationController.swift
//  Common
//
//  Created by Stayout on 31.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

open class UIModalNavigationController: UINavigationController {

    let transitionDelegate = UITransitionDelegate()
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        configureAsModal(transitionDelegate: transitionDelegate)
        configure()
    }
    
    open func configureAsModal() -> Self {
        configureAsModal(transitionDelegate: transitionDelegate)
        configure()
        return self
    }
    
}
