//
//  KeyboardViewController.swift
//  Voice 3
//
//  Created by Stayout on 25.07.2018.
//  Copyright © 2018 White Soft. All rights reserved.
//

import UIKit

open class KeyboardViewController: UIViewController {
    
    deinit {
        NotificationCenter.removeObservers(self)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.addObserver(name: .keyboardWillShow, handler: { notification in
            guard var height = notification.keyboardInfoHeight else { return }
            if #available(iOS 11.0, *) {
                height -= self.view.safeAreaInsets.bottom
            }
            self.keyboardWillShow(duration: notification.keyboardInfoDuration, height: height)
        })
        
        NotificationCenter.addObserver(name: .keyboardWillHide, handler: { notification in
            self.keyboardWillHide(duration: notification.keyboardInfoDuration)
        })
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIApplication.hideKeyboard()
    }
    
    open func keyboardWillShow(duration: TimeInterval, height: CGFloat) {
    }
    
    open func keyboardWillHide(duration: TimeInterval) {
    }
    
}

extension Notification {
    
    fileprivate var keyboardIsLocal: Bool? {
        return userInfo?[UIResponder.keyboardIsLocalUserInfoKey] as? Bool
    }
    
    fileprivate var keyboardInfoHeight: CGFloat? {
        guard let keyboardRect = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect, keyboardIsLocal == true else { return nil }
        return keyboardRect.height
    }
    
    fileprivate var keyboardInfoDuration: TimeInterval {
        return userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0.25
    }
    
}
