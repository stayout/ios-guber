//
//  WSDocumentPickerViewController.swift
//  Common
//
//  Created by Stayout on 04.07.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public class WSDocumentPickerViewController: UIDocumentPickerViewController, UIDocumentPickerDelegate {
    
    private let pickerDelegate: WSDocumentPickerViewControllerProtocol?
    
    public init(pickerDelegate: WSDocumentPickerViewControllerProtocol?) {
        self.pickerDelegate = pickerDelegate
        super.init(documentTypes: ["public.content"], in: .import)
        delegate = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        if let data = try? Data(contentsOf: url) {
            pickerDelegate?.didSelectDocument(name: url.lastPathComponent, data: data)
        }
    }
    
}

public protocol WSDocumentPickerViewControllerProtocol {
    func didSelectDocument(name: String, data: Data)
}
