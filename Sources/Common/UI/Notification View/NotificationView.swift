//
//  NotificationView.swift
//  UI
//
//  Created by Stayout on 08/11/2018.
//  Copyright © 2018 White Soft. All rights reserved.
//

import UIKit

public class NotificationView: UIView {
    
    @IBOutlet private weak var title: UILabel!
    
    public func add(for view: UIView?) {
        title.apply(family: HelveticaNeueFont.self, weight: .regular, size: 16)
        title.textColor = .white
        layer.cornerRadius = 5
        hide(animated: false)
        view?.addSubview(self)
    }
    
    public func present(title: String?, background: UIColor?, duration: Double = 3) {
        self.title.text = title
        backgroundColor = background
        show()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration, execute: {
            self.hide()
        })
    }
    
    private func hide(animated: Bool = true) {
        let duration = animated ? 0.33 : 0
        apply(transform: .hidden, duration: animated ? 0.33 : 0)
        set(isHidden: true, duration: duration)
    }
    
    private func show(animated: Bool = true) {
        let duration = animated ? 0.33 : 0
        apply(transform: .normal, duration: duration)
        set(isHidden: false, duration: duration)
    }
    
}
