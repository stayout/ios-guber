//
//  ProgressButton.swift
//  Voice
//
//  Created by Stayout on 29.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

open class ProgressButton: WSCornerButton {
    
    private let circularProgressLayer = CAShapeLayer()
    public var progressLayerBorderWidth: CGFloat = 4.0
    public var progressLayerBorderColor = UIColor.white.cgColor
    
    public var progress: CGFloat = 0.0 {
        didSet {
            circularProgressLayer.strokeEnd = progress
            if progress == 1 {
                UIView.animate(withDuration: 0.33, animations: {
                    self.circularProgressLayer.opacity = 0
                })
            }
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareCircularLayer()
    }
    
    private func prepareCircularLayer() {
        circularProgressLayer.frame = CGRect(origin: .zero, size: layer.bounds.size)
        circularProgressLayer.isHidden = false
        circularProgressLayer.backgroundColor = UIColor.clear.cgColor
        circularProgressLayer.path = circlePath().cgPath
        circularProgressLayer.strokeStart = 0
        circularProgressLayer.strokeEnd = 0
        circularProgressLayer.lineWidth = progressLayerBorderWidth
        circularProgressLayer.fillColor = UIColor.clear.cgColor
        circularProgressLayer.strokeColor = progressLayerBorderColor
        
        layer.addSublayer(circularProgressLayer)
    }
    
    private func circlePath() -> UIBezierPath {
        let radius = circularProgressLayer.bounds.height / 2 - borderWidth / 2
        let arcCenterXY = radius + borderWidth / 2
        let arcCenter = CGPoint(x: arcCenterXY, y: arcCenterXY)
        let startAngle: CGFloat = CGFloat(-Double.pi / 2)
        let endAngle = startAngle + CGFloat(Double.pi * 2)
        let path = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        return path
    }
    
}
