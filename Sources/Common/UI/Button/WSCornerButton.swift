//
//  WSCornerButton.swift
//  Common
//
//  Created by Stayout on 26.12.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

open class WSCornerButton: UIButton {
    
    public var additionalInfo: Any?
    
    @IBInspectable var borderRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = borderRadius
            layer.masksToBounds = borderRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}
