//
//  WSButton.swift
//  Voice
//
//  Created by Stayout on 03.09.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

open class WSButton: WSCornerButton {
    
    public var tappedHandler: ((_ sender: UIButton) -> Void)?
    
    public convenience init(backgroundColor: UIColor? = nil,
                            tintColor: UIColor? = nil,
                            title: String?,
                            font: UIFont? = nil,
                            handler: ((_ sender: UIButton) -> Void)? = nil) {
        self.init(type: .system)
        self.backgroundColor = backgroundColor
        self.tintColor = tintColor
        setTitle(title, for: .normal)
        if let font = font {
            titleLabel?.font = font
        }
        add(handler: handler)
    }
    
    public convenience init(backgroundColor: UIColor? = nil,
                            tintColor: UIColor? = nil,
                            icon: UIImage? = nil,
                            handler: ((_ sender: UIButton) -> Void)? = nil) {
        self.init(type: .system)
        self.backgroundColor = backgroundColor
        self.tintColor = tintColor
        setImage(icon, for: .normal)
        add(handler: handler)
    }
    
    func add(handler: ((_ sender: UIButton) -> Void)? = nil) {
        addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        if let handler = handler {
            self.tappedHandler = handler
        }
    }
    
    @objc private func buttonTapped(_ sender: UIButton) {
        tappedHandler?(sender)
    }
    
}
