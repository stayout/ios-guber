//
//  WSBarButtonItem.swift
//  Voice
//
//  Created by Stayout on 01.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public class WSBarButtonItem: UIBarButtonItem {
    
    private var handler: ((_ sender: UIBarButtonItem) -> Void)?
    private var badgeLabel: BadgeLabel?
    
    public convenience init(title: String?,
                            style: UIBarButtonItem.Style = .plain,
                            handler: ((_ sender: UIBarButtonItem) -> Void)? = nil) {
        self.init(title: title, style: style, target: nil, action: nil)
        add(handler: handler)
    }
    
    public convenience init(image: UIImage?,
                            style: UIBarButtonItem.Style = .plain,
                            handler: ((_ sender: UIBarButtonItem) -> Void)? = nil) {
        self.init(image: image, style: style, target: nil, action: nil)
        add(handler: handler)
    }
    
    public convenience init(systemItem: UIBarButtonItem.SystemItem,
                            handler: ((_ sender: UIBarButtonItem) -> Void)? = nil) {
        self.init(barButtonSystemItem: systemItem, target: nil, action: nil)
        add(handler: handler)
    }
    
    private func add(handler: ((_ sender: UIBarButtonItem) -> Void)?) {
        self.target = self
        self.action = #selector(buttonTapped)
        self.handler = handler
    }
    
    @objc private func buttonTapped(_ sender: UIBarButtonItem) {
        handler?(sender)
    }
    
}
