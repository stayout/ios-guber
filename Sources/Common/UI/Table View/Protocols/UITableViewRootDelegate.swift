//
//  UITableViewRootDelegate.swift
//  Common
//
//  Created by Stayout on 13.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol UITableViewRootDelegate {
    var tableView: RootTableView? { get set }
}
