//
//  UITableViewSearchDelegate.swift
//  Common
//
//  Created by Stayout on 02.04.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol UITableViewSearchDelegate {
    var searchController: WSSearchController! { get set }
    var searchBar: UISearchBar { get }
    func showOverlayView()
    func hideOverlayView()
}

public extension UITableViewSearchDelegate where Self: UIViewController {
    
    var searchBar: UISearchBar {
        return searchController.searchBar
    }
    
    func showOverlayView() {
        searchController.showOverlay()
    }
    
    func hideOverlayView() {
        guard searchBar.text?.isEmpty == false else { return }
        searchController.hideOverlay()
    }
    
    func addSearchController(for tableView: RootTableView?) {
        if UIDevice.isPhone {
            edgesForExtendedLayout = .all
            extendedLayoutIncludesOpaqueBars = false
        }
        
        searchController.sourceViewController = self
        searchController.tableView = tableView
        searchController.searchBar.placeholder = nil
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = true
        
        searchBar.textField?.backgroundColor = UIColor("F4F4F6")
        searchBar.backgroundColor = .white
        searchBar.tintColor = .blue
        
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = self.searchController
        
        addOverlayView(for: tableView, searchController: searchController)
    }
    
    func addOverlayView(for tableView: RootTableView?, searchController: WSSearchController?) {
        let overlay = WSOverlayView()
        tableView?.overlayView = overlay
        tableView?.superview?.addSubview(overlay)
        tableView?.overlayView?.delegate = searchController
        tableView?.overlayView?.animationDuration = 0.25
    }
    
}
