//
//  UITableViewContentOperationsProtocol.swift
//  Common
//
//  Created by Stayout on 11.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol UITableViewContentOperationsProtocol {
    
    func update(cells: [RootCellContent]?, completion: (() -> Void)?)
    func reloadData()
    
    func reload(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func reload(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func reload(sectionIndexes: [Int], cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func reload(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    
    func insert(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func insert(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func insert(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    
    func remove(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func remove(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    func remove(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation)
    
}
