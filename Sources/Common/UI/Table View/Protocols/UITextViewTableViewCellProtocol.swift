//
//  UITextViewTableViewCellProtocol.swift
//  Common
//
//  Created by Stayout on 11/03/2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

public protocol UITextViewTableViewCellProtocol {
    var textView: WSTextView! { get set }
}
