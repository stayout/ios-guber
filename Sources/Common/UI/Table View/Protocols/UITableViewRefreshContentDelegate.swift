//
//  UITableViewRefreshContentDelegate.swift
//  Common
//
//  Created by Stayout on 13.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol UITableViewRefreshContentDelegate: UITableViewRootDelegate {
    func willUpdateContent()
    func didUpdateContent()
}

public extension UITableViewRefreshContentDelegate {
    func didUpdateContent() {
        DispatchQueue.main.async {
            self.tableView?.customRefreshControl.endRefreshing()
        }
    }
}

public extension UIViewController {
    
    var tableRefreshDelegate: UITableViewRefreshContentDelegate? {
        return self as? UITableViewRefreshContentDelegate
    }
    
}
