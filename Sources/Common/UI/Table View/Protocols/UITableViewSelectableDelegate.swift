//
//  UITableViewSelectableDelegate.swift
//  Common
//
//  Created by Stayout on 13.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation

public protocol UITableViewSelectableDelegate {
    func didSelectItem(by indexPath: IndexPath)
}
