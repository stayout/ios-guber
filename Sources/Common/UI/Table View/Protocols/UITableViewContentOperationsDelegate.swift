//
//  UITableViewContentOperationsDelegate.swift
//  Common
//
//  Created by Stayout on 13.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol UITableViewContentOperationsDelegate: UITableViewContentOperationsProtocol, UITableViewRootDelegate {
    
}

public extension UITableViewContentOperationsDelegate {
    
    func update(cells: [RootCellContent]?, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            self.tableView?.customRefreshControl.endRefreshing()
            self.tableView?.set(cells: cells ?? [])
            completion?()
        }
    }
    
    func reloadData() {
        tableView?.reloadData()
    }
    
    func reload(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        reload(sectionIndexSet: IndexSet(integer: sectionIndex), cells: cells, with: animation)
    }
    
    func reload(sectionIndexes: [Int], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        reload(sectionIndexSet: IndexSet(sectionIndexes), cells: cells, with: animation)
    }
    
    func reload(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        reload(sectionIndexSet: IndexSet(sectionIndexes), cells: cells, with: animation)
    }
    
    private func reload(sectionIndexSet: IndexSet, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        tableView?.set(cells: cells ?? [], reloadDataIfNeeded: false)
        tableView?.beginUpdates()
        tableView?.reloadSections(sectionIndexSet, with: animation)
        tableView?.endUpdates()
    }
    
    func reload(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        tableView?.set(cells: cells ?? [], reloadDataIfNeeded: false)
        tableView?.beginUpdates()
        tableView?.reloadRows(at: rowIndexes, with: animation)
        tableView?.endUpdates()
    }
    
    func insert(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        insert(sectionIndexSet: IndexSet(sectionIndexes), cells: cells, with: animation)
    }
    
    func insert(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        insert(sectionIndexSet: IndexSet(integer: sectionIndex), cells: cells, with: animation)
    }
    
    private func insert(sectionIndexSet: IndexSet, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        tableView?.set(cells: cells ?? [], reloadDataIfNeeded: false)
        tableView?.beginUpdates()
        tableView?.insertSections(sectionIndexSet, with: animation)
        tableView?.endUpdates()
    }
    
    func insert(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        tableView?.set(cells: cells ?? [], reloadDataIfNeeded: false)
        tableView?.beginUpdates()
        tableView?.insertRows(at: rowIndexes, with: animation)
        tableView?.endUpdates()
    }
    
    func remove(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        remove(sectionIndexSet: IndexSet(sectionIndexes), cells: cells, with: animation)
    }
    
    func remove(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        remove(sectionIndexSet: IndexSet(integer: sectionIndex), cells: cells, with: animation)
    }
    
    private func remove(sectionIndexSet: IndexSet, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        tableView?.set(cells: cells ?? [], reloadDataIfNeeded: false)
        tableView?.beginUpdates()
        tableView?.deleteSections(sectionIndexSet, with: animation)
        tableView?.endUpdates()
    }
    
    func remove(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        tableView?.set(cells: cells ?? [], reloadDataIfNeeded: false)
        tableView?.beginUpdates()
        tableView?.deleteRows(at: rowIndexes, with: animation)
        tableView?.endUpdates()
    }
    
}

public extension UITableViewContentOperationsDelegate where Self: UIViewController {
    
    func addBottomInsetIfRequired(value: CGFloat) {
        if self.tabBarController != nil {
            let inset = UIEdgeInsets(top: 0, left: 0, bottom: value, right: 0)
            tableView?.contentInset = inset
            tableView?.scrollIndicatorInsets = inset
        }
    }
    
}
