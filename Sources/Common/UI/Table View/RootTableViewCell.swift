//
//  RootTableViewCell.swift
//  Common
//
//  Created by Stayout on 02.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

open class RootTableViewCell: UITableViewCell {
    
    public var tableView: RootTableView?
    public var indexPath: IndexPath?
    open var isConfigured = false
    open var selectionColor: UIColor = .white {
        didSet {
            selectedBackgroundView?.backgroundColor = selectionColor
        }
    }
    
    open func configure() {
        isConfigured = true
    }
    
    open func update<C: RootCellContent>(by content: C, animDuration: Double = 0) { }
    
}
