//
//  RootTableView.swift
//  Common
//
//  Created by Stayout on 02.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

open class RootTableView: UITableView {
    
    fileprivate var internalContentCells = [RootCellContent]()
    
    open var headerView: UIView?
    public let preloaderView: WSPreloaderView = .fromNib()
    public var overlayView: WSOverlayView?
    public let customRefreshControl = UIRefreshControl()
    
    public var selectableDelegate: UITableViewSelectableDelegate?
    public var refreshContentDelegate: UITableViewRefreshContentDelegate?
    
    public var headerHeight: CGFloat?
    public var footerHeight: CGFloat?
    
    public var cells: [RootCellContent] {
        get { return internalContentCells }
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        configure()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        configure()
    }
    
    open func configure() {
        delegate = self
        dataSource = self
        estimatedRowHeight = 40
        rowHeight = UITableView.automaticDimension
    }
    
    public func addRefreshControl(delegate: UITableViewRefreshContentDelegate?) {
        self.refreshContentDelegate = delegate
        customRefreshControl.addTarget(self, action: #selector(willUpdateContent), for: .valueChanged)
        refreshControl = customRefreshControl
        bringSubviewToFront(customRefreshControl)
    }
    
    open func set(cells: [RootCellContent],
                  reloadDataIfNeeded: Bool = true,
                  scrollToTop: Bool = false) {
        self.internalContentCells = cells
        if reloadDataIfNeeded {
            reloadData()
        }
        if scrollToTop {
            layoutIfNeeded()
            self.scrollToTop(animated: true)
        }
    }
    
    public func update() {
        UIView.performWithoutAnimation {
            self.beginUpdates()
            self.endUpdates()
        }
    }
    
    public func update(with textView: UITextView) {
        let prevContentOffset = contentOffset
        update()
        contentOffset = prevContentOffset
        let caretRect = textView.caretRect(for: textView.selectedTextRange!.start)
        scrollRectToVisible(textView.convert(caretRect, to: self), animated: false)
    }
    
    public func scrollToTop(delay: Double = 0, animated: Bool = false) {
        if cells.count > 0 {
            scrollTo(row: 0, position: .top, delay: delay, animated: animated)
        }
    }
    
    public func scrollToBottom(delay: Double = 0, animated: Bool = false) {
        if cells.count > 0 {
            scrollTo(row: cells.count - 1, position: .bottom, delay: delay, animated: animated)
        }
    }
    
    public func scrollTo(row: Int, section: Int = 0, position: UITableView.ScrollPosition, delay: Double = 0, animated: Bool = false) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: {
            let indexPath = IndexPath(row: row, section: section)
            self.scrollToRow(at: indexPath, at: position, animated: animated)
        })
    }
    
    public var firstVisibleCell: IndexPath? {
        guard let cell = visibleCells.first else { return nil }
        return indexPath(for: cell)
    }
    
    public var lastVisibleCell: IndexPath? {
        guard let cell = visibleCells.last else { return nil }
        return indexPath(for: cell)
    }
    
    @objc fileprivate func willUpdateContent() {
        refreshContentDelegate?.willUpdateContent()
    }
    
    public func textViewBecomeFirstResponder<T>(cellType: T.Type,
                                                delay: Double = 0) where T: UITextViewTableViewCellProtocol {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: {
            guard let cells = self.visibleCells.filter({ $0 is T }) as? [T] else { return }
            let cell = cells.first(where: { $0.textView.isEditable })
            cell?.textView.becomeFirstResponder()
        })
    }
    
    open override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> RootTableViewCell {
        let cell = super.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RootTableViewCell
        cell.configure()
        return cell
    }
    
    open override func cellForRow(at indexPath: IndexPath?) -> UITableViewCell? {
        guard let indexPath = indexPath else { return nil }
        return super.cellForRow(at: indexPath)
    }
    
}

// MARK: - Table view data source
extension RootTableView: UITableViewDataSource {
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return cells.type == .grouped ? cells.count : 1
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.type == .grouped ? cells[section].subItems.count : cells.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let content = cells.type == .grouped ? cells[indexPath.section].subItems[indexPath.row] : cells[indexPath.row]
        let cell = dequeueReusableCell(withIdentifier: content.identifier, for: indexPath)
        cell.tableView = self
        cell.indexPath = indexPath
        cell.update(by: content, animDuration: 0)
        return cell
    }
    
}

// MARK: - Table view delegate
extension RootTableView: UITableViewDelegate {
    
    open func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    open func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return footerHeight ?? 0.000000000001
    }
    
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if headerView != nil {
            return UITableView.automaticDimension
        }
        return headerHeight ?? (cells.type == .grouped ? 44 : 0.000000000001)
    }
    
    open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if cells.type == .grouped, headerView == nil {
            return cells[section].title
        }
        return nil
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    open func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIApplication.hideKeyboard()
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectableDelegate?.didSelectItem(by: indexPath)
        deselectRow(at: indexPath, animated: true)
    }
    
}
