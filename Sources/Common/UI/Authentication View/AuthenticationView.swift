//
//  AuthenticationView.swift
//  Common
//
//  Created by Stayout on 21.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit
import SnapKit

public final class AuthenticationView: UIView, VisibilityProtocol {
    
    @IBOutlet private weak var signInButton: WSButton!
    @IBOutlet private weak var signUpButton: WSButton!
    
    public var signInClosure: (() -> Void)?
    public var signUpClosure: (() -> Void)?
    
    public var signInTitle: String? = Strings.common.btnsSignIn() {
        didSet {
            signInButton.setTitle(signInTitle, for: .normal)
        }
    }
    
    public var signUpTitle: String? = Strings.common.btnsSignUp() {
        didSet {
            signUpButton.setTitle(signUpTitle, for: .normal)
        }
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard superview != nil else { return }
        set(isHidden: true, duration: 0)
        snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }
    
    @IBAction private func signInButtonDidTapped(_ sender: UIButton) {
        signInClosure?()
    }
    
    @IBAction private func signUpButtonDidTapped(_ sender: UIButton) {
        signUpClosure?()
    }
    
}
