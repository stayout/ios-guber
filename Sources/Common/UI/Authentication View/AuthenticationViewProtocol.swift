//
//  AuthenticationViewProtocol.swift
//  Common
//
//  Created by Stayout on 21.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

public protocol AuthenticationViewProtocol {
    var authenticationView: AuthenticationView { get set }

    func showAuthenticationView()
    func showAuthenticationView(animated: Bool)
    func hideAuthenticationView()
    func hideAuthenticationView(animated: Bool)
}

public extension AuthenticationViewProtocol {
    
    func showAuthenticationView() {
        showAuthenticationView(animated: false)
    }
    
    func showAuthenticationView(animated: Bool) {
        authenticationView.show(animated: animated)
    }

    func hideAuthenticationView() {
        hideAuthenticationView(animated: false)
    }
    
    func hideAuthenticationView(animated: Bool) {
        authenticationView.hide(animated: animated)
    }
    
}
