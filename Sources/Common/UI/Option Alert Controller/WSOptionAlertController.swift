//
//  WSOptionAlertController.swift
//  Common
//
//  Created by Stayout on 21.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import Foundation

public class WSOptionAlertController {
    
    fileprivate let alertController: UIAlertController
    public var didSelectOption: ((_ option: RootAlertOption) -> Void)?
    
    public init(title: String? = nil,
                options: [RootAlertOption]) {
        alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        for option in options {
            let action = UIAlertAction(option: option, handler: { _ in
                self.didSelectOption?(option)
            })
            
            alertController.addAction(action)
        }
        
        let cancel = UIAlertAction(title: "Cancel", titleTextColor: .orange, style: .cancel)
        alertController.addAction(cancel)
    }
    
    public func dismiss() {
        alertController.dismiss(animated: true)
    }
    
    public func present(from viewController: UIViewController?) {
        viewController?.present(controller: alertController)
    }
    
    public func present(from viewController: UIViewController?, sender: UIView?) {
        if let popoverController = alertController.popoverPresentationController, let sender = sender {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        viewController?.present(controller: alertController)
    }
    
    public func present(from viewController: UIViewController?, sender: UIButton?) {
        if let popoverController = alertController.popoverPresentationController, let sender = sender {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        viewController?.present(controller: alertController)
    }
    
    public func present(from viewController: UIViewController?, sender: UIBarButtonItem?) {
        if let popoverController = alertController.popoverPresentationController, let sender = sender {
            popoverController.barButtonItem = sender
        }
        viewController?.present(controller: alertController)
    }
    
}
