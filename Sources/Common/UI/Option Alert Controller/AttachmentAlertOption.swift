//
//  AttachmentAlertOption.swift
//  Common
//
//  Created by Stayout on 13.09.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import Foundation

public enum AttachmentAlertOptionType {
    
    case gallery
    case camera
    
    public var description: String? {
        switch self {
        case .gallery: return "Gallery"
        case .camera: return "Camera"
        }
    }
    
}

public class AttachmentAlertOption: RootAlertOption {
    
    public let type: AttachmentAlertOptionType
    
    public init(name: String? = nil, type: AttachmentAlertOptionType) {
        self.type = type
        super.init(name: name ?? type.description)
    }
    
}
