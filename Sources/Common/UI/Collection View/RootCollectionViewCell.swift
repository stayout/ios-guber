//
//  RootCollectionViewCell.swift
//  Common
//
//  Created by Stayout on 17.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

open class RootCollectionViewCell: UICollectionViewCell {
    
    public var tableView: RootCollectionView?
    public var indexPath: IndexPath?
    private var isConfigured = false
    
    open func update(content: RootCellContent?) {
        if isConfigured == false {
            configure()
        }
    }
    
    open func configure() {
        isConfigured = true
    }
    
}
