//
//  UICollectionViewSelectableDelegate.swift
//  Common
//
//  Created by Stayout on 17.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation

public protocol UICollectionViewSelectableDelegate {
    func didSelectItem(by indexPath: IndexPath)
    func didCheckedItem(by indexPath: IndexPath)
}

extension UICollectionViewSelectableDelegate {
    public func didCheckedItem(by indexPath: IndexPath) { }
}
