//
//  RootCollectionView.swift
//  Common
//
//  Created by Stayout on 17.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

open class RootCollectionView: UICollectionView {
    
    fileprivate var internalcontentCells = [RootCellContent]()
    public var selectableDelegate: UICollectionViewSelectableDelegate?
    
    private var separatorWidth: CGFloat = 2
    private var columnCount: CGFloat?
    
    open var cellSize: CGSize {
        if let count = self.columnCount {
            let width = (frame.size.width - separatorWidth * (count + 1)) / count
            return CGSize(width: width, height: width)
        }
        return .zero
    }
    
    open var cells: [RootCellContent] {
        return internalcontentCells
    }
    
    public func setCells(_ internalcontentCells: [RootCellContent],
                         reloadDataIfNeeded: Bool = true,
                         scrollToTop: Bool = false) {
        self.internalcontentCells = internalcontentCells
        if reloadDataIfNeeded {
            reloadData()
        }
        if scrollToTop {
            layoutIfNeeded()
            setContentOffset(.zero, animated: true)
        }
    }
    
    public func configure(separatorWidth: CGFloat = 2,
                          columnCount: CGFloat) {
        self.separatorWidth = separatorWidth
        self.columnCount = columnCount
    }
    
    public func scrollToBottom(animated: Bool) {
        if cells.count > 0 {
            let indexPath = IndexPath(row: cells.count - 1, section: 0)
            self.scrollToItem(at: indexPath, at: .top, animated: animated)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
        dataSource = self
    }
    
}

extension RootCollectionView: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }

}

extension RootCollectionView: UICollectionViewDataSource {
   
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cells.count
    }
 
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let content = cells[indexPath.row]
        let cell = dequeueReusableCell(withReuseIdentifier: content.identifier, for: indexPath) as! RootCollectionViewCell
        cell.configure()
        cell.update(content: content)
        return cell
    }

}

extension RootCollectionView: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectableDelegate?.didSelectItem(by: indexPath)
    }
    
}
