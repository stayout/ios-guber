//
//  WSTextView.swift
//  Common
//
//  Created by Stayout on 15.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit

open class WSTextView: UITextView {
    
    @IBInspectable public var defaultTextColor: UIColor?
    @IBInspectable public var placeholderTextColor: UIColor?
    @IBInspectable public var placeholderText: String? = "" {
        didSet {
            if text.isEmpty {
                text = placeholderText
            }
        }
    }
    
    override open var text: String! {
        didSet {
            if text == placeholderText {
                textColor = placeholderTextColor
            }
            else if text.isEmpty {
                textColor = defaultTextColor
            }
        }
    }
    private var symbolLimit: Int?
    private var symbolLimitClosure: (() -> Void)?
    open var textDidChanged: ((String) -> Void)?
    open var textBeginEditing: (() -> Void)?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
    }
    
    public func add(symbolLimit: Int, closure: @escaping () -> Void) {
        self.symbolLimit = symbolLimit
        symbolLimitClosure = closure
    }
    
}

extension WSTextView: UITextViewDelegate {
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textBeginEditing?()
        return true
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if text == placeholderText { text = "" }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if text.isEmpty { text = placeholderText }
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        textDidChanged?(textView.text)
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let symbolLimit = symbolLimit, let closure = self.symbolLimitClosure, self.text.count + text.count > symbolLimit {
            closure()
            return false
        }
        
        return true
    }

}
