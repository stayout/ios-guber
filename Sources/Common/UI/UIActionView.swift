//
//  UIActionView.swift
//  Common
//
//  Created by Stayout on 06.03.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

class UIActionView: UIView {
    
    var title: UILabel? {
        get {
            return subviews.filter({ $0 is UILabel }).first as? UILabel
        }
    }
    
    var button: UIButton? {
        get {
            return subviews.filter({ $0 is UIButton }).first as? UIButton
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        button?.addTarget(self, action: #selector(toggleTitleHighlight), for: .touchDown)
        button?.addTarget(self, action: #selector(toggleTitleHighlight), for: [.touchDragOutside, .touchUpInside, .touchCancel, .touchUpOutside])
    }
    
    @objc func toggleTitleHighlight() {
        title?.set(alpha: self.title?.alpha == 1 ? 0.5 : 1, duration: 0.125)
    }
    
}
