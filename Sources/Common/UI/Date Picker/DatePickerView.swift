//
//  CustomDatePickerView.swift
//  Common
//
//  Created by Stayout on 16.11.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import SnapKit

protocol DatePickerChangedValueDelegate {
    func dateChanged(date: Date, datePicker: DatePickerView)
}

protocol DatePickerActionsDelegate {
    func rightButtonTapped(datePicker: DatePickerView)
    func leftButtonTapped(datePicker: DatePickerView)
}

class DatePickerView: UIView, WSOverlayViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightActionButton: UIButton!
    @IBOutlet weak var leftActionButton: UIButton!
    @IBOutlet fileprivate weak var datePicker: UIDatePicker!
    @IBOutlet fileprivate weak var overlayView: WSOverlayView!
    @IBOutlet fileprivate weak var datePickerContainerViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate weak var bottomMargin: NSLayoutConstraint!
    
    var changedValueDelegate: DatePickerChangedValueDelegate?
    var actionsDelegate: DatePickerActionsDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        alpha = 0
    }

    func setOptions(_ options: DatePickerDataOptions) {
        datePicker.locale = Locale(identifier: "ru")
        datePicker.minimumDate = options.minDate
        datePicker.maximumDate = options.maxDate
        datePicker.date = options.currentDate
        datePicker.datePickerMode = options.mode
    }
    
    func configureConstraint() {
        self.snp.makeConstraints({
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        })
        
        bottomMargin.constant = -datePickerContainerViewHeight.constant
        overlayView.delegate = self
        overlayView.toggleVisibility()
        datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
    }
    
    @objc func datePickerChanged(picker: UIDatePicker) {
        changedValueDelegate?.dateChanged(date: picker.date, datePicker: self)
    }
    
    @IBAction func rightActionButtonTapped(_ sender: UIButton) {
        actionsDelegate?.rightButtonTapped(datePicker: self)
    }
    
    @IBAction func leftActionButtonTapped(_ sender: UIButton) {
        actionsDelegate?.leftButtonTapped(datePicker: self)
    }
    
    func overlayTapped() {
        bottomMargin.constant = alpha == 0 ? 0 : -datePickerContainerViewHeight.constant
        set(alpha: self.alpha == 0 ? 1 : 0, duration: 0.5)
        layoutIfNeeded(duration: 0.5)
        overlayView.toggleVisibility()
    }
    
}
