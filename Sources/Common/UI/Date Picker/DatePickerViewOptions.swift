//
//  DatePickerViewOptions.swift
//  Common
//
//  Created by Stayout on 27.11.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import Foundation

struct DatePickerDataOptions {
    let minDate: Date
    let maxDate: Date
    let currentDate: Date
    let mode: UIDatePicker.Mode
}
