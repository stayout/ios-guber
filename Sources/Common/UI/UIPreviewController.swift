//
//  UIPreviewController.swift
//  Common
//
//  Created by Stayout on 07.03.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit
import QuickLook

public class PreviewFile: NSObject, QLPreviewItem {
    
    public var previewItemURL: URL?
    public var previewItemTitle: String?
    
    public init(url: URL? = nil, title: String? = nil) {
        previewItemURL = url
        previewItemTitle = title
    }
}


public class UIPreviewController: QLPreviewController {
    
    fileprivate var items = [PreviewFile]()
    
    convenience public init(modalPresentationStyle: UIModalPresentationStyle = .custom) {
        self.init()
        self.modalPresentationStyle = modalPresentationStyle
    }
    
    public func set(_ item: PreviewFile, isNeedReload: Bool = true) -> Self {
        return set( [item] )
    }
    
    public func set(_ items: [PreviewFile], isNeedReload: Bool = true) -> Self {
        self.items = items
        dataSource = self
        delegate = self
        if isNeedReload { reloadData() }
        return self
    }
    
}

extension UIPreviewController: QLPreviewControllerDataSource {
    
    public func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return items.count
    }
    
    public func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        let item = items[index]
        return item
    }
    
}

extension UIPreviewController: QLPreviewControllerDelegate {
}
