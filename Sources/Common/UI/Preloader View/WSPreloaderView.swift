//
//  WSPreloaderView.swift
//  Common
//
//  Created by Stayout on 03.12.2017.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import UIKit
import Lottie

public class WSPreloaderView: UIView {

    fileprivate let animDuration = 0.35
    private var animationView: AnimationView?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        set(isHidden: true, duration: 0)
        animationView = AnimationView(name: "PreloaderView", bundle: bundle)
        
        add(subview: animationView, constraints: { maker in
            maker.centerX.equalToSuperview()
            maker.centerY.equalToSuperview()
            maker.height.equalTo(40)
            maker.width.equalTo(160)
        })
        animationView?.loopMode = .loop
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard superview != nil else { return }
        snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }
    
    public func show(animated: Bool = true) {
        DispatchQueue.main.async {
            self.set(isHidden: false, duration: animated ? self.animDuration : 0)
            self.animationView?.play()
        }
    }
    
    public func hide(animated: Bool = true) {
        DispatchQueue.main.async {
            self.set(isHidden: true, duration: animated ? self.animDuration : 0)
            self.animationView?.stop()
        }
    }
    
}
