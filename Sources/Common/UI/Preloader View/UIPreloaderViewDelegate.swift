//
//  UIPreloaderViewDelegate.swift
//  Common
//
//  Created by Stayout on 13.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol UIPreloaderViewDelegate: class {
    var preloaderView: WSPreloaderView { get set }
    
    func showPreloader()
    func showPreloader(animated: Bool)
    func hidePreloader()
    func hidePreloader(animated: Bool)

}

public extension UIPreloaderViewDelegate where Self: UIViewController {
    
    func showPreloader() {
        showPreloader(animated: true)
    }
    
    func showPreloader(animated: Bool) {
        preloaderView.show(animated: animated)
    }
    
    func hidePreloader() {
        hidePreloader(animated: true)
    }
    
    func hidePreloader(animated: Bool) {
        preloaderView.hide(animated: animated)
    }
    
}
