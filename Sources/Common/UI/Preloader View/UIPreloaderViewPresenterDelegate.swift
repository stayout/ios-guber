//
//  UIPreloaderViewPresenterDelegate.swift
//  Common
//
//  Created by Stayout on 15.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

public protocol UIPreloaderViewPresenterDelegate {
    func showPreloader()
    func hidePreloader()
}

extension UIPreloaderViewPresenterDelegate where Self: RootPresenterProtocol {
    
    public func showPreloader() {
        if let view = view as? UIPreloaderViewDelegate {
            view.showPreloader()
        }
    }

    public func hidePreloader() {
        if let view = view as? UIPreloaderViewDelegate {
            view.hidePreloader()
        }
    }
    
}
