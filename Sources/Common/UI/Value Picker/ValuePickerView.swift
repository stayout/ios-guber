//
//  ValuePickerView.swift
//  Common
//
//  Created by Stayout on 25.05.2020.
//  Copyright © 2020 WhiteSoft. All rights reserved.
//

import UIKit
import SnapKit

public protocol ValuePickerChangedValueDelegate {
    func valueDidChanged(valuePicker: ValuePickerView)
}

public protocol ValuePickerActionsDelegate {
    func rightButtonTapped(valuePicker: ValuePickerView)
    func leftButtonTapped(valuePicker: ValuePickerView)
}

public class ValuePickerView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightActionButton: UIButton!
    @IBOutlet weak var leftActionButton: UIButton!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet weak var overlayView: WSOverlayView!
    @IBOutlet weak var datePickerContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    public var items = [RootCellContent]() {
        didSet {
            pickerView.reloadAllComponents()
        }
    }
    public var selectedItem: RootCellContent? {
        return items.selected.first
    }
    public var changedValueDelegate: ValuePickerChangedValueDelegate?
    public var actionsDelegate: ValuePickerActionsDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        alpha = 0
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        pickerView.dataSource = self
        pickerView.delegate = self
        titleLabel.text = nil
        rightActionButton.setTitle(nil, for: .normal)
        leftActionButton.setTitle(nil, for: .normal)
        guard let view = superview else { return }
        snp.makeConstraints({
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        })
        bottomMargin.constant = -datePickerContainerViewHeight.constant
        overlayView.delegate = self
        overlayView.toggleVisibility()
    }
    
    @IBAction func rightActionButtonTapped(_ sender: UIButton) {
        actionsDelegate?.rightButtonTapped(valuePicker: self)
    }
    
    @IBAction func leftActionButtonTapped(_ sender: UIButton) {
        actionsDelegate?.leftButtonTapped(valuePicker: self)
    }
    
    public func setTitle(_ value: String?) {
        titleLabel.text = value
    }
    
    public func setRightActionTitle(_ value: String?) {
        rightActionButton.setTitle(value, for: .normal)
    }
    
    public func setLeftActionTitle(_ value: String?) {
        leftActionButton.setTitle(value, for: .normal)
    }
    
    public func toggleVisibility() {
        bottomMargin.constant = alpha == 0 ? 0 : -datePickerContainerViewHeight.constant
        set(alpha: self.alpha == 0 ? 1 : 0, duration: 0.5)
        layoutIfNeeded(duration: 0.5)
        overlayView.toggleVisibility()
    }
    
}

extension ValuePickerView: UIPickerViewDataSource, UIPickerViewDelegate {
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return items.type == .grouped ? items.count : 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.type == .grouped ? items[component].subItems.count : items.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item = items.type == .grouped ? items[component].subItems[row] : items[row]
        return item.title
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if items.type == .grouped {
            items[component].subItems.update(index: row, selection: .single)
        }
        else {
            items.update(index: row, selection: .single)
        }
        changedValueDelegate?.valueDidChanged(valuePicker: self)
    }
    
}

extension ValuePickerView: WSOverlayViewDelegate {
    
    public func overlayTapped() {
        toggleVisibility()
    }

}

