//
//  WSPickerView.swift
//  Common
//
//  Created by Stayout on 30.09.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public protocol WSPickerViewProtocol {
    func setPlaceholder(text: String?)
    func setSelectedValue(text: String?)
}

public protocol WSViewErrorProtocol {
    func showError(text: String?)
    func showError(text: String?, animated: Bool)
    func hideError()
    func hideError(animated: Bool)
}

public class WSPickerView: StoryboardInitiableView {
    
    @IBOutlet private var button: UIButton!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var errorLabelTopMargin: NSLayoutConstraint!
    public var buttonDidTappedHandler: (() -> Void)?
    
    public override func configure() {
        super.configure()
        hideError(animated: false)

        button.imageView?.snp.makeConstraints({ maker in
            maker.height.equalTo(16)
            maker.width.equalTo(16)
            maker.right.equalToSuperview().offset(-16)
            maker.centerY.equalToSuperview()
        })
    }
    
    @IBAction private func buttonDidTapped(_ sender: UIButton) {
        buttonDidTappedHandler?()
    }
    
}

extension WSPickerView: WSPickerViewProtocol {
    
    public func setPlaceholder(text: String?) {
        button.setTitle(text, for: .normal)
        button.tintColor = .orange
    }
    
    public func setSelectedValue(text: String?) {
        button.setTitle(text, for: .normal)
        button.tintColor = .orange
    }
    
}

extension WSPickerView: WSViewErrorProtocol {
    
    public func showError(text: String?) {
        showError(text: text, animated: true)
    }
    
    public func showError(text: String?, animated: Bool) {
        errorLabelTopMargin.constant = 8
        layoutIfNeeded(duration: animated ? 0.33 : 0)
    }
    
    public func hideError() {
        hideError(animated: true)
    }
    
    public func hideError(animated: Bool) {
        errorLabelTopMargin.constant = -errorLabel.height
        layoutIfNeeded(duration: animated ? 0.33 : 0)
    }
    
}
