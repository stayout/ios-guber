//
//  WSTapGestureRecognizer.swift
//  1EyeOnU
//
//  Created by Stayout on 10/03/2019.
//  Copyright © 2019 demon112. All rights reserved.
//

import UIKit

public class WSTapGestureRecognizer: UITapGestureRecognizer {
    
    private let handler: () -> ()
    
    public init(handler: @escaping () -> ()) {
        self.handler = handler
        super.init(target: nil, action: nil)
        addTarget(self, action: #selector(didTapped))
    }
    
    @objc private func didTapped() {
        handler()
    }
    
}

public extension UIView {
    
    func addTapGestureRecognizer(_ handler: (() -> Void)?) {
        guard let closure = handler else { return }
        isUserInteractionEnabled = true
        let tapGesture = WSTapGestureRecognizer(handler: closure)
        tapGesture.cancelsTouchesInView = false
        addGestureRecognizer(tapGesture)
    }
    
}
