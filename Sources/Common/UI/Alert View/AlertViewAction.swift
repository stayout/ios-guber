//
//  AlertViewAction.swift
//  UI
//
//  Created by Stayout on 08/11/2018.
//  Copyright © 2018 White Soft. All rights reserved.
//

import UIKit

public class AlertViewAction: UIButton {
 
    public var source: AlertView?
    public var handler: (() -> Void)?
    
    public convenience init(title: String,
                            fontWeight: UIFont.Weight = .medium,
                            image: UIImage? = nil,
                            handler: (() -> Void)? = nil) {
        self.init(type: .system)
        self.handler = handler
        setTitle(image == nil ? title : "\(title)  ", for: .normal)
        setImage(image, for: .normal)
        semanticContentAttribute = .forceRightToLeft
        tintColor = .cyan
        apply(family: HelveticaNeueFont.self, weight: fontWeight, size: 15)
        addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    }
    
    @objc private func buttonTapped() {
        source?.hide()
        handler?()
    }
    
}
