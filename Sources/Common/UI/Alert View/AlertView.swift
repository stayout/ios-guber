//
//  AlertView.swift
//  UI
//
//  Created by Stayout on 08/11/2018.
//  Copyright © 2018 White Soft. All rights reserved.
//

import SnapKit
import UIKit

public class AlertView: UIView {
    
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var desc: UILabel!
    @IBOutlet private weak var baseView: UIView?
    @IBOutlet private weak var blurView: UIVisualEffectView?
    @IBOutlet private weak var actionStackView: UIStackView!
    
    private var isFinished: Bool = false
    
    public func add(for view: UIView) {
        hide(animated: false)
        configure()
        view.add(subview: self, constraints: { maker in
            maker.edges.equalToSuperview()
        })
    }
    
    private func configure() {
        title.apply(family: HelveticaNeueFont.self, weight: .medium, size: 20)
        desc.apply(family: HelveticaNeueFont.self, weight: .light, size: 16)
        baseView?.rounded(by: .allCorners, radius: 5)
        baseView?.layer.shadowColor = UIColor.cyan.cgColor
        baseView?.layer.shadowOffset = .zero
        baseView?.layer.shadowOpacity = 0.2
        baseView?.layer.shadowRadius = 30
        blurView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(blurViewTapped)))
    }
    
    public func set(title: String?) {
        self.title.text = title
    }
    
    public func set(desc: String) {
        let attributedString = NSMutableAttributedString(string: desc)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.2
        attributedString.addAttribute(.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        self.desc.attributedText = attributedString
    }
    
    public func set(actions: [AlertViewAction]) {
        actionStackView.arrangedSubviews.forEach({ view in
            view.removeFromSuperview()
        })
        for action in actions {
            action.source = self
            actionStackView.addArrangedSubview(action)
        }
        actionStackView.axis = actions.count > 2 ? .vertical : .horizontal
    }
    
    @objc private func blurViewTapped() {
        hide()
    }
    
    public func hide(animated: Bool = true) {
        let duration = animated ? 0.33 : 0
        baseView?.apply(transform: .hidden, duration: duration)
        self.set(isHidden: true, duration: duration)
    }
    
    public func show(animated: Bool = true) {
        let duration = animated ? 0.33 : 0
        baseView?.apply(transform: .normal, duration: duration)
        self.set(isHidden: false, duration: duration)
    }
    
}
