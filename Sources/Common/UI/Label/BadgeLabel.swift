//
//  BadgeLabel.swift
//  Voice 3
//
//  Created by Вячеслав Толстобров on 21/04/2019.
//  Copyright © 2019 White Soft. All rights reserved.
//

import UIKit

public class BadgeLabel: RootLabel {
    
    public override func configure() {
        super.configure()
        font = HelveticaNeueFont.font(weight: .bold, size: 12)
        textAlignment = .center
        layer.masksToBounds = true
        layer.cornerRadius = 10
        textColor = .white
        backgroundColor = .cyan
        textInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
}
