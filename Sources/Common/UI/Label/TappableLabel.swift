//
//  TappableLabel.swift
//  Voice 3
//
//  Created by Stayout on 25.07.2018.
//  Copyright © 2018 White Soft. All rights reserved.
//

import UIKit

public class TappableLabel: UILabel {
   
    private var tapClosure: (() -> Void)?
    
    public func addTapGesture(closure: (() -> Void)?) {
        isUserInteractionEnabled = true
        self.tapClosure = closure
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
    }
    
    @objc private func tapped() {
        tapClosure?()
    }
    
}
