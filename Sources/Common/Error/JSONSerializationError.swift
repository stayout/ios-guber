//
//  SerializationError.swift
//  Common
//
//  Created by Stayout on 28/12/2016.
//  Copyright © 2016 WhiteSoft. All rights reserved.
//

import Foundation

public struct JSONSerializationError: Error {
    
    public let message: String
    
    private func showErrorMessage() {
        print("""
            ================================================================================
            Serialization error
            \(message))
            ================================================================================\n
            """)
    }
    
    public init() {
        message = ""
        showErrorMessage()
    }
    
    public init(missing key: String, classType: Any) {
        message = "Class name: \(classType)\nMissing key: \(key)"
        showErrorMessage()
    }
    
    public init(missing keys: [String], classType: Any) {
        message = "Class name: \(classType)\nMissing keys: \(keys)"
        showErrorMessage()
    }
    
    public init(invalid key: String, classType: Any) {
        message = "Class name: \(classType)\nInvalid key: \(key)"
        showErrorMessage()
    }
    
    public init(wrongRootElement: Any?, classType: Any) {
        message = "Class name: \(classType)\nWrong root element, expected json dictionary not: \(String(describing: wrongRootElement))"
        showErrorMessage()
    }
    
}

public extension Error {
    
    var jsonSerializationError: JSONSerializationError? {
        return self as? JSONSerializationError
    }
    
}
