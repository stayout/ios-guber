//
//  HTTPError.swift
//  Common
//
//  Created by Stayout on 12.10.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

import Foundation

private struct Keys {
    static let message = "message"
    static let code = "code"
}

public enum HTTPErrorType {
    case unknownError
    case connectionError
    case authorizationError
    case invalidRequest
    case notFound
    case invalidResponse
    case serverError
    case serverUnavailable
    case timeOut
    case internetIsOffline
    case errorWithDescription
}

public struct HTTPError: Error {
    
    public let type: HTTPErrorType
    private let json: JSONDictonary?
    public var message: String?
    
    public init(type: HTTPErrorType) {
        self.json = nil
        self.type = type
        self.message = nil
    }
    
    public init(code: Int?, message: String? = nil) {
        self.json = nil
        self.type = HTTPError.type(from: code)
        self.message = message
    }
    
    public init(code: Int?, data: Data? = nil) {
        self.json = data?.asJSONDictionary
        self.type = HTTPError.type(from: code)
        self.message = message(key: Keys.message)
    }
    
    public var internalCode: Int? {
        return json?.int(for: Keys.code)
    }
    
    public func message(key: String) -> String? {
        return json?.string(for: key)
    }
    
    fileprivate static func type(from code: Int?) -> HTTPErrorType {
        switch code {
        case 1000:
            return .internetIsOffline
        case -1001:
            return .timeOut
        case 400:
            return .invalidRequest
        case 401:
            return .authorizationError
        case 404:
            return .notFound
        case 409:
            return .errorWithDescription
        case 500:
            return .serverError
        default:
            return .unknownError
        }
    }
    
}

public extension Error {
    
    var httpError: HTTPError? {
        return self as? HTTPError
    }
    
}
