//
//  SocialShareProtocol.swift
//  UI
//
//  Created by Stayout on 09/11/2018.
//  Copyright © 2018 White Soft. All rights reserved.
//

import UIKit

public protocol SocialShareProtocol {
    
}

fileprivate enum SocialShareType {
    
    case vk
    case fb
    case twitter
    
    public func shareURL(by sourceURL: String) -> URL? {
        var components: URLComponents?
        
        switch self {
        case .vk:
            components = URLComponents(string: "https://vk.com/share.php")
            components?.queryItems = [ URLQueryItem(name: "url", value: sourceURL) ]
        case .fb:
            components = URLComponents(string: "https://facebook.com/sharer.php")
            components?.queryItems = [ URLQueryItem(name: "u", value: sourceURL) ]
        case .twitter:
            components = URLComponents(string: "https://twitter.com/intent/tweet")
            components?.queryItems = [ URLQueryItem(name: "url", value: sourceURL) ]
        }
        
        return components?.url
    }
    
}

extension SocialShareProtocol where Self: UIViewController {
    
    public func presentShareController(sourceURL: String) {
        let controller = UIAlertController(title: "Выберите социальную сеть", message: nil, preferredStyle: .actionSheet)
        
        let vkAction = UIAlertAction(title: "ВКонтакте", style: .default, handler: { _ in
            self.share(by: .vk, sourceURL: sourceURL)
        })
        controller.addAction(vkAction)
        
        let fbAction = UIAlertAction(title: "Facebook", style: .default, handler: { _ in
            self.share(by: .fb, sourceURL: sourceURL)
        })
        controller.addAction(fbAction)
        
        let twitterAction = UIAlertAction(title: "Twitter", style: .default, handler: { _ in
            self.share(by: .twitter, sourceURL: sourceURL)
        })
        controller.addAction(twitterAction)
        controller.addAction(UIAlertAction(title: "Отмена", style: .cancel))
        self.present(controller, animated: true)
    }
    
    fileprivate func share(by type: SocialShareType, sourceURL: String) {
        let url = type.shareURL(by: sourceURL)
        UIApplication.open(url: url)
    }
    
}
