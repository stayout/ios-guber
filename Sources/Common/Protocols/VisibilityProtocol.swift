//
//  VisibilityProtocol.swift
//  Common
//
//  Created by Stayout on 16.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

public protocol VisibilityProtocol {
    func show()
    func show(animated: Bool)
    func show(duration: Double)
    func hide()
    func hide(animated: Bool)
    func hide(duration: Double)
}

public extension VisibilityProtocol where Self: UIView {
    
    func show() {
        show(animated: false)
    }
    
    func show(animated: Bool) {
        show(duration: animated ? 0.33 : 0)
    }
    
    func show(duration: Double) {
        self.set(isHidden: false, duration: duration)
    }
    
    func hide() {
        hide(animated: false)
    }
    
    func hide(animated: Bool) {
        hide(duration: animated ? 0.33 : 0)
    }
    
    func hide(duration: Double) {
        self.set(isHidden: true, duration: duration)
    }
    
}
