//
//  UIPreviewControllerProtocol.swift
//  Voice
//
//  Created by Stayout on 17.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public protocol UIPreviewControllerProtocol {
    func preview(file: PreviewFile)
}

extension UIPreviewControllerProtocol where Self: UIViewController {
    public func preview(file: PreviewFile) {
        DispatchQueue.main.async { [weak self] in
            let controller = UIPreviewController(modalPresentationStyle: .custom).set(file)
            self?.present(controller: controller)
        }
    }
}
