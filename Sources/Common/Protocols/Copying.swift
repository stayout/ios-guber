//
//  Copying.swift
//  Model
//
//  Created by Stayout on 05.02.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import Foundation

public protocol Copying {
    init(instance: Self)
}

public extension Copying {
    
    func copy() -> Self {
        return Self.init(instance: self)
    }
    
}

public extension Array where Element: Copying {
    
    func copy() -> Self {
        return map({ $0.copy() })
    }
    
}
