//
//  PhotoAndCameraLibraryPermissionPresenterDelegate.swift
//  Common
//
//  Created by Stayout on 11.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

public protocol PhotoAndCameraLibraryPermissionPresenterDelegate {
    
    func didSelect(option: AttachmentAlertOption?)
    func presentGallery<R: PhotoAndCameraLibraryPermissionRouterDelegate>(router: R)
    func presentCamera<R: PhotoAndCameraLibraryPermissionRouterDelegate>(router: R)

}

extension PhotoAndCameraLibraryPermissionPresenterDelegate {
    
    public func presentGallery<R: PhotoAndCameraLibraryPermissionRouterDelegate>(router: R) {
        router.presentGallery()
    }
    
    public func presentCamera<R: PhotoAndCameraLibraryPermissionRouterDelegate>(router: R) {
        router.presentCamera()
    }
    
}
