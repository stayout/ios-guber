//
//  PhotoAndCameraLibraryPermissionRouterDelegate.swift
//  Common
//
//  Created by Stayout on 11.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

public protocol PhotoAndCameraLibraryPermissionRouterDelegate {
    func presentGallery()
    func presentCamera()
}
