//
//  PhotoAndCameraLibraryPermissionInteractorDelegate.swift
//  Common
//
//  Created by Stayout on 11.01.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit
import Photos

public protocol PhotoAndCameraLibraryPermissionInteractorDelegate {

    func checkPhotoLibraryPermission<P: PhotoAndCameraLibraryPermissionPresenterDelegate,
        R: PhotoAndCameraLibraryPermissionRouterDelegate>(presenter: P, router: R, viewController: UIViewController?)
    
    func checkCameraLibraryPermission<P: PhotoAndCameraLibraryPermissionPresenterDelegate,
        R: PhotoAndCameraLibraryPermissionRouterDelegate>(presenter: P, router: R, viewController: UIViewController?)
    
}

extension PhotoAndCameraLibraryPermissionInteractorDelegate {
    
    fileprivate func showAlert(message: String, viewController: UIViewController?) {
        let alert = UIAlertController(title: "Пожалуйста предоставьте доступ",
                                      message: message,
                                      acceptButtonTitle: "Настройки",
                                      acceptButtonAction: {
                                        let url = URL(string: UIApplication.openSettingsURLString)
                                        UIApplication.open(url: url)
        },
                                      cancelButtonTitle: "Закрыть")
        
        viewController?.present(alert, animated: true)
    }
    
    public func checkPhotoLibraryPermission<P: PhotoAndCameraLibraryPermissionPresenterDelegate,
        R: PhotoAndCameraLibraryPermissionRouterDelegate> (presenter: P, router: R, viewController: UIViewController?) {
        
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            presenter.presentGallery(router: router)
        }
        else if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization({ _ in
                self.checkPhotoLibraryPermission(presenter: presenter, router: router, viewController: viewController)
            })
        }
        else if status == .denied {
            showAlert(message: "Приложению необходим доступ к вашей фото галлереи для отправки фото.", viewController: viewController)
        }
        
    }
    
    public func checkCameraLibraryPermission<P: PhotoAndCameraLibraryPermissionPresenterDelegate,
        R: PhotoAndCameraLibraryPermissionRouterDelegate> (presenter: P, router: R, viewController: UIViewController?) {
        
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        if status == .authorized {
            presenter.presentCamera(router: router)
        }
        else if status == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video,
                                          completionHandler: ({ _ in
                                            self.checkCameraLibraryPermission(presenter: presenter, router: router, viewController: viewController)
                                          })
            )
        }
        else if status == .denied {
            showAlert(message: "Приложению необходим доступ к вашей камере для отправки фото.", viewController: viewController)
        }
        
    }
    
}
