//
//  StoryboardInitiableView.swift
//  Common
//
//  Created by Stayout on 15.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit

open class StoryboardInitiableView: UIView {
    
    public var wrapperView: UIView?
    
    public init() {
        super.init(frame: .zero)
        if let view = UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: self, options: nil).first as? UIView {
            wrapperView = view
            add(subview: view, constraints: { maker in
                maker.edges.equalToSuperview()
            })
        }
        configure()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let view = UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: self, options: nil).first as? UIView {
            wrapperView = view
            add(subview: view, constraints: { maker in
                maker.edges.equalToSuperview()
            })
        }
        configure()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    open func configure() {

    }
    
}
