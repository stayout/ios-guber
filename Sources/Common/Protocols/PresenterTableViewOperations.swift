//
//  PresenterTableViewOperations.swift
//  Common
//
//  Created by Stayout on 11.05.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol PresenterTableViewOperations: UITableViewContentOperationsProtocol, RootPresenterProtocol {
    
}

extension PresenterTableViewOperations {
    
    public var delegate: UITableViewContentOperationsDelegate? {
        get {
            return view as? UITableViewContentOperationsDelegate
        }
    }
    
    public func update(cells: [RootCellContent]) {
        delegate?.update(cells: cells)
    }
    
    public func reloadData() {
        delegate?.reloadData()
    }
    
    public func reload(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.reload(sectionIndexes: sectionIndexes, cells: cells, with: animation)
    }
    
    public func reload(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.reload(sectionIndex: sectionIndex, cells: cells, with: animation)
    }
    
    public func reload(sectionIndexes: [Int], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.reload(sectionIndexes: sectionIndexes, cells: cells, with: animation)
    }
    
    public func reload(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.reload(rowIndexes: rowIndexes, cells: cells, with: animation)
    }
    
    public func insert(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.insert(sectionIndexes: sectionIndexes, cells: cells, with: animation)
    }
    
    public func insert(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.insert(sectionIndex: sectionIndex, cells: cells, with: animation)
    }
    
    public func insert(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.insert(rowIndexes: rowIndexes, cells: cells, with: animation)
    }
    
    public func remove(sectionIndexes: CountableRange<Int>, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.remove(sectionIndexes: sectionIndexes, cells: cells, with: animation)
    }
    
    public func remove(sectionIndex: Int, cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.remove(sectionIndex: sectionIndex, cells: cells, with: animation)
    }
    
    public func remove(rowIndexes: [IndexPath], cells: [RootCellContent]?, with animation: UITableView.RowAnimation) {
        delegate?.remove(rowIndexes: rowIndexes, cells: cells, with: animation)
    }
    
}
