//
//  ViewTraits.swift
//  Voice
//
//  Created by Stayout on 12/11/2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

protocol FontTraits {
    func set(font: UIFont?)
}

extension FontTraits {
    public func apply(family: UIFontProtocol.Type, weight: UIFont.Weight, size: CGFloat) {
        set(font: family.font(weight: weight, size: size))
    }
}

extension UILabel: FontTraits {
    public func set(font: UIFont?) {
        self.font = font
    }
}

extension UIButton: FontTraits {
    public func set(font: UIFont?) {
        self.titleLabel?.font = font
    }
}

extension UITextView: FontTraits {
    public func set(font: UIFont?) {
        self.font = font
    }
}

extension UITextField: FontTraits {
    public func set(font: UIFont?) {
        self.font = font
    }
}
