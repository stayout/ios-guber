//
//  Selectable.swift
//  Common
//
//  Created by Stayout on 17.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

public protocol Selectable {
    var isSelected: Bool { get set }
}

public enum SelectionMode {
    case single
    case multiple
    case noSelection
}

extension Array where Element: Selectable {
    
    public mutating func clearSelection() {
        self = self.map({
            var item = $0
            item.isSelected = false
            return $0
        })
    }
    
    public mutating func update(_ item: Selectable, selection: SelectionMode = .noSelection) {
        var item = item
        if selection == .single {
            clearSelection()
            item.isSelected = true
        }
            
        else if selection == .multiple {
            let newValue = item.isSelected ? false : true
            item.isSelected = newValue
        }
    }
    
    public mutating func update(index: Int, selection: SelectionMode = .noSelection) {
        if index < self.count {
            var item = self[index]
            
            if selection == .single {
                clearSelection()
                item.isSelected = true
            }
                
            else if selection == .multiple {
                let newValue = item.isSelected ? false : true
                item.isSelected = newValue
            }
        }
    }
    
    public var selected: [Element] {
        return filter({ $0.isSelected })
    }
    
    public var selectedItem: Element? {
        return selected.first
    }
    
    public var selectedIndex: Int? {
        return self.firstIndex(where: { $0.isSelected == true })
    }
    
}
