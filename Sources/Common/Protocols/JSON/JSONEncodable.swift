//
//  JSONEncodable.swift
//  Voice
//
//  Created by Stayout on 07/05/2019.
//  Copyright © 2019 WS. All rights reserved.
//

import Foundation

public protocol JSONEncodable {
    func encode() -> JSONDictonary
}

public extension Array where Element == JSONEncodable {
    
    func encode() -> [JSONDictonary] {
        return map({ $0.encode() })
    }
    
}
