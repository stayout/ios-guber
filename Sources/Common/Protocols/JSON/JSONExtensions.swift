//
//  JSONExtensions.swift
//  Voice
//
//  Created by Stayout on 07/05/2019.
//  Copyright © 2019 WS. All rights reserved.
//

import Foundation

public extension Data {
    
    var asJSONAny: Any? {
        return try? JSONSerialization.jsonObject(with: self, options: [])
    }
    
    var asJSONDictionary: JSONDictonary? {
        return asJSONAny as? JSONDictonary
    }
    
    func asJSONArray(forKey key: String = "items") -> JSONArray? {
        return asJSONAny as? JSONArray ?? asJSONDictionary?.array(for: key)
    }
    
    func getCount() -> Int? {
        return self.asJSONDictionary?.int(for: "totalCount")
    }
    
    func getItem<T: JSONDecodable>() throws -> T {
        guard let json = self.asJSONDictionary else {
            throw JSONSerializationError(wrongRootElement: self, classType: T.self)
        }
        
        return try T(json: json)
    }
    
    func getItems<T: JSONDecodable>(forKey key: String = "items") throws -> [T] {
        guard let json = self.asJSONArray(forKey: key) else {
            throw JSONSerializationError(wrongRootElement: self, classType: T.self)
        }
        return json.compactMap({ try? T(json: $0) })
    }
    
}

public extension Array where Element == JSONDictonary {
    
    var asData: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: [])
    }
    
}

public extension Dictionary where Key == String, Value == Any {
    
    var asJsonString: String? {
        if let data = self.asData, let string = String(data: data, encoding: .utf8) {
            return string
        }
        return nil
    }
    
    var asQuery: String {
        return self.map({ (key, value) in "\(key)=\(value)" }).joined(separator: "&")
    }
    
    var asQueryData: Data? {
        return asQuery.data(using: .utf8)
    }
    
    var asData: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: [])
    }
    
    func string(for key: String) -> String? {
        return value(for: key)
    }
    
    func bool(for key: String) -> Bool? {
        return try? value(for: key)
    }
    
    func int(for key: String) -> Int? {
        return value(for: key)
    }
    
    func double(for key: String) -> Double? {
        return value(for: key)
    }
    
    func dict(for key: String) -> JSONDictonary? {
        return try? value(for: key)
    }
    
    func array(for key: String) -> JSONArray? {
        return try? value(for: key)
    }
    
    func value<A: Any>(for keys: String...) throws -> A {
        for key in keys {
            if let value = value(for: key) as? A {
                return value
            }
        }
        throw JSONSerializationError(missing: keys, classType: A.self)
    }
    
    func value<C: Convertable>(for keys: String...) throws -> C {
        for key in keys {
            if let value = C(value: value(for: key)) {
                return value
            }
        }
        throw JSONSerializationError(missing: keys, classType: C.self)
    }
    
    func value<C: Convertable>(for keys: String...) -> C? {
        for key in keys {
            if let value = C(value: value(for: key)) {
                return value
            }
        }
        return nil
    }
    
    func value<C: Convertable>(for keys: String...) throws -> [C] {
        for key in keys {
            if let array: [Any] = try? value(for: key) {
                return array.compactMap({ C(value: $0) })
            }
            else if let array = array(for: key) {
                return array.compactMap({ C(value: $0) })
            }
        }
        throw JSONSerializationError(missing: keys, classType: C.self)
    }
    
    func value<C: Convertable>(for keys: String...) -> [C]? {
        for key in keys {
            if let array: [Any] = try? value(for: key) {
                return array.compactMap({ C(value: $0) })
            }
            else if let array = array(for: key) {
                return array.compactMap({ C(value: $0) })
            }
        }
        return nil
    }
    
    func value<J: JSONDecodable>(for keys: String...) -> J? {
        for key in keys {
            if let value = try? J(json: value(for: key)) {
                return value
            }
        }
        return nil
    }
    
    func value<J: JSONDecodable>(for keys: String...) throws -> J {
        for key in keys {
            if let value = try? J(json: value(for: key)) {
                return value
            }
        }
        throw JSONSerializationError(missing: keys, classType: J.self)
    }
    
    func value<J: JSONDecodable>(for keys: String...) throws -> [J] {
        for key in keys {
            if let array = array(for: key) {
                return array.compactMap({ try? J(json: $0) })
            }
        }
        throw JSONSerializationError(missing: keys, classType: J.self)
    }
    
    func value<J: JSONDecodable>(for keys: String...) -> [J]? {
        for key in keys {
            if let array = array(for: key) {
                return array.compactMap({ try? J(json: $0) })
            }
        }
        return nil
    }
    
    private func value(for key: String) -> Any? {
        let keyArray = key.split(separator: ".").map({ $0.description })
        var result: Any? = self
        
        for key in keyArray {
            result = value(for: key, in: result)
        }
        
        return result
    }
    
    private func value(for key: String, in json: Any?) -> Any? {
        guard let json = json as? JSONDictonary else { return nil }
        return json[key]
    }
    
}
