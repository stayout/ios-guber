//
//  JSONCodable.swift
//  Voice
//
//  Created by Stayout on 07/05/2019.
//  Copyright © 2019 WS. All rights reserved.
//

public protocol JSONCodable: JSONDecodable, JSONEncodable {
}
