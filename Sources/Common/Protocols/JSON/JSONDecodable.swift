//
//  JSONCreatable.swift
//  Common
//
//  Created by Stayout on 28/12/2016.
//  Copyright © 2016 WhiteSoft. All rights reserved.
//

import Foundation

public typealias JSONDictonary = [String: Any]
public typealias JSONArray = [JSONDictonary]

public protocol JSONDecodable {
    init(json: JSONDictonary) throws
}

public extension JSONDictonary {
    
    static func json(_ dict: [String: Any?]) -> JSONDictonary {
        var json = JSONDictonary()
        dict.filter({ $0.value != nil }).forEach({ key, value in
            if let value = value as? JSONEncodable {
                json[key] = value.encode()
            }
            else if let value = value as? [JSONEncodable] {
                json[key] = value.encode()
            }
            else {
                json[key] = value ?? NSNull()
            }
        })
        return json
    }
    
    init?(plist: String, bundle: Bundle = .main) {
        if let url = bundle.url(forResource: plist, withExtension: "plist"),
           let data = try? Data(contentsOf: url),
           let options = try? PropertyListSerialization.propertyList(from: data, options: .mutableContainers, format: nil),
           let json = options as? JSONDictonary {
            self = json
        }
        else {
            return nil
        }
    }
    
}


public extension JSONArray {
    
    init(json: String, bundle: Bundle = .main) {
        if let url = bundle.url(forResource: json, withExtension: "json"),
           let data = try? Data(contentsOf: url),
           let array = data.asJSONArray() {
            self = array
        }
        else {
            self = []
        }
    }
    
    func items<J: JSONDecodable>() -> [J] {
        return compactMap({
            try? J(json: $0)
        })
    }
    
}
