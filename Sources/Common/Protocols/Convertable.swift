//
//  Convertable.swift
//  Voice
//
//  Created by Stayout on 21.07.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Foundation

public protocol Convertable {
    init?(value: Any?)
}

extension Date: Convertable {
    
    public init?(value: Any?) {
        if let date = value as? Date {
            self = date
        }
        else if let value = value as? Int {
            let double = Double(value)
            self.init(timeIntervalSince1970: double / 1000)
        }
        else if let value = value as? Double {
            self.init(timeIntervalSince1970: value / 1000)
        }
        else if let value = value as? String, let date = Date(value, formatter: .iso8601) {
            self = date
        }
        else if let value = value as? String {
            let formatter = ISO8601DateFormatter()
            if let date = Date(value, formatter: .iso8601) ?? formatter.date(from: value) {
                self = date
            } else {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
}

extension String: Convertable {
    
    public init?(value: Any?) {
        if let value = value as? String {
            self = value
        }
        else if let value = value as? Int {
            self = value.description
        }
        else if let value = value as? Double {
            self = value.description
        }
        else {
            return nil
        }
    }
    
}

extension Double: Convertable {
    
    public init?(value: Any?) {
        if let value = value as? String, let temp = Double(value) {
            self = temp
        }
        else if let value = value as? Double {
            self = value
        }
        else if let value = value as? Int {
            self = Double(value)
        }
        else {
            return nil
        }
    }
    
}

extension Float: Convertable {
    
    public init?(value: Any?) {
        if let value = value as? String, let temp = Float(value) {
            self = temp
        }
        else if let value = value as? Float {
            self = value
        }
        else if let value = value as? Double {
            self = Float(value)
        }
        else if let value = value as? Int {
            self = Float(value)
        }
        else {
            return nil
        }
    }
    
}

extension Int: Convertable {
    
    public init?(value: Any?) {
        if let value = value as? String, let temp = Double(value) {
            self = Int(temp)
        }
        else if let value = value as? Double {
            self = Int(value)
        }
        else if let value = value as? Int {
            self = value
        }
        else {
            return nil
        }
    }
    
}
