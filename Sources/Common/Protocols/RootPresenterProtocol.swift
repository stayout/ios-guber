//
//  RootPresenterProtocol.swift
//  Common
//
//  Created by Stayout on 13.04.2018.
//  Copyright © 2018 WhiteSoft. All rights reserved.
//

import UIKit

public protocol RootPresenterProtocol: RootPresenterOutputProtocol {
    associatedtype ViewControllerType
    var view: ViewControllerType? { get set }
}

public protocol RootPresenterOutputProtocol: class {
    func present(controller: UIViewController?)
    func present(alertController: UIAlertController?, sender: UIButton?)
    func present(error: Error?, acceptHandler: (()->Void)?)
    func presentError(title: String?, message: String?, acceptHandler: (()->Void)?)
}

extension RootPresenterOutputProtocol where Self: RootPresenterProtocol {
    
    public func present(controller: UIViewController?) {
        guard let sourceViewController = view as? UIViewController else { return }
        sourceViewController.present(controller: controller)
    }
    
    public func present(alertController: UIAlertController?, sender: UIButton? = nil) {
        guard let sourceViewController = view as? UIViewController else { return }
        if let popoverController = alertController?.popoverPresentationController, let sender = sender {
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        sourceViewController.present(controller: alertController)
    }
    
    public func present(error: Error?, acceptHandler: (()->Void)? = nil) {
        let errorDescription: (title: String, message: String?) = {
            if let message = error?.httpError?.message(key: "error_description") {
                return (title: "Error", message: message)
            }
            if let message = error?.httpError?.message {
                return (title: "Error", message: message)
            }
            if let message = error?.jsonSerializationError?.message {
                return (title: "Serialization error", message: message)
            }
            return (title: "Error", message: "Unknown error")
        }()
        
        presentError(title: errorDescription.title, message: errorDescription.message, acceptHandler: acceptHandler)
    }
    
    public func presentError(title: String?, message: String?, acceptHandler: (()->Void)? = nil) {
        if let view = view as? UIViewController {
            view.presentError(title: title, message: message, acceptHandler: acceptHandler)
        }
        if let view = view as? UIPreloaderViewDelegate {
            view.hidePreloader()
        }
        if let view = view as? UITableViewRefreshContentDelegate {
            view.didUpdateContent()
        }
    }
    
}
