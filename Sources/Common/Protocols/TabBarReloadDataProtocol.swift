//
//  TabBarReloadDataProtocol.swift
//  Common
//
//  Created by Stayout on 16.11.2020.
//  Copyright © 2020 WS. All rights reserved.
//

public protocol TabBarReloadDataProtocol {
    func reloadContent()
}
