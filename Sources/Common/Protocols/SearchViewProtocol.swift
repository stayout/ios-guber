//
//  SearchViewProtocol.swift
//  Common
//
//  Created by Stayout on 04.09.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

public enum ScrollDirection {
    case up
    case down
}

protocol SearchViewProtocol {
    func didScrolled(direction: ScrollDirection)
}
