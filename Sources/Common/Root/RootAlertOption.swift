//
//  RootAlertOption.swift
//  Voice
//
//  Created by Stayout on 17.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public class RootAlertOption {
    
    public let name: String
    public let icon: UIImage?
    public let titleTextColor: UIColor?
    public let titleTextAlignment: NSTextAlignment?
    public let customValue: Any?
    
    public init(name: String?,
                icon: UIImage? = nil,
                titleTextColor: UIColor? = .orange,
                titleTextAlignment: NSTextAlignment? = nil,
                customValue: Any? = nil) {
        self.name = name ?? ""
        self.icon = icon
        self.titleTextColor = titleTextColor
        self.titleTextAlignment = titleTextAlignment
        self.customValue = customValue
    }
    
}
