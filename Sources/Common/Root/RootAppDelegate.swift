//
//  RootAppDelegate.swift
//  Voice
//
//  Created by Stayout on 09.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit
import QuickLook

open class RootAppDelegate: UIResponder, UIApplicationDelegate {
    open var window: UIWindow?
    
    open func didFinishLaunch(_ application: UIApplication, options: [UIApplication.LaunchOptionsKey: Any]?) {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .clear
        window?.makeKeyAndVisible()
    }
    
    open class func setRootViewControllerAnimated(_ router: RootRouter,
                                                  dismissSourceIfNeeded: Bool = true,
                                                  completion: ((_ source: UIViewController?) -> Void)? = nil) {
        let appDelegate = UIApplication.shared.delegate as? RootAppDelegate
        appDelegate?.setRootViewControllerAnimated(router.viewController, dismissSourceIfNeeded: dismissSourceIfNeeded, completion: completion)
    }
    
    open class func setRootViewControllerAnimated(_ viewController: UIViewController?,
                                                  dismissSourceIfNeeded: Bool = true,
                                                  completion: ((_ source: UIViewController?) -> Void)? = nil) {
        let appDelegate = UIApplication.shared.delegate as? RootAppDelegate
        appDelegate?.setRootViewControllerAnimated(viewController, dismissSourceIfNeeded: dismissSourceIfNeeded, completion: completion)
    }
    
    open func setRootViewControllerAnimated(_ viewController: UIViewController?,
                                            dismissSourceIfNeeded: Bool = true,
                                            completion: ((_ source: UIViewController?) -> Void)? = nil) {
        let source = self.window?.rootViewController
        DispatchQueue.main.async {
            UIView.transition(from: source,
                              to: viewController,
                              completion: { _ in
                                self.window?.rootViewController = viewController
                                completion?(source)
                                if dismissSourceIfNeeded {
                                    source?.dismiss()
                                }
            })
        }
    }
    
}

