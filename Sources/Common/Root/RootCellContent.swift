//
//  RootCellContent.swift
//  Common
//
//  Created by Stayout on 01.11.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

open class RootCellContent: Selectable {
    
    public enum DataType {
        case list
        case grouped
    }
    
    public let identifier: String
    public var title: String?
    public var description: String?
    public var subItems = [RootCellContent]()
    public var isSelected: Bool = false
    public var customValue: Any?
    
    public init() {
        self.identifier = "defaultCell"
    }
    
    public init(identifier: String) {
        self.identifier = identifier
    }

}

public extension RootCellContent {
    
    func with(title: String?) -> Self {
        self.title = title
        return self
    }
    
    func with(description: String?) -> Self {
        self.description = description
        return self
    }
    
    func with(isSelected: Bool) -> Self {
        self.isSelected = isSelected
        return self
    }
    
    func with(customValue: Any?) -> Self {
        self.customValue = customValue
        return self
    }
    
    func with(subItems: [RootCellContent]) -> Self {
        self.subItems = subItems
        return self
    }
    
}

public extension Array where Element == RootCellContent {
    
    func casted<T>() -> [T] {
        return self.compactMap({ $0 as? T })
    }
    
    var type: RootCellContent.DataType {
        return filter({ $0.subItems.count > 0 }).count > 0 ? .grouped : .list
    }
    
}
