//
//  HashableClass.swift
//  Voice
//
//  Created by Stayout on 07/05/2019.
//  Copyright © 2019 WS. All rights reserved.
//

import Foundation

open class HashableClass: Equatable {
    public init() { }
}

extension HashableClass: Hashable {
    public var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
}

public func ==(lhs: HashableClass, rhs: HashableClass) -> Bool {
    return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
}
