//
//  RootItem.swift
//  Common
//
//  Created by Stayout on 02.08.17.
//  Copyright © 2017 WhiteSoft. All rights reserved.
//

open class RootItem: Selectable {
    public var isSelected: Bool = false
    public init() { }
}
