//
//  RootRouter.swift
//  Voice
//
//  Created by Stayout on 18.07.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit

public protocol RootRouterInputProtocol {
    func dismiss(animated: Bool)
    func dismiss(animated: Bool, completion: @escaping () -> Void)
    func pop(animated: Bool)
    func popToRoot(animated: Bool)
}

open class RootRouter: RootRouterInputProtocol {
    
    public unowned let viewController: UIViewController
    public unowned let navigationController: UINavigationController?
    
    public init(view: UIViewController) {
        viewController = view
        navigationController = view as? UINavigationController
    }
    
    open func dismiss(animated: Bool = true) {
        viewController.dismiss(animated: animated)
    }
    
    open func dismiss(animated: Bool, completion: @escaping () -> Void) {
        viewController.dismiss(animated: animated, completion: completion)
    }
    
    open func pop(animated: Bool = true) {
        let controller = navigationController ?? viewController.navigationController ?? viewController as? UINavigationController
        controller?.popViewController(animated: animated)
    }
    
    open func popToRoot(animated: Bool) {
        let controller = navigationController ?? viewController.navigationController ?? viewController as? UINavigationController
        controller?.popToRootViewController(animated: animated)
    }
    
}
