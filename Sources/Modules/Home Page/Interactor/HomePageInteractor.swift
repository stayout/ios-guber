//  
//  HomePageInteractor.swift
//  Voice
//
//  Created by Stayout on 10.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import MapKit
import Common
import Model
import Networking
import RxSwift

final class HomePageInteractor: NSObject {
    
    weak var presenter: HomePagePresenterOutput?
    private let locationManager = CLLocationManager()
    
    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
}

extension HomePageInteractor: HomePageInteractorInput {
    
    func requestUserLocation() {
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        else if status.contains(.authorizedAlways, .authorizedWhenInUse) {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
}

extension HomePageInteractor: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.last?.coordinate {
            presenter?.userLocationPositionDidUpdated(coordinate)
        }
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status.contains(.authorizedAlways, .authorizedWhenInUse) {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        presenter?.userLocationHeadingDidUpdate(angle: newHeading.trueHeading)
    }
    
}
