//
//  MapPlacemark.swift
//  Guber
//
//  Created by Stayout on 26.03.2021.
//

import MapKit

final class WSAnnotationView: MKAnnotationView {

    private let _annotation = MKPointAnnotation()
    let type: PlacemarkType
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(type: PlacemarkType) {
        self.type = type
        _annotation.title = type.rawValue
        super.init(annotation: _annotation, reuseIdentifier: type.rawValue)
        image = type.image
    }
    
    func setCoordinate(_ value: CLLocationCoordinate2D) {
        _annotation.coordinate = value
    }
    
}
