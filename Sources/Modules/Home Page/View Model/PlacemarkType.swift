//
//  PlacemarkType.swift
//  Guber
//
//  Created by Stayout on 26.03.2021.
//

import UIKit
import Common

enum PlacemarkType: String {
    
    case user
    case passenger
    case destination
    case pin
    
    var image: UIImage? {
        switch self {
        case .user: return UIImage(named: "UserArrow")
        case .passenger: return UIImage(named: "Passenger")
        default: return nil
        }
    }
    
}

extension PlacemarkType: Convertable {
    
    public init?(value: Any?) {
        if let value = value as? String, let enumValue = Self(rawValue: value) {
            self = enumValue
        }
        else {
            return nil
        }
    }
    
}
