//
//  HomePagePresenterOutput.swift
//  Voice
//
//  Created by Stayout on 10.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit
import CoreLocation
import Model
import Common

protocol HomePagePresenterOutput: RootPresenterOutputProtocol {
    func userLocationPositionDidUpdated(_ coordinate: CLLocationCoordinate2D)
    func userLocationHeadingDidUpdate(angle: CLLocationDirection)
}

extension HomePagePresenter: HomePagePresenterOutput {
    
    func userLocationPositionDidUpdated(_ coordinate: CLLocationCoordinate2D) {
        if trackingIsEnabled {
            view?.updateCamera(coordinate: coordinate, angle: userAngle, duration: 1)
        }
        if userLocation == nil {
            view?.setCameraPosition(coordinate: coordinate, zoom: 0.01)
            view?.showUserLocationPlacemark(coordinate: coordinate)
        }
        else {
            view?.updateUserLocation(coordinate: coordinate)
        }
        userLocation = coordinate
    }
    
    func userLocationHeadingDidUpdate(angle: CLLocationDirection) {
        userAngle = angle
        if trackingIsEnabled, let coordinate = userLocation {
            view?.updateCamera(coordinate: coordinate, angle: userAngle, duration: 0.4)
        }
    }
    
}
