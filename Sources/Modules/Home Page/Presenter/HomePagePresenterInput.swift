//
//  HomePagePresenterInput.swift
//  Voice
//
//  Created by Stayout on 10.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import MapKit
import Model
import Common

protocol HomePagePresenterInput {

    var trackingIsEnabled: Bool { get }
    
    func viewDidLoad()
    func createOrderButtonDidTapped()
    func userTrackingButtonDidTapped()

}


// MARK: - HomePagePresenterInput
extension HomePagePresenter: HomePagePresenterInput {
    
    func viewDidLoad() {
        let coordinate = CLLocationCoordinate2D(latitude: 48.49016, longitude: 135.083588)
        view?.setCameraPosition(coordinate: coordinate, zoom: 0.08)
        interactor.requestUserLocation()
    }
    
    func userTrackingButtonDidTapped() {
        trackingIsEnabled.toggle()
        let title = trackingIsEnabled ? "Отключить следование" : "Включить следование"
        view?.updateUserTrackingDescription(title)
        
        if trackingIsEnabled, let coordinate = userLocation {
            view?.updateUserLocation(coordinate: coordinate)
            view?.updateCamera(coordinate: coordinate, angle: userAngle, duration: 1)
        }
    }
    
    func createOrderButtonDidTapped() {
        output?.homePageCreateOrderDidTriggered()
    }
    
}
