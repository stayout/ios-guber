//
//  HomePagePresenter.swift
//  Voice
//
//  Created by Stayout on 10.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import MapKit
import Model
import Common

final class HomePagePresenter: RootPresenterProtocol, UIPreloaderViewPresenterDelegate {

    let router: HomePageRouterInput
    let interactor: HomePageInteractorInput
    weak var view: HomePageViewControllerOutput?
    weak var output: HomePageOutput?
    
    var route: Route?
    var userLocation: CLLocationCoordinate2D?
    var userAngle: CLLocationDirection?
    var trackingIsEnabled = false
    
    init(router: HomePageRouterInput,
         interactor: HomePageInteractorInput,
         view: HomePageViewController?,
         output: HomePageOutput?) {
        self.router = router
        self.interactor = interactor
        self.view = view
        self.output = output
    }

}

extension HomePagePresenter: HomePageInput {
    
    func orderDidDeclined() {
        
    }
    
    func orderDidCreated(route: Route) {
        self.route = route
        let coordinate = CLLocationCoordinate2D(latitude: route.start.latitude, longitude: route.start.longitude)
        view?.showRouteToPassenger(coordinate: coordinate)
    }
    
}
