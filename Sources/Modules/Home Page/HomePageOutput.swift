//
//  HomePageOutput.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

protocol HomePageOutput: class {
    func homePageCreateOrderDidTriggered()
}
