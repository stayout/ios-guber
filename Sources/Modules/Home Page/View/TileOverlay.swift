//
//  TileOverlay.swift
//  Guber
//
//  Created by Stayout on 05.04.2021.
//

import MapKit

public class TileOverlay: MKTileOverlay {
    
    private let cache = NSCache<NSURL, NSData>()
    private let urlSession = URLSession(configuration: URLSessionConfiguration.default)
    
    override public func url(forTilePath path: MKTileOverlayPath) -> URL {
        let str = String(format: "https://rtile2.maps.2gis.com/tiles?x=%d&y=%d&z=%d&v=1", path.x, path.y, path.z)
        return URL(string: str)!
    }
    
    internal init() {
        super.init(urlTemplate: nil)
        cache.countLimit = 20
        tileSize = CGSize(width: 512, height: 512)
        canReplaceMapContent = true
        maximumZ = 20
    }
    
    override public func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
        let url = self.url(forTilePath: path)
        
        if let cachedData = self.cache.object(forKey: url as NSURL) as Data? {
            result(cachedData, nil)
        }
        else {
            let task = self.urlSession.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
                if let data = data {
                    self?.cache.setObject(data as NSData, forKey: url as NSURL)
                }
                result(data, error)
            })
            task.resume()
        }
    }
    
}
