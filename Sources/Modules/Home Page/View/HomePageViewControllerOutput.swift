//
//  HomePageViewControllerOutput.swift
//  Voice
//
//  Created by Stayout on 10.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import MapKit
import Foundation
import Common

protocol HomePageViewControllerOutput: UIPreloaderViewDelegate {
    func updateUserTrackingDescription(_ value: String)
    func setCameraPosition(coordinate: CLLocationCoordinate2D, zoom: CLLocationDegrees)
    func showUserLocationPlacemark(coordinate: CLLocationCoordinate2D)
    func updateUserLocation(coordinate: CLLocationCoordinate2D)
    func updateCamera(coordinate: CLLocationCoordinate2D, angle: CLLocationDirection?, duration: Double)
    func updateUserHeaging(angle: CLLocationDirection)
    func showRouteToPassenger(coordinate: CLLocationCoordinate2D)
}
