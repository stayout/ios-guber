//
//  HomePageViewController.swift
//  Voice
//
//  Created by Stayout on 10.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import MapKit
import Common

final class HomePageViewController: UIViewController {

    var presenter: HomePagePresenterInput?

    @IBOutlet var heatingMapLabel: UILabel!
    @IBOutlet var heatingDeviceLabel: UILabel!
    @IBOutlet var userLocationCoordinateLabel: UILabel!
    @IBOutlet var pinLocationCoordinateLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var userTrackingButton: UIButton!
    private let tilesOverlay = TileOverlay()
    var preloaderView: WSPreloaderView = .fromNib()
    
    lazy var userAnnotationView = WSAnnotationView(type: .user)
    lazy var passengerAnnotationView = WSAnnotationView(type: .passenger)
    lazy var pinAnnotationView = WSAnnotationView(type: .pin)
    var pinIsAdded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(preloaderView)
        
        let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap))
        longTapGesture.minimumPressDuration = 0.5
        mapView.addGestureRecognizer(longTapGesture)
        mapView.insertOverlay(tilesOverlay, at: 0, level: .aboveLabels)
        mapView.isPitchEnabled = false
        mapView.delegate = self
        
        presenter?.viewDidLoad()
    }
    
    @objc func longTap(sender: UIGestureRecognizer) {
        if sender.state == .began {
            let locationInView = sender.location(in: mapView)
            let coordinate = mapView.convert(locationInView, toCoordinateFrom: mapView)
            
            pinAnnotationView.setCoordinate(coordinate)
            if pinIsAdded == false {
                pinIsAdded = true
                mapView.addAnnotation(pinAnnotationView.annotation!)
            }
            
            let array = [userAnnotationView.annotation!.coordinate, coordinate]
            let polyline = MKPolyline(coordinates: array, count: 2)
            for overlay in mapView.overlays {
                mapView.removeOverlay(overlay)
            }
            mapView.addOverlay(polyline)
            
            pinLocationCoordinateLabel.text = "Pin location coordinate\nlatitude: \(coordinate.latitude)\nlongitude: \(coordinate.longitude)"
        }
    }
    
    @IBAction func trackingButtonDidTapped(_ sender: UIButton) {
        presenter?.userTrackingButtonDidTapped()
    }
    
    @IBAction func createOrderButtonDidTapped(_ sender: UIButton) {
        presenter?.createOrderButtonDidTapped()
    }
    
    
}

extension HomePageViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let type = PlacemarkType(value: annotation.title as Any?) else { return nil }

        if type == .user {
            return userAnnotationView
        }
        else if type == .passenger {
            return passengerAnnotationView
        }
        
        return nil
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        heatingMapLabel.text = "Heating map: \(mapView.camera.heading)"
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let routePolyline = overlay as? MKPolyline {
            let renderer = MKPolylineRenderer(polyline: routePolyline)
            renderer.strokeColor = UIColor.orange
            renderer.lineWidth = 1.5
            return renderer
        }
//        return MKOverlayRenderer()
        return MKTileOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if mapView.region.span.latitudeDelta < 0.002, presenter?.trackingIsEnabled == false {
            let coordinate = CLLocationCoordinate2DMake(mapView.region.center.latitude, mapView.region.center.longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)
            let region = MKCoordinateRegion(center: coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
    }
    
}

// MARK: - HomePageViewControllerOutput
extension HomePageViewController: HomePageViewControllerOutput {
    
    func updateUserTrackingDescription(_ value: String) {
        userTrackingButton.setTitle(value, for: .normal)
    }
    
    func setCameraPosition(coordinate: CLLocationCoordinate2D, zoom: CLLocationDegrees) {
        let span = MKCoordinateSpan(latitudeDelta: zoom, longitudeDelta: zoom)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func showUserLocationPlacemark(coordinate: CLLocationCoordinate2D) {
        userAnnotationView.setCoordinate(coordinate)
        mapView.addAnnotation(userAnnotationView.annotation!)
        userLocationCoordinateLabel.text = "User location coordinate\nlatitude: \(coordinate.latitude)\nlongitude: \(coordinate.longitude)"
    }
    
    func updateUserLocation(coordinate: CLLocationCoordinate2D) {
        UIView.animate(withDuration: 2.5, animations: {
            self.userAnnotationView.setCoordinate(coordinate)
        })
    }
    
    func updateCamera(coordinate: CLLocationCoordinate2D, angle: CLLocationDirection?, duration: Double) {
        UIView.animate(withDuration: duration, animations: { [unowned self] in
            let angle = angle ?? self.mapView.camera.heading
            let camera = MKMapCamera(lookingAtCenter: coordinate, fromDistance: 500, pitch: 0, heading: angle)
            self.mapView.setCamera(camera, animated: true)
        })
    }
    
    func updateUserHeaging(angle: CLLocationDirection) {
        heatingDeviceLabel.text = "Heating device: \(angle)"
    }
    
    func showRouteToPassenger(coordinate: CLLocationCoordinate2D) {
        passengerAnnotationView.setCoordinate(coordinate)
        mapView.addAnnotation(passengerAnnotationView.annotation!)
    }
    
}
