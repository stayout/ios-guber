//
//  HomePageBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct HomePageBuilder {
    
    func build(output: HomePageOutput?) -> (UIViewController, HomePageInput) {
        let controller: HomePageViewController = .instantiate(storyboard: "HomePage")
        let router = HomePageRouter(view: controller)
        let interactor = HomePageInteractor()
        let presenter = HomePagePresenter(router: router,
                                          interactor: interactor,
                                          view: controller,
                                          output: output)
        interactor.presenter = presenter
        controller.presenter = presenter
        return (controller, presenter)
    }
    
}
