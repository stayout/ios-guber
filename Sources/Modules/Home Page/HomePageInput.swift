//
//  HomePageInput.swift
//  Guber
//
//  Created by Stayout on 23.03.2021.
//

protocol HomePageInput: class {
    func orderDidDeclined()
    func orderDidCreated(route: Route)
}
