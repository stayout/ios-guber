//
//  RootAppPresenterInput.swift
//  Voice
//
//  Created by Stayout on 23.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Model
import Common
import YandexMapsMobile

protocol RootAppPresenterInput {
    func viewDidLoad()
}

extension RootAppPresenter: RootAppPresenterInput {
 
    func viewDidLoad() {
        router.presentStartScreen(output: self)
    }
    
}

extension RootAppPresenter: StartScreenOutput {
    
    func startScreenAuthenticationDidTriggered() {
        router.presentAuthentication(success: {
            self.router.presentCreateWorkSwift(output: self)
        })
    }
    
    func startScreenCreateWorkSwiftDidTriggered() {
        router.presentCreateWorkSwift(output: self)
    }
    
}

extension RootAppPresenter: WorkSwiftRootOutput {
    
    func workSwiftDidCreated() {
        homePagePresenter = router.presentHomePage(output: self)
    }
    
}

extension RootAppPresenter: HomePageOutput {
    
    func homePageCreateOrderDidTriggered() {
        router.presentCreateOrder(output: self)
    }
    
}

extension RootAppPresenter: OrderRootOutput {
    
    func orderDidDeclined() {
        homePagePresenter?.orderDidDeclined()
    }
    
    func orderDidCreated(route: Route) {
        homePagePresenter?.orderDidCreated(route: route)
    }
    
}
