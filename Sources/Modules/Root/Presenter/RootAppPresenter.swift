//  
//  RootChatPresenter.swift
//  Voice
//
//  Created by Stayout on 21.07.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Model
import Common
import UIKit

final class RootAppPresenter: RootPresenterProtocol {
    
	let router: RootAppRouterInput
    var view: RootAppViewControllerOutput?
    weak var homePagePresenter: HomePageInput?
    
    init(router: RootAppRouterInput,
         view: RootAppViewControllerOutput) {
        self.router = router
        self.view = view
    }

}
