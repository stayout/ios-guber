//
//  RootAppBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct RootAppBuilder {
    
    func build() -> UIViewController {
        let controller = RootAppViewController()
        let router = RootAppRouter(view: controller)
        let presenter = RootAppPresenter(router: router, view: controller)
        controller.presenter = presenter
        return controller
    }
    
}
