//  
//  RootChatRouter.swift
//  Voice
//
//  Created by Stayout on 21.07.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import UIKit
import Model
import Common

final class RootAppRouter: RootRouter, RootAppRouterInput {
    
    func presentStartScreen(output: StartScreenOutput?) {
        let controller = StartScreenBuilder().build(output: output)
        UIApplication.setRootViewController(controller)
    }
    
    func presentCreateWorkSwift(output: WorkSwiftRootOutput?) {
        let controller = WorkSwiftRootBuilder().build(output: output)
        UIApplication.setRootViewController(controller)
    }
    
    func presentAuthentication(success: @escaping () -> Void) {
        let controller = AuthenticationRootBuilder().build(success: success)
        UIApplication.setRootViewController(controller)
    }
    
    func presentHomePage(output: HomePageOutput?) -> HomePageInput {
        let builder = HomePageBuilder().build(output: output)
        navigationController?.pushViewController(builder.0, animated: false)
        UIApplication.setRootViewController(viewController)
        return builder.1
    }
    
    func presentCreateOrder(output: OrderRootOutput?) {
        let controller = OrderRootBuilder().build(output: output)
        viewController.present(controller: controller)
    }
    
    func dismissToRoot(completion: @escaping () -> Void) {
        UIApplication.root?.dismiss(animated: true, completion: completion)
    }
    
}
