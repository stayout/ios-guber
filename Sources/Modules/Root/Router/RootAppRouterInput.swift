//
//  RootChatRouterInputProtocol.swift
//  Voice
//
//  Created by Stayout on 01.08.2018.
//  Copyright © 2018 WS. All rights reserved.
//

import Common
import Model

protocol RootAppRouterInput: RootRouterInputProtocol {
    func presentStartScreen(output: StartScreenOutput?)
    func presentCreateWorkSwift(output: WorkSwiftRootOutput?)
    func presentAuthentication(success: @escaping () -> Void)
    func presentHomePage(output: HomePageOutput?) -> HomePageInput
    func presentCreateOrder(output: OrderRootOutput?)
    func dismissToRoot(completion: @escaping () -> Void)
}
