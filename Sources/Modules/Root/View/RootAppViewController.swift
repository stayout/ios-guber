//
//  WSTabBarViewController.swift
//  Voice
//
//  Created by Stayout on 23.10.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit
import Common

public class RootAppViewController: UINavigationController {
    
    var presenter: RootAppPresenterInput?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
}

extension RootAppViewController: RootAppViewControllerOutput {
}
