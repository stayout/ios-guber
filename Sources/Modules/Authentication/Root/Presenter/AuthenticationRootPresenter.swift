//  
//  AuthenticationRootPresenter.swift
//  Voice
//
//  Created by Stayout on 23.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common
import Foundation
import Model
import Networking

final class AuthenticationRootPresenter: RootPresenterProtocol {

    // MARK: - Properties
    
    let router: AuthenticationRootRouterInput
    weak var view: AuthenticationRootViewController?
    private let success: (() -> Void)?
    private let failure: (() -> Void)?
    
    private var phone: Phone?
    
    // MARK: - Init
    
    init(router: AuthenticationRootRouterInput,
         view: AuthenticationRootViewController?,
         success: (() -> Void)?,
         failure: (() -> Void)?) {
        self.router = router
        self.view = view
        self.success = success
        self.failure = failure
    }

}


// MARK: - PhoneOutputProtocol
extension AuthenticationRootPresenter: LoginOutputProtocol {
    
    func loginDidSucceed() {
        success?()
    }
    
}
