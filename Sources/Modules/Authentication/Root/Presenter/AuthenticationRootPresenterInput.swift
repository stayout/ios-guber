//
//  AuthenticationRootPresenterInput.swift
//  Voice
//
//  Created by Stayout on 02.09.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

protocol AuthenticationRootPresenterInput {
    func viewDidLoad()
}

// MARK: - AuthenticationRootPresenterInput
extension AuthenticationRootPresenter: AuthenticationRootPresenterInput {
    
    func viewDidLoad() {
        router.presentLogin(output: self)
    }
    
}
