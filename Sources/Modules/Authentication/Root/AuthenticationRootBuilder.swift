//
//  AuthenticationRootBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct AuthenticationRootBuilder {
    
    func build(success: (() -> Void)? = nil,
               failure: (() -> Void)? = nil) -> UIViewController {
        let controller: AuthenticationRootViewController = .instantiate(storyboard: "AuthenticationRoot")
        let router = AuthenticationRootRouter(view: controller)
        let presenter = AuthenticationRootPresenter(router: router,
                                                    view: controller,
                                                    success: success,
                                                    failure: failure)
        controller.presenter = presenter
        return controller
    }
    
}
