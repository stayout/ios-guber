//
//  AuthenticationRootOutputProtocol.swift
//  Voice
//
//  Created by Stayout on 15.09.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation

protocol AuthenticationRootOutputProtocol: class {
    func authenticationDidRequested(success: @escaping () -> Void, failure: @escaping () -> Void)
}
