//  
//  AuthenticationRootRouter.swift
//  Voice
//
//  Created by Stayout on 23.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common
import Model

final class AuthenticationRootRouter: RootRouter, AuthenticationRootRouterInput {
    
    func presentLogin(output: LoginOutputProtocol?) {
        let controller = LoginBuilder().build(output: output)
        navigationController?.pushViewController(controller, animated: false)
    }
    
}
