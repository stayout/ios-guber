//
//  AuthenticationRootRouterInput.swift
//  Voice
//
//  Created by Stayout on 02.09.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Model
import Common

protocol AuthenticationRootRouterInput: RootRouterInputProtocol {
    func presentLogin(output: LoginOutputProtocol?)
}
