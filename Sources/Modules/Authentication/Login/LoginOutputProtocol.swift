//
//  PhoneOutputProtocol.swift
//  Voice
//
//  Created by Aleksandr Konakov on 17.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Model

protocol LoginOutputProtocol: class {
    func loginDidSucceed()
}
