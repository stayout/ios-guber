//  
//  PhoneInteractor.swift
//  Voice
//
//  Created by Aleksandr Konakov on 27.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Model
import Networking
import RxSwift

final class LoginInteractor {
    
    // MARK: - Properties
    
    weak var presenter: LoginPresenterOutput?
    
}

// MARK: - PhoneInteractorInput
extension LoginInteractor: LoginInteractorInput {
    
    func signIn(login: String, password: String) {
        let authenticaion = Authentication(login: login, password: password)
        AuthenticationStore.save(authenticaion)
    }
    
}
