//
//  PhoneInteractorInput.swift
//  Voice
//
//  Created by Stayout on 31.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

protocol LoginInteractorInput {
    func signIn(login: String, password: String)
}
