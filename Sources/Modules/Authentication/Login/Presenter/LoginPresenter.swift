//  
//  PhonePresenter.swift
//  Voice
//
//  Created by Aleksandr Konakov on 27.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common
import Model

// MARK: - Presenter
final class LoginPresenter: RootPresenterProtocol {

    let interactor: LoginInteractorInput
    weak var view: LoginViewControllerOutput?
    weak var output: LoginOutputProtocol?
    
    init(interactor: LoginInteractorInput,
         view: LoginViewControllerOutput?,
         output: LoginOutputProtocol?) {
        self.interactor = interactor
        self.view = view
        self.output = output
    }
    
}



