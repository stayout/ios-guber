//
//  PhonePresenterInput.swift
//  Voice
//
//  Created by Stayout on 31.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Foundation
import Model

protocol LoginPresenterInput {
    func viewDidLoad()
    func continueButtonDidTapped()
}

// MARK: - PhonePresenterInput
extension LoginPresenter: LoginPresenterInput {
    
    func viewDidLoad() {
    }
    
    func continueButtonDidTapped() {
        interactor.signIn(login: "login", password: "password")
        output?.loginDidSucceed()
    }
    
}
