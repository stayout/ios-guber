//
//  PhonePresenterOutput.swift
//  Voice
//
//  Created by Stayout on 31.08.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import Common
import Model

protocol LoginPresenterOutput: RootPresenterOutputProtocol {
}

// MARK: - PhonePresenterOutput
extension LoginPresenter: LoginPresenterOutput {
}
