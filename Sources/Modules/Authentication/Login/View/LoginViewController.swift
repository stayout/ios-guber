//  
//  PhoneViewController.swift
//  Voice
//
//  Created by Aleksandr Konakov on 27.07.2020.
//  Copyright © 2020 WS. All rights reserved.
//

import UIKit
import Model
import Common

final class LoginViewController: UIViewController {
    
    var presenter: LoginPresenterInput?
    
    var preloaderView: WSPreloaderView = .fromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(preloaderView)
        presenter?.viewDidLoad()
    }
    
    @IBAction func continueButtonDidTapped(_ sender: UIButton) {
        presenter?.continueButtonDidTapped()
    }
    
}

extension LoginViewController: LoginViewControllerOutput {
    
}
