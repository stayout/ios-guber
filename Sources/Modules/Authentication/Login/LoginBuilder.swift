//
//  LoginBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct LoginBuilder {
    
    func build(output: LoginOutputProtocol?) -> UIViewController {
        let controller: LoginViewController = .instantiate(storyboard: "Login")
        let interactor = LoginInteractor()
        let presenter = LoginPresenter(interactor: interactor,
                                       view: controller,
                                       output: output)
        interactor.presenter = presenter
        controller.presenter = presenter
        return controller
    }
    
}
