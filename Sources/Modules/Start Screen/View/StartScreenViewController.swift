//  
//  StartScreenViewController.swift
//  Voice
//
//  Created by Stayout on 11.02.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import UIKit
import Common

class StartScreenViewController: UIViewController {
    
    var presenter: StartScreenPresenterInput?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
}

extension StartScreenViewController: StartScreenViewControllerOutput {    
}
