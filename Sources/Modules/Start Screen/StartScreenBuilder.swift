//
//  StartScreenBuilder.swift
//  Guber
//
//  Created by Stayout on 11.03.2021.
//

import UIKit

struct StartScreenBuilder {
    
    func build(output: StartScreenOutput?) -> UIViewController {
        let controller: StartScreenViewController = .instantiate(storyboard: "StartScreen")
        let presenter = StartScreenPresenter(view: controller, output: output)
        controller.presenter = presenter
        return controller
    }
    
}
