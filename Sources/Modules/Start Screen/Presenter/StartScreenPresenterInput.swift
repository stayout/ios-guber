//
//  StartScreenPresenterInput.swift
//  Voice
//
//  Created by Stayout on 11.02.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import Networking
import Common

protocol StartScreenPresenterInput {
    func viewDidLoad()
}

extension StartScreenPresenter: StartScreenPresenterInput {
    
    func viewDidLoad() {
        if AuthenticationStore.isAuthenticated {
            output?.startScreenCreateWorkSwiftDidTriggered()
        }
        else {
            output?.startScreenAuthenticationDidTriggered()
        }
    }
    
}
