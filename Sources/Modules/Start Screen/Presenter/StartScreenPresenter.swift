//  
//  StartScreenPresenter.swift
//  Voice
//
//  Created by Stayout on 11.02.2021.
//  Copyright © 2021 WS. All rights reserved.
//

import Foundation
import Model
import Common

class StartScreenPresenter: RootPresenterProtocol, UIPreloaderViewPresenterDelegate {

    weak var view: StartScreenViewControllerOutput?
    weak var output: StartScreenOutput?
    
    init(view: StartScreenViewControllerOutput?,
         output: StartScreenOutput?) {
        self.view = view
        self.output = output
    }

}
