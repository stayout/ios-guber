//
//  StartScreenOutput.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

protocol StartScreenOutput: class {
    func startScreenAuthenticationDidTriggered()
    func startScreenCreateWorkSwiftDidTriggered()
}
