//  
//  OrderRootBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct OrderRootBuilder {
    
    func build(output: OrderRootOutput?) -> UIViewController {
        let controller: OrderRootViewController = .instantiate(storyboard: "OrderRoot")
        let router = OrderRootRouter(view: controller)
        let presenter = OrderRootPresenter(router: router, output: output)
        controller.presenter = presenter
        return controller
    }
    
}
