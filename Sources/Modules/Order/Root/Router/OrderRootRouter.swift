//  
//  OrderRootRouter.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import Model
import Common

final class OrderRootRouter: RootRouter, OrderRootRouterInput {
    
    func presentOrderCreate(output: OrderCreateOutput?) {
        let controller = OrderCreateBuilder().build(output: output)
        navigationController?.pushViewController(controller, animated: false)
    }
    
}
