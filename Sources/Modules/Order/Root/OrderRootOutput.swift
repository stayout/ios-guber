//
//  OrderRootOutput.swift
//  Guber
//
//  Created by Stayout on 17.03.2021.
//

protocol OrderRootOutput: class {
    func orderDidDeclined()
    func orderDidCreated(route: Route)
}
