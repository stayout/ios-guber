//  
//  OrderRootViewController.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit
import Common

class OrderRootViewController: UINavigationController {

    var presenter: OrderRootPresenterInput?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }

}
