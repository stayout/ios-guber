//  
//  OrderRootPresenter.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import Model
import Common

final class OrderRootPresenter {

    let router: OrderRootRouterInput
    weak var output: OrderRootOutput?
    
    init(router: OrderRootRouterInput,
         output: OrderRootOutput?) {
        self.router = router
        self.output = output
    }

}
