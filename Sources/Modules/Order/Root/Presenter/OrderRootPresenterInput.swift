//  
//  OrderRootPresenterInput.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import Common

protocol OrderRootPresenterInput {    
    func viewDidLoad()
}

extension OrderRootPresenter: OrderRootPresenterInput {
    
    func viewDidLoad() {
        router.presentOrderCreate(output: self)
    }
    
}

extension OrderRootPresenter: OrderCreateOutput {
    
    func orderCreateDeclineDidRequested() {
        output?.orderDidDeclined()
        router.dismiss(animated: true)
    }
    
    func orderCreateAccepntDidRequested(route: Route) {
        output?.orderDidCreated(route: route)
        router.dismiss(animated: true)
    }
    
}
