//  
//  OrderCreateInteractor.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit
import Networking
import Model
import Common
import RxSwift

final class OrderCreateInteractor {
    
    let bag = DisposeBag()
    weak var presenter: OrderCreatePresenterOutput?
    
}

extension OrderCreateInteractor: OrderCreateInteractorInput { 

}