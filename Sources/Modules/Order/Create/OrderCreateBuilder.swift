//  
//  OrderCreateBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct OrderCreateBuilder {
    
    func build(output: OrderCreateOutput?) -> UIViewController {
        let controller: OrderCreateViewController = .instantiate(storyboard: "OrderCreate")
        let router = OrderCreateRouter(view: controller)
        let interactor = OrderCreateInteractor()
        let presenter = OrderCreatePresenter(router: router,
                                             interactor: interactor,
                                             view: controller,
                                             output: output)
        interactor.presenter = presenter
        controller.presenter = presenter
        controller.title = "Новая заявка"
        return controller
    }
    
}
