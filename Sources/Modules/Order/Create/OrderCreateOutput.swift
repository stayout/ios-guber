//
//  OrderCreateOutput.swift
//  Guber
//
//  Created by Stayout on 17.03.2021.
//

protocol OrderCreateOutput: class {
    func orderCreateDeclineDidRequested()
    func orderCreateAccepntDidRequested(route: Route)
}
