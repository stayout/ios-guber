//  
//  OrderCreatePresenter.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import Model
import Common
import YandexMapsMobile

class OrderCreatePresenter: RootPresenterProtocol, UIPreloaderViewPresenterDelegate {

    let router: OrderCreateRouterInput
    let interactor: OrderCreateInteractorInput
    weak var view: OrderCreateViewControllerOutput?
    weak var output: OrderCreateOutput?
    
    let route: Route
    
    init(router: OrderCreateRouterInput,
         interactor: OrderCreateInteractorInput,
         view: OrderCreateViewControllerOutput?,
         output: OrderCreateOutput?) {
        self.router = router
        self.interactor = interactor
        self.view = view
        self.output = output
        route = Route(
            start: YMKPoint(latitude: 48.490032, longitude: 135.083148),
            end: YMKPoint(latitude: 48.471733, longitude: 135.0751809)
        )
        
    }

}
