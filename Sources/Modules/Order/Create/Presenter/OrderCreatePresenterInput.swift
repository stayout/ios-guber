//  
//  OrderCreatePresenterInput.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import Foundation
import Common
import YandexMapsMobile

protocol OrderCreatePresenterInput {
    func viewDidLoad()
    func timerDidEnded()
    func declineButtonDidTapped()
    func acceptButtonDidTapped()
    func dismissButtonDidTapped()
}

extension OrderCreatePresenter: OrderCreatePresenterInput {
    
    func viewDidLoad() {
        view?.updateUser(title: "Евгений Петров Сергеевич")
        view?.updateCommentary(title: "Текстовый комментарий к заявке")
        view?.createRoute(start: route.start, end: route.end)
        view?.startTimer(60)
    }
    
    func timerDidEnded() {
        view?.hideTimer()
    }
    
    func declineButtonDidTapped() {
        output?.orderCreateDeclineDidRequested()
    }
    
    func acceptButtonDidTapped() {
        view?.hideTimer()
        output?.orderCreateAccepntDidRequested(route: route)
    }
    
    func dismissButtonDidTapped() {
        router.dismiss(animated: true)
    }
    
}
