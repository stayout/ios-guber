//  
//  OrderCreateViewController.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit
import Common
import YandexMapsMobile
import SRCountdownTimer

class OrderCreateViewController: UIViewController {
    
    @IBOutlet var userLabel: UILabel!
    @IBOutlet var commentaryLabel: UILabel!
    @IBOutlet var mapView: YMKMapView!
    @IBOutlet var timerView: SRCountdownTimer!
    @IBOutlet var timerViewWrapper: UIView!
    @IBOutlet var timerWrapperViewBottomMargin: NSLayoutConstraint!
    @IBOutlet var routeDistanceLabel: UILabel!
    @IBOutlet var routeDurationLabel: UILabel!
    var preloaderView: WSPreloaderView = .fromNib()
    
    var drivingSession: YMKDrivingSession?
    var trafficLayer : YMKTrafficLayer!
    var presenter: OrderCreatePresenterInput?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        timerView.delegate = self
        view.addSubview(preloaderView)
        
        addDismissButton(handler: presenter?.dismissButtonDidTapped)
        presenter?.viewDidLoad()
    }
    
    func onRoutesReceived(_ routes: [YMKDrivingRoute]) {
        let mapObjects = mapView.mapWindow.map.mapObjects
        
        let route = routes.min(by: { l, r in
            let lt = l.sections.map({ section in
                section.metadata.weight.timeWithTraffic.value
            }).reduce(0, +)
            let rt = r.sections.map({ section in
                section.metadata.weight.timeWithTraffic.value
            }).reduce(0, +)
            return lt < rt
        })
        
        let distance = route!.sections.map({ $0.metadata.weight.distance.value }).reduce(0, +)
        routeDistanceLabel.text = "Расстояние маршрута: \( (distance / 1000).round(to: 1) ) км"
        let duration = route!.sections.map({ $0.metadata.weight.timeWithTraffic.value }).reduce(0, +).rounded(.up)
        routeDurationLabel.text = "Длительность маршрута: \( Int(duration / 60) ) мин"
        
        let polyline = mapObjects.addPolyline(with: route!.geometry)
        polyline.strokeColor = .orange
        polyline.strokeWidth = 2
    }
    
    func iconFrom(text: String) -> UIImage {
        let scale = UIScreen.main.scale
        let fontSize: CGFloat = 15
        let marginSize: CGFloat = 3
        let strokeSize: CGFloat = 3
        let font = UIFont.systemFont(ofSize: scale * fontSize)
        let size = text.size(withAttributes: [NSAttributedString.Key.font: font])
        let textRadius = sqrt(size.height * size.height + size.width * size.width) / 2
        let internalRadius = textRadius + marginSize * scale
        let externalRadius = internalRadius + strokeSize * scale
        let iconSize = CGSize(width: externalRadius * 2, height: externalRadius * 2)
        
        UIGraphicsBeginImageContext(iconSize)
        let ctx = UIGraphicsGetCurrentContext()!
        
        ctx.setFillColor(UIColor.red.cgColor)
        ctx.fillEllipse(in: CGRect(
                            origin: .zero,
                            size: CGSize(width: 2 * externalRadius, height: 2 * externalRadius))
        )
        
        ctx.setFillColor(UIColor.white.cgColor)
        ctx.fillEllipse(in: CGRect(
                            origin: CGPoint(x: externalRadius - internalRadius, y: externalRadius - internalRadius),
                            size: CGSize(width: 2 * internalRadius, height: 2 * internalRadius)))
        
        text.draw(
            in: CGRect(
                origin: CGPoint(x: externalRadius - size.width / 2, y: externalRadius - size.height / 2),
                size: size),
            withAttributes: [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ])
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        return image
    }
    
    @IBAction func declineButtonDidTapped(_ sender: UIButton) {
        presenter?.declineButtonDidTapped()
    }
    
    @IBAction func acceptButtonDidTapped(_ sender: UIButton) {
        presenter?.acceptButtonDidTapped()
    }
    
}

extension OrderCreateViewController: SRCountdownTimerDelegate {
    
    func timerDidEnd() {
        presenter?.timerDidEnded()
    }
    
}

extension OrderCreateViewController: OrderCreateViewControllerOutput {
    
    func updateUser(title: String?) {
        userLabel.text = title
    }
    
    func updateCommentary(title: String?) {
        commentaryLabel.text = title
    }
    
    func createRoute(start: YMKPoint, end: YMKPoint) {
        let position = YMKCameraPosition(target: start, zoom: 15, azimuth: 0, tilt: 0)
        let animation = YMKAnimation(type: .smooth, duration: 1.5)
        let style = YMKIconStyle()
        mapView.mapWindow.map.mapObjects.addPlacemark(with: start, image: iconFrom(text: "A"), style: style)
        mapView.mapWindow.map.mapObjects.addPlacemark(with: end, image: iconFrom(text: "B"), style: style)
        mapView.mapWindow.map.move(with: position, animationType: animation, cameraCallback: nil)
        
        let requestPoints : [YMKRequestPoint] = [
            YMKRequestPoint(point: start, type: .waypoint, pointContext: nil),
            YMKRequestPoint(point: end, type: .waypoint, pointContext: nil),
        ]
        
        let responseHandler = {(routesResponse: [YMKDrivingRoute]?, error: Error?) -> Void in
            if let routes = routesResponse {
                self.onRoutesReceived(routes)
            }
        }
        
        let drivingRouter = YMKDirections.sharedInstance().createDrivingRouter()
        drivingSession = drivingRouter.requestRoutes(
            with: requestPoints,
            drivingOptions: YMKDrivingDrivingOptions(),
            vehicleOptions: YMKDrivingVehicleOptions(),
            routeHandler: responseHandler
        )
    }
    
    func startTimer(_ value: Int) {
        timerView.start(beginingValue: value)
        timerWrapperViewBottomMargin.constant = 16
        view.layoutIfNeeded(duration: 0.75)
    }
    
    func hideTimer() {
        timerView.pause()
        timerWrapperViewBottomMargin.constant = -(timerViewWrapper.height * 1.5)
        view.layoutIfNeeded(duration: 0.75)
    }
    
}
