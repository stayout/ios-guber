//  
//  OrderCreateViewControllerOutput.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import Common
import YandexMapsMobile

protocol OrderCreateViewControllerOutput: UIPreloaderViewDelegate {
    func updateUser(title: String?)
    func updateCommentary(title: String?)
    func createRoute(start: YMKPoint, end: YMKPoint)
    func startTimer(_ value: Int)
    func hideTimer()
}
