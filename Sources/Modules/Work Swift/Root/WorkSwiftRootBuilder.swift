//
//  WorkSwiftRootBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct WorkSwiftRootBuilder {
    
    func build(output: WorkSwiftRootOutput?) -> UIViewController {
        let controller: WorkSwiftRootViewController = .instantiate(storyboard: "WorkSwiftRoot")
        let router = WorkSwiftRootRouter(view: controller)
        let presenter = WorkSwiftRootPresenter(router: router,
                                               view: controller,
                                               output: output)
        controller.presenter = presenter
        return controller
    }
    
}
