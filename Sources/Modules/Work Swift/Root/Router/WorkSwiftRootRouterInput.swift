//  
//  WorkSwiftRootRouterInput.swift
//  Guber
//
//  Created by Stayout on 09.03.2021.
//

import Common
import Model

protocol WorkSwiftRootRouterInput: RootRouterInputProtocol {
    func presentCreateSwift(output: WorkSwiftCreateOutput?)
}
