//  
//  WorkSwiftRootRouter.swift
//  Guber
//
//  Created by Stayout on 09.03.2021.
//

import Model
import Common

final class WorkSwiftRootRouter: RootRouter, WorkSwiftRootRouterInput {
    
    func presentCreateSwift(output: WorkSwiftCreateOutput?) {
        let controller = WorkSwiftCreateBuilder().build(output: output)
        navigationController?.pushViewController(controller, animated: false)
    }
    
}
