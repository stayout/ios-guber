//  
//  WorkSwiftRootViewController.swift
//  Guber
//
//  Created by Stayout on 09.03.2021.
//

import UIKit
import Common

class WorkSwiftRootViewController: UINavigationController {

    var presenter: WorkSwiftRootPresenterInput?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.prefersLargeTitles = true
        presenter?.viewDidLoad()
    }
    
}

extension WorkSwiftRootViewController: WorkSwiftRootViewControllerOutput {
    
}
