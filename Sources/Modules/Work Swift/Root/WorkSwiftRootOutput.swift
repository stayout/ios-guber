//
//  WorkSwiftOutput.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

protocol WorkSwiftRootOutput: class {
    func workSwiftDidCreated()
}
