//  
//  WorkSwiftRootPresenter.swift
//  Guber
//
//  Created by Stayout on 09.03.2021.
//

import Model
import Common

class WorkSwiftRootPresenter: RootPresenterProtocol, UIPreloaderViewPresenterDelegate {

    let router: WorkSwiftRootRouterInput
    weak var view: WorkSwiftRootViewControllerOutput?
    var output: WorkSwiftRootOutput?
    
    init(router: WorkSwiftRootRouterInput,
         view: WorkSwiftRootViewControllerOutput?,
         output: WorkSwiftRootOutput?) {
        self.router = router
        self.view = view
        self.output = output
    }

}
