//  
//  WorkSwiftRootPresenterInput.swift
//  Guber
//
//  Created by Stayout on 09.03.2021.
//

import Common
import Foundation

protocol WorkSwiftRootPresenterInput {
    func viewDidLoad()
}

extension WorkSwiftRootPresenter: WorkSwiftRootPresenterInput {
    
    func viewDidLoad() {
        router.presentCreateSwift(output: self)
    }
    
}

extension WorkSwiftRootPresenter: WorkSwiftCreateOutput {
    
    func workSwiftDidCreated() {
        output?.workSwiftDidCreated()
    }
    
}
