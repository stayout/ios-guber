//  
//  WorkSwiftCreateInteractor.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import UIKit
import RxSwift
import Networking
import Model
import Common

final class WorkSwiftCreateInteractor {
    
    let bag = DisposeBag()
    weak var presenter: WorkSwiftCreatePresenterOutput?
    
}

extension WorkSwiftCreateInteractor: WorkSwiftCreateInteractorInput { 

}