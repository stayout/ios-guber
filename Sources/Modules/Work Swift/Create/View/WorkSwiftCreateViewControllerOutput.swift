//  
//  WorkSwiftCreateViewControllerOutput.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import Common

protocol WorkSwiftCreateViewControllerOutput: UITableViewContentOperationsDelegate, UIPreloaderViewDelegate {
    func updatePerson(description: String?)
    func updateContinueButton(isHidden: Bool)
}
