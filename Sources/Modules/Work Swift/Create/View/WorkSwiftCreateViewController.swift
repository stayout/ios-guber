//  
//  WorkSwiftCreateViewController.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import UIKit
import Common

class WorkSwiftCreateViewController: UIViewController {

    @IBOutlet var personLabel: UILabel!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet weak var tableView: RootTableView?

    var presenter: WorkSwiftCreatePresenterInput?
    var preloaderView: WSPreloaderView = .fromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(preloaderView)
        tableView?.selectableDelegate = presenter
        presenter?.viewDidLoad()
    }

    @IBAction func continueButtonDidTapped(_ sender: UIButton) {
        presenter?.continueButtonDidTapped()
    }
    
}

extension WorkSwiftCreateViewController: WorkSwiftCreateViewControllerOutput {
    
    func updatePerson(description: String?) {
        personLabel.text = description
    }
    
    func updateContinueButton(isHidden: Bool) {
        continueButton.isHidden = isHidden
    }
    
}
