//  
//  WorkSwiftCreateTableViewDefaultCell.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import UIKit
import Common

class WorkSwiftCreateTableViewDefaultCell: RootTableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var diclosureImageView: UIImageView!
    
    override func update<C: RootCellContent>(by content: C?, animDuration: Double) {
        titleLabel.text = content?.title
        diclosureImageView.isHidden = content?.isSelected == false
    }
    
}
