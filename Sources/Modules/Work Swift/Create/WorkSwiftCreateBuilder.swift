//
//  WorkSwiftCreateBuilder.swift
//  Guber
//
//  Created by Stayout on 12.03.2021.
//

import UIKit

struct WorkSwiftCreateBuilder {
    
    func build(output: WorkSwiftCreateOutput?) -> UIViewController {
        let controller: WorkSwiftCreateViewController = .instantiate(storyboard: "WorkSwiftCreate")
        let router = WorkSwiftCreateRouter(view: controller)
        let interactor = WorkSwiftCreateInteractor()
        let presenter = WorkSwiftCreatePresenter(router: router,
                                               interactor: interactor,
                                               view: controller,
                                               output: output)
        interactor.presenter = presenter
        controller.presenter = presenter
        controller.title = "Открыть смену"
        return controller
    }
    
}
