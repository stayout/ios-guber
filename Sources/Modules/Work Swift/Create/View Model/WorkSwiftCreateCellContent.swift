//  
//  WorkSwiftCreateCellContent.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import UIKit
import Common
import Model

class WorkSwiftCreateCellContent: RootCellContent {

    typealias TableType = WorkSwiftCreateTableView

    init(_ identifier: TableType.identifiers = .base) {
        super.init(identifier: identifier.rawValue)
    }

}

extension WorkSwiftCreateCellContent {
}
