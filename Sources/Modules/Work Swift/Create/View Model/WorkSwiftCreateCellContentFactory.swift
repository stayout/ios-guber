//  
//  WorkSwiftCreateCellContentFactory.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import UIKit
import Common
import Model

class WorkSwiftCreateCellContentFactory {
    
    typealias CellType = WorkSwiftCreateCellContent
    typealias TableType = WorkSwiftCreateTableView
    
    public static func cells() -> [CellType] {
        let cells = [
            CellType().with(title: "Машина 1"),
            CellType().with(title: "Машина 2"),
            CellType().with(title: "Машина 3"),
            CellType().with(title: "Машина 4"),
            CellType().with(title: "Машина 5")
        ]
        return cells
    }
    
}
