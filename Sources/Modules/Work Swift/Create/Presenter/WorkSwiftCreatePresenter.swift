//  
//  WorkSwiftCreatePresenter.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import Model
import Common

class WorkSwiftCreatePresenter: RootPresenterProtocol, UIPreloaderViewPresenterDelegate {

    let router: WorkSwiftCreateRouterInput
    let interactor: WorkSwiftCreateInteractorInput
    weak var view: WorkSwiftCreateViewControllerOutput?
    weak var output: WorkSwiftCreateOutput?
    
    var cells = [WorkSwiftCreateCellContent]()
    
    init(router: WorkSwiftCreateRouterInput,
         interactor: WorkSwiftCreateInteractorInput,
         view: WorkSwiftCreateViewControllerOutput?,
         output: WorkSwiftCreateOutput?) {
        self.router = router
        self.interactor = interactor
        self.view = view
        self.output = output
    }
    
}
