//  
//  WorkSwiftCreatePresenterInput.swift
//  Guber
//
//  Created by Stayout on 10.03.2021.
//

import Foundation
import Common

protocol WorkSwiftCreatePresenterInput: UITableViewSelectableDelegate {
    func viewDidLoad()
    func continueButtonDidTapped()
}

extension WorkSwiftCreatePresenter: WorkSwiftCreatePresenterInput {
    
    func viewDidLoad() {
        cells = WorkSwiftCreateCellContentFactory.cells()
        view?.update(cells: cells)
        view?.updatePerson(description: "Иванов Иван Иванович")
    }
    
    func didSelectItem(by indexPath: IndexPath) {
        cells.update(index: indexPath.row, selection: .single)
        view?.reloadData()
        view?.updateContinueButton(isHidden: cells.selectedItem == nil)
    }
    
    func continueButtonDidTapped() {
        output?.workSwiftDidCreated()
    }
    
}
